<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="apple-touch-icon" sizes="180x180" href="../apple-touch-icon.png" />
    <link rel="icon" type="image/x-icon" sizes="32x32" href="../favicon-32x32.png" />
    <link rel="icon" type="image/x-icon" sizes="16x16" href="../favicon-16x16.png" />
    <link rel="manifest" href="/..site.webmanifest" />
    <title>Al-mumtaz | Online Electronics &amp; Home Appliances Store in Lahore ,bhapur &amp; toba taxing - Best Prices
        | Also on Installments | Al-mumtaz</title>
    <meta name="description"
        content="Top Quality products like inverter A/C, Refrigerator, LED TV, Mobiles of Brands: Samsung, LG, Haier, Gree, Apple... We provide a large variety of international Products at Al-mumtaz | Buy online Air Conditioner, LED TV, Kitchen Appliances, Washing Machines, Refrigerators, Water Dispensers, Mobile and Air Coolers in lahore at link road We have all kind of Appliances on Cash &amp; Installments" />
    <link rel="stylesheet" href="{{ asset('/plugins/OwlCarousel/dist/assets/owl.carousel.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/header-footer.css') }}" />
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-220937501-1"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.7/jquery.lazyload.js"></script>
    <!-- Meta Pixel Code -->
    <script>
        !(function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments);
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = "2.0";
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s);
        })(window, document, "script", "https://connect.facebook.net/en_US/fbevents.js");
        fbq("init", "301019362097381");
        fbq("track", "PageView");
    </script>
    <noscript><img height="1" width="1" style="display: none;"
            src="https://www.facebook.com/tr?id=301019362097381&ev=PageView&noscript=1" /></noscript>
    <!-- End Meta Pixel Code -->
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag("js", new Date());

        gtag("config", "UA-220937501-1");
    </script>
    <style>
        .wa-icon {
            position: fixed;
            display: flex;
            width: 60px;
            height: 60px;
            z-index: 100;
            bottom: 100px;
            right: 26px;
            text-align: center;
            font-size: 40px;
            border-radius: 50%;
            box-shadow: 0 1px 15px rgb(32 33 36 / 28%);
        }

        #wa-chat-widget {
            display: none;
            position: fixed;
            border-radius: 10px;
            box-shadow: 0 1px 15px rgb(32 33 36 / 28%);
            bottom: 165px;
            right: 30px;
            overflow: hidden;
            z-index: 99;
            animation-name: wa-chat-animation;
            animation-duration: 1s;
            /* width: 20%; */
            opacity: 100;
        }

        #wa-chat-widget:target {
            display: block;
        }

        .wa-chat-widget-header {
            display: flex;
            background: rgb(7, 94, 84);
            background: linear-gradient(90deg, rgba(7, 94, 84, 1) 0%, rgba(18, 140, 126, 1) 85%);
            color: #ffffff;
            padding: 1rem;
        }

        .wa-chat-widget-header img {
            max-width: 2.5rem;
            border-radius: 50%;
        }

        .wa-chat-widget-header a.close {
            position: absolute;
            top: 5px;
            right: 15px;
            color: #fff;
            font-size: 30px;
            text-decoration: none;
        }

        .wa-chat-widget-profile {
            padding: 0 1rem 0 1rem;
            border-radius: 50%;
        }

        .wa-chat-widget-body {
            padding: 20px 20px 20px 10px;
            background-color: rgb(230, 221, 212);
        }

        .wa-chat-widget-body .message {
            padding: 0.5rem;
            background-color: rgb(255, 255, 255);
            border-radius: 0px 8px 8px;
            box-shadow: rgb(0 0 0 / 13%) 0px 1px 0.5px;
        }

        .wa-chat-widget-body .message .profile-name {
            color: #787878;
        }

        .wa-chat-widget-body .message .wcw-message {
            margin-top: 0.5rem;
        }

        .wa-chat-widget-send form {
            display: flex;
            margin: 0;
            background-color: rgb(230, 221, 212);
        }

        .wa-chat-widget-send input {
            border: none;
            padding: 0.5rem;
            width: 100%;
            border-radius: 1rem;
        }

        .wa-chat-widget-send button {
            background-color: #075e54;
            padding: 0.5rem;
            border-radius: 50%;
            border: none;
            fill: #ffffff;
        }

        @keyframes wa-chat-animation {
            from {
                opacity: 1;
            }
        }

        /* .whats-app {
            position: fixed;
            width: 60px;
            height: 60px;
            bottom: 100px;
            background-color: #25d366;
            color: #FFF;
            border-radius: 50px;
            text-align: center;
            font-size: 40px;
            right: 26px;
            z-index: 100;
        }

        .whats-app-icon {
            margin-top: 9px;
            margin-left: 10px;
            font-size: 36px;
        } */

        /* for page body text */
        @import url("https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap");

        /* for the neon font - Train One */
        @import url("https://fonts.googleapis.com/css2?family=Train+One&display=swap");
        @import url("https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800|Poppins:300,400,500,600,700,800,900");

        .nav a {
            font-family: Century Gothic, sans-serif !important;
        }

        .offerbg {
            background-image: linear-gradient(to right, #2056aa, #e91a24);
            font-family: "Roboto";
            color: #2056aa, #e91a24;
            text-align: center;
            position: relative;
        }

        .offerbg #close {
            position: absolute;
            top: 0px;
            right: 14px;
            background: transparent;
            border: none;
            color: black;
            font-size: 20px;
            z-index: 101;
        }

        .closeicon {
            height: 30px;
            width: 30px;
            right: -5px;
        }

        #container {
            position: fixed;
            width: 600px;
            height: 600px;
            bottom: 100px;
            text-align: center;
            border-radius: 15px;
            font-size: 30px;
            background: #4a0560;
            right: 535px;
            z-index: 100;
        }

        #example-link {
            margin-top: 50px;
        }

        a {
            color: #999;
        }

        a:hover {
            color: #fff;
        }

        /* BELOW: CODE FOR NEON FONT EFFECT */

        .neon {
            font-family: "Train One", cursive;
            font-size: 30pt;
            animation: glow 1s linear infinite;
            -moz-animation: glow 1s linear infinite;
            -webkit-animation: glow 1s linear infinite;
            -o-animation: glow 1s linear infinite;
            margin-top: 15px;
        }

        @keyframes glow {
            50% {
                text-shadow: 0 0 0.5vw #8f2626, 0 0 2vw #ff0000, 0 0 3vw #ff3366, 0 0 3vw #fff, 0 0 5vw #fed641, 0 0 3vw #fed641, 0 0 2vw #ff69b4, 0 0 0.5vw #ff69b4;
                color: #ff6347;
            }

            0%,
            100% {
                text-shadow: 0 0 0.5vw #e01c1c, 0 0 1vw #e01c1c, 0 0 2vw #ff3366, 0 0 3vw #ff3366, 0 0 5vw #ff3366, 0 0 2vw #ff3366, 0 0 1vw #ff3366, 0 0 0.5vw #8f2626;
                color: #ff3366;
                -webkit-text-stroke: 0.1px #ff69b4;
            }
        }

        .modal-header {
            background: #0275d8;
        }

        .modal-title {
            margin-bottom: 5px;
            line-height: 0.5;
            color: white;
        }

        .modal.fade.show .modal-dialog.modal-dialog-slideout {
            -webkit-animation-name: bounce;
            animation-name: bounce;
            -webkit-animation-duration: 1s;
            animation-duration: 1s;
        }

        @keyframes bounce {
            0% {
                transform: translateY(-25%);
            }

            20% {
                transform: translateY(15%);
            }

            40% {
                transform: translateY(-15%);
            }

            60% {
                transform: translateY(5%);
            }

            80% {
                transform: translateY(-5%);
            }

            100% {
                transform: translateY(0%);
            }
        }

        .lang {
            height: 24px;
            width: 31px;
            position: relative;
        }

        #cover {
            position: fixed;
            height: 100%;
            width: 100%;
            top: 0;
            left: 0;
            background: #ffffff;
            z-index: 9999;
        }

        body {
            overflow-x: hidden;
        }
    </style>
</head>

<body class="">
    <div id="cover"></div>
    <div id="app">
        @include('website.header')
        <router-view></router-view>
        @include('website.footer')
    </div>

    <script src="{{ asset('/js/app.js') }}"></script>
    <script src="https://kit.fontawesome.com/a0183de4e9.js" crossorigin="anonymous"></script>
    <script src="{{ asset('/plugins/OwlCarousel/dist/owl.carousel.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <!-- Messenger Chat plugin Code -->
    <div id="fb-root"></div>

    <!-- Your Chat plugin code -->
    <div id="fb-customer-chat" class="fb-customerchat"></div>

    <script>
        $(window).on("load", function() {
            $("#cover").hide();
            $("body").css("overflow": "hidden");
        });

        var chatbox = document.getElementById("fb-customer-chat");
        chatbox.setAttribute("page_id", "797367833991244");
        chatbox.setAttribute("attribution", "biz_inbox");

        window.fbAsyncInit = function() {
            FB.init({
                xfbml: true,
                version: "v12.0",
            });
        };

        (function(d, s, id) {
            var js,
                fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js";
            fjs.parentNode.insertBefore(js, fjs);
        })(document, "script", "facebook-jssdk");
    </script>
    <!-- Meta Pixel Code -->
    <script>
        !(function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments);
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = "2.0";
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s);
        })(window, document, "script", "https://connect.facebook.net/en_US/fbevents.js");
        fbq("init", "301019362097381");
        fbq("track", "PageView");
    </script>
    <noscript><img height="1" width="1" style="display: none;"
            src="https://www.facebook.com/tr?id=301019362097381&ev=PageView&noscript=1" /></noscript>
    <!-- End Meta Pixel Code -->
</body>

<script type="text/javascript">
    $(document).ready(function() {
        // Mobile Fixed Bottom Nav
        var navItems = document.querySelectorAll(".mobile-bottom-nav__item");
        navItems.forEach(function(e, i) {
            e.addEventListener("click", function(e) {
                navItems.forEach(function(e2, i2) {
                    e2.classList.remove("mobile-bottom-nav__item--active");
                });
                this.classList.add("mobile-bottom-nav__item--active");
            });
        });
        // Mobile Menu
        var trigger = $(".hamburger"),
            overlay = $(".overlay"),
            isClosed = false;

        trigger.click(function() {
            hamburger_cross();
        });

        function hamburger_cross() {
            if (isClosed == true) {
                overlay.hide();
                trigger.removeClass("is-open");
                trigger.addClass("is-closed");
                isClosed = false;
            } else {
                overlay.show();
                trigger.removeClass("is-closed");
                trigger.addClass("is-open");
                isClosed = true;
            }
        }

        $('[data-toggle="offcanvas"]').click(function() {
            $("#wrapper").toggleClass("toggled");
        });

        fetchBrands();
        getCategory();
        // Category
        function getCategory() {
            var category = [];
            $.ajax({
                url: "{{ url('getcategory') }}",
                type: "GET",
                success: function(response) {
                    category = response.data.categories;
                    $.each(category, function(index, category) {
                        $("#cat").append('<li><a href="./#/store/category/' + category
                            .name + '"><img src =".' + category.image_path +
                            '"/><span>' + category.name + "</span> </a></li>");
                        $("#mobile-nav").append('<li><a href="./#/store/category/' +
                            category.name + '"><span>' + category.name +
                            "</span> </a></li>");
                    });
                },
            });
        }
        // Fetch Brands
        function fetchBrands() {
            var website = true;
            var brands = [];
            $.ajax({
                url: "{{ route('brandimage.index') }}",
                type: "GET",
                data: {
                    website: website,
                },
                success: function(response) {
                    brands = response.data;
                    $.each(brands, function(index, item) {
                        $(".brand-slider").append('<div> <a href="./#/store/company/' + item
                            .company_id + '"> <img src=".' + item.image_path +
                            '" style="width: 150px; height: 60px;"></a> </div>');
                    });
                    // Brand Slider
                    $(".brand-slider").slick({
                        slidesToShow: 5,
                        slidesToScroll: 1,
                        autoplay: true,
                        autoplaySpeed: 2000,
                        dots: false,
                        arrows: false,
                        responsive: [{
                                breakpoint: 991,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 1,
                                },
                            },
                            {
                                breakpoint: 768,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 1,
                                },
                            },
                            {
                                breakpoint: 500,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 1,
                                },
                            },
                        ],
                    });
                },
            });
        }
        $("#searchItems").click(function() {
            window.location.href = "./#/store/search/" + $("#search_items").val();
        });
    });
</script>

</html>
