@php
    use App\Models\Disclaimer;
    $disc = Disclaimer::first();
@endphp
<header>
    <div class="container res-container web-top-bar">
        <!-- Topbar -->
        <div class="header">
            @if (!empty($disc->title))
                <marquee
                    style="animation: blinker 1.5s linear infinite;
        color:#ffffff;
        width:100%;
        background-color:#1E56AB;
        font-family: Sofia pro,Arial,sans-serif;"
                    behavior="scroll" direction=""><b><span style = "color:yellow">DISCLAIMER!</span>

                        {{ $disc->title }}
                    </b> </marquee>
            @endif
        </div>
        {{-- <div class="M001-topbar" id="main_head">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-2 col-lg-4 col-xl-6">
                    </div>
                    <div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
                        <div class="M001-nav-list">
                            <nav class="nav">
                                <a href="http://www.atozcargo.pk/tracking" class="nav-link">Track Order</a>
                                <router-link class="nav-link" to="/aboutus">About Us</router-link> --}}
        {{-- <router-link class="nav-link" to="#">Our Stores</router-link> --}}
        {{-- <router-link class="nav-link" to="/contactus">Contact Us</router-link>
                                <router-link class="nav-link" to="/feedback">Feedback</router-link>
                                <a href="https://www-almumtaz-com-pk.translate.goog/?_x_tr_sl=auto&_x_tr_tl=ur&_x_tr_hl=en&_x_tr_pto=wapp#/"
                                    class="nav-link"><img src="{{ asset('images/globe.png') }}" class="lang"
                                        alt="language" />Urdu</a>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        <!-- Search & Logo Area -->
        <div class="M001-search-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-3 col-xl-3">
                        <div class="M001-logo text-center text-lg-left">
                            <div class="M001-inner-logo w-100 d-flex align-items-center justify-content-start ">
                                <router-link to="/">
                                    <img class="w_adjustment" src="{{ asset('header_image/logo.png') }}"
                                        alt="Al-Mumtaz" />
                                </router-link>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-5 col-xl-5">
                        <div class="M001-input-inner">
                            <div class="M001-input">
                                <div class="category-style">
                                    <div class="M001-nav-category">
                                        <div class="M001-nav-cat-btn">
                                            <span>
                                                ALL CATEGORIES
                                            </span>
                                            <img src="{{ asset('header_image/dropdown.png') }}" width="20px" />
                                        </div>
                                        <div class="M001-cat-list-container">
                                            <div class="M001-cat-list-inner">
                                                <div class="M001-cat-list">
                                                    <ul class="M001-main-cat-list M001-cat-001 list-unstyled m-0 font-size w-100 aresp"
                                                        id="cat"></ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="text" class="form-control" placeholder="Search for Products"
                                    id="search_items" />
                                <div class="M001-search-btn">
                                    <a id="searchItems"> <i class="fa fa-search text-white mr-0"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4 col-xl-4">
                        <div class="row p-0 m-0">
                            <div class="col-4 p-0 m-0 text-center">
                                <div class="M001-fav-btn">
                                    <div class="M001-fav-inner">
                                        <router-link to="/wishlist">
                                            <div class="M001-fav-border mb-1">
                                                <img src="{{ asset('header_image/wishlist.png') }}" width="22px"
                                                    height="22px" />
                                            </div>
                                            <span class="d-none d-md-inline">WishList</span>
                                        </router-link>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4 p-0 m-0 text-center">
                                <div class="M001-cart-btn">
                                    <div class="M001-cart-inner ">
                                        <router-link to="/cart" class="">
                                            <div class="M001-fav-border position-relative">
                                                <div class="badge p-1 rounded-circle">
                                                    <div id="user_cart_count p-1">0</div>
                                                </div>
                                                <img src="{{ asset('header_image/cart.png') }}" width="20px" />
                                            </div>
                                        </router-link>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4 p-0 m-0 text-center">
                                <div class="M001-cart-btn">
                                    <a href="https://www-almumtaz-com-pk.translate.goog/?_x_tr_sl=auto&_x_tr_tl=ur&_x_tr_hl=en&_x_tr_pto=wapp#/"
                                        class="M001-cart-inner">
                                        <img src="{{ asset('header_image/language.png') }}" width="20px" />
                                        <span class="ml-2">Urdu</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Navigation Menu's -->
        <div class="M001-nav-area M001-topbar">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-1 d-none d-xl-inline-block p-0"></div>
                    <div class="col-lg-9 col-md-12 col-xl-8">
                        <div class="M001-cat-list justify-content-md-center justify-content-lg-start ">
                            <nav class="nav">
                                <router-link class="nav-link" to="/">Home</router-link>
                                <router-link class="nav-link" to="/store">Shop</router-link>
                                <router-link class="nav-link" to="/blog">Blog</router-link>
                                <router-link class="nav-link" to="/deals">Deals</router-link>
                                <router-link class="nav-link" to="/newarrival">New Arrivals</router-link>
                                <router-link class="nav-link" to="/onsale">On Sale</router-link>
                                <router-link class="nav-link" to="/offers">New Offers</router-link>
                                <router-link class="nav-link" to="/aboutus">About Us</router-link>
                                <router-link class="nav-link" to="/contactus">Contact Us</router-link>
                                <router-link class="nav-link" to="/feedback">Feedback</router-link>
                                <div class="M001-list-animation M001-home-list"></div>
                            </nav>
                        </div>
                    </div>
                    <div class="col-1 d-md-none d-inline-block "></div>
                    @if (Auth::check())
                        <div
                            class="col-lg-3 col-md-12 col-xl-2 text-left p-xl-0 d-flex align-items-center justify-content-end justify-content-md-center justify-content-lg-end ">
                            <div class="M001-login-c002 p-xl-0">
                                <div class="M001-c002-logins p-xl-0">
                                    @if (Auth::user()->is_customer == 0)
                                        <a href="{{ route('dashboard') }}">Dashboard</a>
                                    @else
                                        <span
                                            style="color: white; border-right: 2px solid white; padding-right: 15px;">{{ Auth::user()->name }}</span>
                                    @endif
                                    <a href="{{ route('logoutcustomer') }}">Logout</a>
                                </div>
                            </div>
                        </div>
                    @else
                        <div
                            class="col-lg-3 col-md-12 col-xl-2 text-left p-xl-0 d-flex align-items-center justify-content-end justify-content-md-center justify-content-lg-end ">
                            <div class="M001-login-c002 p-xl-0">
                                <div class="M001-c002-logins p-xl-0">
                                    <router-link to="/customerlogin">Login</router-link>
                                    <router-link to="/register">Register</router-link>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <!-- Mobile Navbar -->

        <div class="mobile-header">
            <div id="wrapper">
                <div class="overlay"></div>
                <!-- Sidebar -->
                <nav class="navbar navbar-inverse fixed-top d-block " id="sidebar-wrapper" role="navigation">
                    <ul class="nav sidebar-nav">
                        <div class="sidebar-header">
                            <div class="sidebar-brand">
                                <a href="./#/">ALMUMTAZ</a>
                            </div>
                        </div>
                        <h5 class="text-light w-100 p-2 pl-2 m-0">All Categories</h5>
                        <div id="mobile-nav"></div>
                    </ul>
                </nav>
                <!-- Page Content -->
                {{-- <div id="page-content-wrapper"> --}}
                {{-- <div class="row">
                        <div class="col-2">
                            <button type="button" class="hamburger animated fadeInLeft is-closed"
                                data-toggle="offcanvas">
                                <span class="hamb-top"></span>
                                <span class="hamb-middle"></span>
                                <span class="hamb-bottom"></span>
                            </button>
                        </div>
                        <div class="col-7 text-center">
                            <router-link to="/">
                                <img src="{{ asset('images/Al-Mumtaz.png') }}" alt="Al-Mumtaz" width="200px" />
                            </router-link>
                        </div>
                        <div class="col-3">
                            <div class="M001-cart-btn">
                                <div class="M001-cart-inner">
                                    <router-link to="/addtocart">
                                        <i class="fa fa-shopping-cart border-right pr-2"></i>
                                        <span class="pl-1">0</span>
                                    </router-link>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                {{-- </div> --}}
                <!-- /#page-content-wrapper -->
            </div>
            <!-- /#wrapper -->
            <div class="mobile_footer">
                <nav class="mobile-bottom-nav">
                    <div class="mobile-bottom-nav__item mobile-bottom-nav__item--active">
                        <div class="mobile-bottom-nav__item-content">
                            <a href="./#/">
                                <i class="fa fa-home"></i>
                                Home
                            </a>
                        </div>
                    </div>
                    <div class="mobile-bottom-nav__item">
                        <div class="mobile-bottom-nav__item-content">
                            <a href="./#/store">
                                <i class="fa fa-shopping-cart"></i>
                                Shop
                            </a>
                        </div>
                    </div>
                    <div class="mobile-bottom-nav__item">
                        <div class="mobile-bottom-nav__item-content">
                            <a href="./#/blog">
                                <i class="fab fa-blogger-b"></i>
                                Blogs
                            </a>
                        </div>
                    </div>
                    {{-- <div class="mobile-bottom-nav__item">
                        <div class="mobile-bottom-nav__item-content">
                            <a href="./#/deals">
                                <i class="fab fa-blogger-b"></i>
                                Deals
                            </a>
                        </div>
                    </div>
                    <div class="mobile-bottom-nav__item">
                        <div class="mobile-bottom-nav__item-content">
                            <a href="./#/newarrival">
                                <i class="fab fa-blogger-b"></i>
                                New Arrivals
                            </a>
                        </div>
                    </div>
                    <div class="mobile-bottom-nav__item">
                        <div class="mobile-bottom-nav__item-content">
                            <a href="./#/offers">
                                <i class="fab fa-blogger-b"></i>
                                New Offers
                            </a>
                        </div>
                    </div>

                    <div class="mobile-bottom-nav__item">
                        <div class="mobile-bottom-nav__item-content">
                            <a href="./#/aboutus">
                                <i class="fa fa-envelope"></i>
                                About Us
                            </a>
                        </div>
                    </div> --}}
                    <div class="mobile-bottom-nav__item">
                        <div class="mobile-bottom-nav__item-content">
                            <a href="./#/contactus">
                                <i class="fa fa-envelope"></i>
                                Contact Us
                            </a>
                        </div>
                    </div>
                    {{-- <div class="mobile-bottom-nav__item">
                        <div class="mobile-bottom-nav__item-content">
                            <a href="./#/feedback">
                                <i class="fa fa-envelope"></i>
                                Feedback
                            </a>
                        </div>
                    </div> --}}
                </nav>
            </div>
        </div>
</header>
