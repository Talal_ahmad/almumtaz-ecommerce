@extends('layout.master')
@section('content')
    <!-- All Records -->
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header row">
                    <h3 class="col-10">All Offers Items</h3>
                    {{-- Checking permission --}}
                    @can('Add Item')
                        <div class="col-2">
                            <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                                data-target="#add_new_modal">Add New</button>
                        </div>
                    @endcan
                    {{-- End Checking permission --}}
                </div>
                <div class="card-body">
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
                        style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Price</th>
                                <th>Discount Price</th>
                                <th>Net Amount</th>
                                <th>Item Type</th>
                                <th>Season</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Records -->
    <!-- Add Record -->
    <div id="add_new_modal" class="modal fade" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered modal-lg" permission="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add offers Item</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form id="add">
                    <div class="card-body" style="padding:0.9rem 1.25rem">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Name:</label>
                                    <input type="text" name="name" class="form-control" placeholder="Enter  Name"
                                        required />
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Price:</label>
                                    <input type="text" name="price" class="form-control" placeholder="Enter  Price"
                                        required />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Discount:</label>
                                    <input type="text" name="discount_price" class="form-control"
                                        placeholder="Enter Discount %" />
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Category:</label>
                                    <select name="category_id" id="" class="form-control">
                                        <option value="">--Select--</option>
                                        @foreach ($category as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Type:</label>
                                    <select name="item_type" id="" class="form-control">
                                        <option value="">--Select--</option>
                                        <option value="Onsale">Onsale</option>
                                        <option value="New Arrival">New Arrival</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Season:</label>
                                    <select name="season" id="" class="form-control">
                                        <option value="">--Select--</option>
                                        <option value="Autmn">Autmn</option>
                                        <option value="Winter">Winter</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Company:</label>
                                    <select name="company_id" class="form-control">
                                        <option value="">--Select--</option>
                                        @foreach ($company as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Display Image:</label>
                                    <input type="file" name="image" class="form-control" accept="image/*" required />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label for="permission_name">Description:</label>
                                <textarea name="description" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer form_footer text-right">
                        <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Add Record -->
    <!-- Edit Record -->
    <div id="edit_modal" class="modal fade" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered modal-lg" permission="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit OnSale</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form id="edit">
                    <div class="card-body" style="padding:0.9rem 1.25rem">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Name:</label>
                                    <input type="text" name="name" class="form-control" placeholder="Enter  Name"
                                        id="editname" required />
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Price:</label>
                                    <input type="text" name="price" class="form-control" placeholder="Enter  Price"
                                        id="editprice" required />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Discount Price:</label>
                                    <input type="text" id="editdiscount_price" name="discount_price" class="form-control" placeholder="Enter  Price"
                                        required />
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Category:</label>
                                    <select name="category_id" id="editcategory_id" class="form-control">
                                        <option value="">--Select--</option>
                                        @foreach ($category as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Type:</label>
                                    <select name="item_type" id="edit_type" class="form-control">
                                        <option value="Onsale">Onsale</option>
                                        <option value="New Arrival">New Arrival</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Season:</label>
                                    <select name="season" id="editseason" class="form-control">
                                        <option value="Autmn">Autmn</option>
                                        <option value="Winter">Winter</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Company:</label>
                                    <select name="company_id" class="form-control" id="editcompany">
                                        @foreach ($company as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Display Image:</label>
                                    <input type="file" name="image" class="form-control" accept="image/*" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label for="permission_name">Description:</label>
                                <textarea name="description" class="form-control" id="edittext" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer form_footer text-right">
                        <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Edit Record -->
@endsection
@section('scripts')
    <script>
        var datatable;
        var rowid;
        $(document).ready(function() {
            // Fetching Records
            datatable = $("#datatable").DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: false,
                ajax: {
                    url: "{{ url('get/onsale/list') }}",
                    method: "get",
                },
                columns: [{
                        data: "name"
                    },
                    {
                        data: "image_path"
                    },
                    {
                        data: "price"
                    },
                    {
                        data: "discount_price",
                        render:function(data){
                            return data + "%" ;
                        },
                    },
                    {
                        data: "net_amount"
                    },
                    {
                        data: "item_type"
                    },
                    {
                        data: "season"
                    },
                    {
                        data: "description"
                    },
                    {
                        data: "action"
                    },
                ],

            });
            // End Fetching Records
            // Adding Record
            $("#add").submit(function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ route('onsale.store') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(success) {
                        if (success.errors) {
                            $.each(success.errors, function(index, value) {
                                toastr.error(value);
                            });
                        } else if (success.error_message) {
                            toastr.error(
                                'An error has been occured! Please Contact Administrator.');
                        } else {
                            datatable.ajax.reload();
                            document.getElementById("add").reset();
                            $(".select2").val("").trigger("change");
                            $("#add_new_modal").modal("hide");
                            toastr.success('Item has been Added Successfully!');
                        }
                    },
                    error: function(response) {
                        toastr.error('Error! Item could not be created due to some reasons.');
                    },
                });
            });
            // End Adding Record
            // Updating Record
            $("#edit").submit(function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ url('onsale') }}" + "/" + rowid,
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(success) {
                        if (success.errors) {
                            $.each(success.errors, function(index, value) {
                                toastr.error(value);
                            });
                        } else if (success.error_message) {
                            toastr.error(
                                'An error has been occured! Please Contact Administrator.');
                        } else {
                            datatable.ajax.reload();
                            $("#edit_modal").modal("hide");
                            toastr.success('Item has been Updated Successfully!');
                        }
                    },
                    error: function(response) {
                        toastr.error('Error! Item could not be updated due to some reasons.');
                    },
                });
            });
            // End Updating Record
            // Initializing Select2
            $(".select2").select2({
                placeholder: "Select Company",
                closeOnSelect: true,
                allowClear: true,
            });
            // End Initializing Select2
        });
        // Fetching existing data to edit
        function edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('onsale') }}" + "/" + id + "/edit",
                type: "get",
                success: function(success) {
                    $("#edittext").empty();
                    $("#editname").val(success.name);
                    $("#editprice").val(success.price);
                    $("#editdiscount_price").val(success.discount_price);
                    $("#editcategory_id").val(success.category_id).select2({
                        placeholder: "Select Category",
                        closeOnSelect: true,
                        allowClear: true,
                    });
                    $("#edit_type").val(success.item_type);
                    $("#editseason").val(success.season);
                    $("#edittext").append(success.description);
                    $("#editcompany").val(success.company_id).select2({
                        placeholder: "Select Company",
                        closeOnSelect: true,
                        allowClear: true,
                    });
                    $("#edit_modal").modal("show");
                },
            });
        }
        // End Fetching existing data to edit
        // Deleting Record
        function deleterow(id) {
            if (confirm("Do you really want to delete this Item?")) {
                $.ajax({
                    url: "{{ url('onsale') }}" + "/" + id,
                    type: "delete",
                    data: {
                        _token: "{{ csrf_token() }}",
                    },
                    datatype: "JSON",
                    success: function(success) {
                        if (success.error_message) {
                            toastr.error('An error has been occured! Please Contact Administrator.');
                        } else {
                            datatable.ajax.reload();
                            toastr.success('Item has been deleted Successfully!');
                        }
                    },
                    error: function(response) {
                        toastr.error('Error! Item could not be deleted due to some reasons.');
                    },
                });
            }
        }
        // End Deleting Record
    </script>
@endsection
