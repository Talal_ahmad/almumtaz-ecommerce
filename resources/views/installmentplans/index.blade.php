@extends('layout.master') 
@section('content')
<!-- All Records -->
<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header row">
                <h3 class="col-10">All Plans</h3>
                {{-- Checking permission --}}
                @can('Add Installment Plan')
                <div class="col-2">
                    <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#addModal">Add New</button>
                </div>
                @endcan
                {{-- End Checking permission --}}
            </div>
            <div class="card-body">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Months</th>
                            <th>Profit Rate</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End All Records -->
<!-- Add Record -->
<div class="modal fade" id="addModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Plan</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form id="add">
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" name="name" class="form-control" placeholder="Enter Name" required />
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="months">Months:</label>
                                <select close-on-select="false" class="form-control select2" name="months" id="months" required>
                                    <option value=""></option>
                                    <option value="3">3 Months</option>
                                    <option value="6">6 Months</option>
                                    <option value="9">9 Months</option>
                                    <option value="12">12 Months</option>
                                    <option value="15">15 Months</option>
                                    <option value="18">18 Months</option>
                                    <option value="21">21 Months</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="interest_rate">Profit Rate:</label>
                                <input type="text" name="interest_rate" class="form-control" placeholder="Enter Profit Rate %" required />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <h4>Option 1: <small>Advance Based</small></h4>
                        </div>
                        <div class="col">
                            <h4>Option 2: <small>1st Installment as Advance</small></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="advance_deduction" id="advance_deduction" value="1" />
                                <label for="advance_deduction"><small>Advance deduction from price to calculate interest</small></label>
                            </div>
                        </div>
                        <div class="col">
                            <small>Already Included</small>
                        </div>
                    </div>
                    <div class="row" id="advance_ratediv" style="display: none">
                        <div class="col-6">
                            <label for="advance_rate">Minimum Advance Rate:</label>
                            <input type="text" name="min_advance_rate" class="form-control" placeholder="Enter Advance Rate %" required />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Add Record -->
<!-- Edit Record -->
<div class="modal fade" id="editModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Plan</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form id="edit">
                <div class="modal-body">
                    @csrf @method ('PUT')
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" name="name" class="form-control" id="editname" placeholder="Enter Name" required />
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="months">Months:</label>
                                <select close-on-select="false" class="form-control select2" name="months" id="editmonths" required>
                                    <option value="3">3 Months</option>
                                    <option value="6">6 Months</option>
                                    <option value="9">9 Months</option>
                                    <option value="12">12 Months</option>
                                    <option value="15">15 Months</option>
                                    <option value="18">18 Months</option>
                                    <option value="21">21 Months</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="interest_rate">Profit Rate:</label>
                                <input type="text" name="interest_rate" class="form-control" id="editinterest_rate" required />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <h4>Option 1: <small>Advance Based</small></h4>
                        </div>
                        <div class="col">
                            <h4>Option 2: <small>1st Installment as Advance</small></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="advance_deduction" id="editadvance_deduction" value="1" />
                                <label for="advance_deduction"><small>Advance deduction from price to calculate interest</small></label>
                            </div>
                        </div>
                        <div class="col">
                            <small>Already Included</small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" id="editadvance_ratediv">
                            <label for="advance_rate">Minimum Advance Rate %:</label>
                            <input type="text" name="min_advance_rate" class="form-control" id="editadvance_rate" placeholder="Enter Advance Rate %" required />
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="status">Status:</label>
                                <select name="status" id="editstatus" class="form-control" required>
                                    <option value="1">Activate</option>
                                    <option value="0">Deactivate</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Edit Record -->
@endsection 
@section('scripts')
<script>
    var datatable;
    var rowid;
    $(document).ready(function () {
        // Fetching Records
        datatable = $("#datatable").DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            ajax: {
                url: "{{url('installmentplan')}}",
                method: "get",
            },
            columns: [
                { data: "name"},
                {
                    data: "months",
                    render: function (data) {
                        return data + " Months";
                    },
                },
                {
                    data: "interest_rate",
                    render: function (data) {
                        return data + "%";
                    },
                },
                { data: "status"},
                { data: "action"},
            ],
        });
        // End Fetching Records
        // Adding Record
        $("#add").submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{route('installmentplan.store')}}",
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (success) {
                    if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        document.getElementById("add").reset();
                        $(".select2").val("").trigger("change");
                        $("#addModal").modal("hide");
                        toastr.success('Plan has been Added Successfully!');
                    }
                },
                error: function (response) {
					toastr.error('Error! Plan could not be created due to some reasons.');
				},
            });
        });
        // End Adding Record
        // Updating Record
        $("#edit").submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{url('installmentplan')}}" + "/" + rowid,
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (success) {
                    if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        $("#editModal").modal("hide");
                        toastr.success('Plan has been updated Successfully!');
                    }
                },
                error: function (response) {
					toastr.error('Error! Plan could not be updated due to some reasons.');
				},
            });
        });
        // End Updating Record
        // Showing & Hiding Minimum advance rate field based on advance deduction checkbox
        $("#advance_deduction").change(function() {
            if($(this).is(':checked')){
                $("#advance_ratediv").show(400);
            }
            else{
                $("#advance_ratediv").hide(400);
            }
        });
        $("#editadvance_deduction").change(function() {
            if($(this).is(':checked')){
                $("#editadvance_ratediv").show(400);
            }
            else{
                $("#editadvance_ratediv").hide(400);
            }
        });
        // End Showing & Hiding Minimum advance rate field based on advance deduction checkbox
        // Initializing Select2
        $(".select2").select2({
            placeholder: "Select Months",
        });
        // End Initializing Select2
    });
    // Fetching existing data to edit
    function edit(id) {
        rowid = id;
        $.ajax({
            url: "{{url('installmentplan')}}" + "/" + id + "/edit",
            type: "get",
            success: function (success) {
                $("#editname").val(success.name);
                $("#editmonths").val(success.months).select2({
                    placeholder: "Select Months",
                });
                $("#editinterest_rate" ).val( success.interest_rate);
                if(success.advance_deduction == 1){              
                    $("#editadvance_deduction" ).prop( "checked", true );
                    $("#editadvance_rate").val(success.min_advance_rate);
                    $("#editadvance_ratediv").show(400);
                }
                else{
                    $("#editadvance_deduction" ).prop( "checked", false );
                    $("#editadvance_ratediv").hide(400);
                }
                $("#editstatus").val(success.status);
                $("#editModal").modal("show");
            },
        });
    }
    // End Fetching existing data to edit
    // Deleting Record
    function deleterow(id) {
        if (confirm("Do you really want to delete this plan?")) {
            $.ajax({
                url: "{{url('installmentplan')}}" + "/" + id,
                type: "delete",
                data: {
                    _token: "{{ csrf_token() }}",
                },
                datatype: "JSON",
                success: function (success) {
                    if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        toastr.success('Plan has been deleted Successfully!');
                    }
                },
                error: function (response) {
					toastr.error('Error! Plan could not be deleted due to some reasons.');
				},
            });
        }
    }
    // End Deleting Record
</script>
@endsection
