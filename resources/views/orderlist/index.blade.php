@extends('layout.master')
@section('content')

    <!-- All Records -->
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header row">
                    <h3 class="col-10">Order list</h3>
                </div>
                <div class="card-body">
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
                        style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Customer name</th>
                                <th>city</th>
                                <th>Address</th>
                                <th>payment_method</th>
                                <th>Total_items</th>
                                <th>Total_amount</th>
                                <th>Status</th>
                                <th>action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="item_detail_model">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header bg-dark">
              <h4 class="modal-title">Item Details</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
              <table class="table table-bordered table-striped">
                  <thead>
                      <tr>
                          <th>Sr.No</th>
                          <th>Item Name</th>
                          <th>Quantity</th>
                          <th>discount</th>
                          <th>Price</th>
                          <th>Net Amount</th>
                      </tr>
                  </thead>
                  <tbody id="order_details">

                  </tbody>
              </table>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

          </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            // Fetching Records
            datatable = $("#datatable").DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: false,
                ajax: {
                    url: "{{ url('order_detail') }}",
                    method: "get",
                },
                columns: [{
                        data: "id"
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "city"
                    },
                    {
                        data: "delivery_address"
                    },
                    {
                        data: "payment_method"
                    },

                    {
                        data: "total_items"
                    },
                    {
                        data: "total_amount"
                    },
                    {
                        data: "status"
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
            });
        });

        function item_details(order_id)
        {
            $.ajax({
            url: "{{url('order_detail')}}" + "/" + order_id,
            type: "get",
            success: function (result) {
                $('#order_details').empty();
                var count = 1;
                $.each(result,function(index,data){
                    $('#order_details').append(`<tr>
                    <td>${count++}</td>
                    <td>${data.name}</td>
                    <td>${data.quantity}</td>
                    <td>${data.discount}%</td>
                    <td>${data.price}</td>
                    <td>${data.net_amount}</td>
                    </tr>`)
                })
                $("#item_detail_model").modal("show");
            },
        });
        }

        $(document).on('click','.process',function(e){
            e.preventDefault();
            var process = $(this).data('id');
            $.ajax({
                type: "POST",
                url:"{{url('orderstatuss')}}",
                data :{
                processed : process,
                _token: $("input[name=_token]").val()
            },
            success:function(success){
                if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        toastr.success('Order has been Placed!');
                    }
            }
            });
        });

        $(document).on('click','.cancel',function(e){
            e.preventDefault();
            var cancel = $(this).data('id');
            $.ajax({
                type: "POST",
                url:"{{url('orderstatuss')}}",
                data :{
                    cancel : cancel,
                _token: $("input[name=_token]").val()
            },
            success:function(success){
                if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        toastr.success('Order has been Cancle!');
                    }
            }
            });
        })

    </script>
@endsection
