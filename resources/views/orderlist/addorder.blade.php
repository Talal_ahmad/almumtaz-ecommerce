
@extends('layout.master')

@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header row">
                <h3 class="col-10">Add Order</h3>
            </div>
            <div class="card-body">
                <div class="row mb-4">
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                            Add New Customer
                        </button>
                    </div>
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4" style="postition:relative;">
                        <form id="search_customer">
                            @csrf
                        <div class="input-group">
                            <input type="search"  id="search_keyword" name="search_keyword" class="form-control select2 " placeholder="search by Name,Phone,Email" autocomplete="off" required />
                            <button type="button" id="search_button" class="btn btn-primary">
                                <i class="fas fa-search "></i>
                            </button>
                        </div>
                        </form>
                        <div style="position:absolute; z-index:1; width:100%;" id="searchcustomer" >
                                <ul class="list-unstyled bg-light" id="myUl">
                                </ul>
                            </div>
                    </div>
                </div>
                <form  id="order_now">
                    @csrf
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="customer">Customers</label>
                                <select name="customer" id="customer" class="form-control" onchange="get_customer(this, false)" required>
                                    <option value=""></option>
                                    @foreach($users as $user)
                                    <option value="{{$user->id}}" {{$user->id == request()->query('cust_id') ? 'selected' : ''}}>{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="product">Products</label>
                                <select name="product" id="product" class="form-control" required>
                                    <option value=""></option>
                                    @foreach($items as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="">Quantity</label>
                                <input type="numeric" name="quantity" value="0" id="quantity" class="form-control" placeholder="Quantity" required />
                            </div>
                        </div>
                        <div class="col-lg-2" id="hide_delivery">
                            <div class="form-group">
                                <label for="">Delivery Charges</label>
                                <input type="number" name="delivery_charges" id="delivery_charges" class="form-control" placeholder="Delivery Charges" required/>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <button type="button" class="btn btn-primary" style="margin-top: 32px;" id="addProduct">Add</button>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="productTable">
                                    <thead>
                                        <th>Sr#</th>
                                        <th>Name</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Discount</th>
                                        <th>Total Price</th>
                                        <th>Action</th>
                                    </thead>

                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-lg-8"></div>
                                <div class="col-lg-4">
                                    <div class="row">
                                        <div class="col-6">
                                            <p>Gross Amount</p>
                                        </div>
                                        <div class="col-6">
                                            <p id="gross_amount"></p>
                                        </div>

                                        <div class="col-6">
                                            <p>Discount Amount</p>
                                        </div>
                                        <div class="col-6">
                                            <p id="gross_discount"></p>
                                        </div>
                                        <div class="col-6">
                                            <p>Delivery Charges</p>
                                        </div>
                                        <div class="col-6">
                                            <p id="charges"></p>
                                        </div>
                                        <div class="col-12">
                                            <hr class="bg-light" />
                                        </div>

                                        <div class="col-6">
                                            <p class="font-weight-bold">Total</p>
                                        </div>
                                        <div class="col-6">
                                            <p id="gross_total"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Branches</label>
                                <select name="branch" id="branch" class="form-control">
                                    <option value=""></option>
                                    @foreach($branches as $branch)
                                    <option value="{{$branch->id}}">{{$branch->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="customer-name">Customer Name</label>
                                <input type="text" name="customer-name" id="customer-name" class="form-control" value="{{!empty($customer_details) ? $customer_details->name : ''}}" placeholder="Customer Name" />
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Number</label>
                                <input type="text" name="customer-number" id="customer-number" class="form-control" value="{{!empty($customer_details) ? $customer_details->phone_number : ''}}" placeholder="Number" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="">Country</label>
                                <input type="text" name="customer-country" id="customer-country" class="form-control" placeholder="Country" />
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="">City</label>
                                <input type="text" name="customer-city" id="customer-city" class="form-control" placeholder="City/Province" />
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="">Postal Code</label>
                                <input type="text" name="postal-code" id="postal-code" class="form-control" placeholder="Postal Code" />
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="">House No/Street/Town</label>
                                <input type="text" name="customer-address" id="customer-address" class="form-control" placeholder="House No/Street/Town" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <label for="payment">Payment Method</label>
                            <select name="payment" id="payment" class="custom-select">
                                <option value="">Select Payment Option</option>
                                <option value="card">Card Payment</option>
                                <option value="cash">Cash on Deliver</option>
                            </select>
                        </div>
                        <div class="col-lg-3" id="card" style="display: none;">
                            <div class="form-group">
                                <label for="card_number">Card Number</label>
                                <input type="text" name="card_number" id="card_number" class="form-control" placeholder="Card Number" />
                            </div>
                        </div>
                        <div class="col-lg-5" id="expiry_date" style="display: none;">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Expiry Date</label>
                                        <select name="month" id="month" class="custom-select">
                                            <option value="">Select Month</option>
                                            <option value="01">Janurary</option>
                                            <option value="02">Feburary</option>
                                            <option value="03">March</option>
                                            <option value="04">April</option>
                                            <option value="05">May</option>
                                            <option value="06">June</option>
                                            <option value="07">July</option>
                                            <option value="08">August</option>
                                            <option value="09">September</option>
                                            <option value="10">Octobar</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select name="year" id="year" type="date" class="custom-select" style="margin-top: 32px;">
                                            <option value="">Select Year</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="cvv" id="cvv" class="form-control" placeholder="CVV" style="margin-top: 32px;" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                
                                <button type="submit" class="btn btn-primary" style="margin-top: 32px;">Order Now</button>
                            </div>
                        </div>
                    </div>

                 
                </form>
            </div>
        </div>
    </div>
</div>
   

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Add Customer</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form id="add_customer">
                                   @csrf
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="">Name</label>
                                            <input type="text" name="first_name" id="customerName" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <label for="">Last Name</label>
                                            <input type="text" name="last_name" id="customerLName" class="form-control" />
                                        </div>

                                        <div class="form-group">
                                            <label for="">Phone No</label>
                                            <input type="number" name="phone_number" id="customerNumber" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <label for="">Email</label>
                                            <input type="email" name="email" id="customerEmail" class="form-control" />
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                  
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
@endsection
@section('scripts')

<script>
    var selectedItems = [];
    $(document).ready(function () {
        $("#customer").select2({
            placeholder: "Select a Customer",
            allowClear: true,
        });
        $("#product").select2({
            placeholder: "Select a Products",
            allowClear: true,
        });
        $("#branch").select2({
            placeholder: "Select a Branch",
            allowClear: true,
        });

        $("#payment").on("change", function () {
            var payment = $(this).val();
            if (payment == "card") {
                $("#card").show(300);
                $("#expiry_date").show(300);
            } else {
                $("#card").hide(300);
                $("#expiry_date").hide(300);
            }
        });

        $("#search_button").on('click',function(e){
            e.preventDefault(e);
            
            $.ajax({
                url: "search_customer",
                type: "post",
                data: {
                    searchkeyowrd : $("#search_keyword").val(), 
                    _token: "{{ csrf_token() }}",

                },
                success: function(response){
                  
                    if(response.length < 1){
                        $("#myUl").html('<li class="p-2">No record found</li>');
                    }
                    else{
                        $("#myUl").empty();
                        $('#myUl').show();
                        $.each(response, function(index, res){
                            $("#myUl").append('<li class="p-2 border" id="li_' + res.id + ' " onclick="get_customer(false,' + res.id + ')">' + res.name + '</li>');
                        });
                    }
                },
            });
        });

          
        $("#addProduct").click(function(e){
            $.ajax({
                url: "getproduct",
                type: "post",
                data: {
                    product_id: $("#product").val(),
                    _token: "{{ csrf_token() }}",
                },
                success: function (response) {
                    response.quantity = $("#quantity").val();
                    response.net_amount = response.total_price * $("#quantity").val();
                    selectedItems.push(response);
                    appendRows();
                },
            });

        })
    
        
        $("#add_customer").submit(function (e){
            e.preventDefault(e);
               $.ajax({
                url: "customer",
                type: "post",
                data: {
                    first_name : $("#customerName").val(),
                    last_name: $("#customerLName").val(),
                    phone_number: $("#customerNumber").val(),
                    email: $("#customerEmail").val(),
                    _token: "{{ csrf_token() }}",
                },
                success: function(response){
                    if(response.error){
                        $.each(response.error, function(index,value){
                            toastr.error(value);

                        });
                    }
                    else if(response.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else
                    {
                        document.getElementById("add_customer").reset();
                        $("#exampleModal").modal("hide");
                        toastr.success('Customer has been Added Successfully!');
                    }
                    
                },
                error: function (response) {
					toastr.error('Error! Permission could not be created due to some reasons.');
				},

            });

        });

        $("#order_now").submit(function (e){
            e.preventDefault(e);
            $.ajax({
                url:"manual_detail",
                type: "post",
                data: {
                    id : {{request()->query('id')}},
                    user_id: $("#customer").val(),
                    items: selectedItems,
                    delivery_charges : $("#delivery_charges").val(),
                    gross_amount: $("#gross_amount").html(),
                    total_discount: $("#gross_discount").html(),
                    total_amount:  $("#gross_total").html(),
                    branch_id: $("#branch").val(),
                    phone_number: $("#customer-number").val(),
                    country: $("#customer-country").val(),
                    city: $("#customer-city").val(),
                    postal_code: $("#postal-code").val(),
                    delivery_address: $("#customer-address").val(),
                    payment_method: $("#payment").val(),
                    total_items: selectedItems.length,
                    card_number: $("#card_number").val(),
                    year: $("#year").val(),
                    month: $("#month").val(),
                    cvv_number: $("#cvv").val(),
                    _token: "{{ csrf_token() }}",
                },
                success:function(response)
                {
                    toastr.success('Order has been placed Successfully!');
                    setTimeout(function(){
                        window.location.href = 'order_detail';
                     }, 1500);
                    
                }
            })
        })

        
    });

    function get_customer(object,id)
    {

        if(id != '')
        {
            $('#search_keyword').val('');
            var customer_id = id;
            $('#customer').val(id);
            $('#customer').trigger('change');
            $('#myUl').hide();
        }
        else
        {
            var customer_id = object.value;
        }
        $.ajax({
            url: "getuserdetail",
            type: "post",
            data: {
                customer_id: customer_id,
                _token: "{{ csrf_token() }}",
            },
            success: function (response) {
                $("#customer-name").val(response.name);
                $("#customer-number").val(response.phone_number);
            },
        });
    }
    function appendRows() {
        var gross_amount = 0;
        var net_discount = 0;
        var total = 0;
        var rows = "";
        $.each(selectedItems, function (index, data) {
            gross_amount += Number(data.total_price) * Number(data.quantity);
            net_discount += (Number(data.price * data.quantity) * Number(data.discount)) / 100;
            total = Number(gross_amount) + Number($("#delivery_charges").val());
            rows += `                <tr>
                                        <td>${(index = index + 1)}</td>
                                        <td>${data.name}</td>
                                        <td>${data.quantity}</td>
                                        <td>${data.price}</td>
                                        <td>${data.discount}%</td>
                                        <td>${data.total_price * data.quantity}</td>
                                        <td><button type="button"  class="btn btn-danger" onclick="removeItem(${index})"><i class="fa fa-times" aria-hidden="true"></i></button></td>
                                    </tr>
                    `;
        });
        $("tbody").html(rows);
        $("#gross_amount").html(gross_amount);
        $("#gross_discount").html(net_discount);
        $("#charges").html($("#delivery_charges").val());
        $("#gross_total").html(total);
    }

    function removeItem(index) {
        selectedItems.splice(index-1, 1);
        appendRows();
    }


</script>

@endsection
