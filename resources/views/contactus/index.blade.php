@extends('layout.master')
@section('content')

    <!-- All Records -->
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header row">
                    <h3 class="col-10">All Contact Us </h3>
                </div>
                <div class="card-body">
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
                        style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Message</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        var datatable;
        var rowid;
        $(document).ready(function() {
            // Fetching Records
            datatable = $("#datatable").DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: false,
                ajax: {
                    url: "{{ url('contactus') }}",
                    method: "get",
                },
                columns: [{
                        data: "id"
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "email"
                    },
                    {
                        data: "phone"
                    },
                    {
                        data: "message"
                    },
                ],

            });
        });


    </script>
@endsection
