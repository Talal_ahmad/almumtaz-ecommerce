@extends('layout.master') 
@section('content')
<!-- All Records -->
<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header row">
                <h3 class="col-8">All Roles</h3>
                {{-- Checking permission --}}
                <div class="col text-right">
                    @can('Add Setting')
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#addModal">Add Role</button>
                    @endcan
                    @can('View Settings')
                    <a href="{{route('users.index')}}" class="btn btn-primary">Users</a>
                    <a href="{{route('permissions.index')}}" class="btn btn-success">Permissions</a>
                    @endcan
                </div>
                {{-- End Checking permission --}}
            </div>
            <div class="card-body">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End All Records -->
<!-- Add Record -->
<div class="modal fade" id="addModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Role</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form id="add">
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" name="name" class="form-control" placeholder="Enter Name" required />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="permissions">Assign Permissions:</label>
                                <select multiple="true" class="form-control select2" name="permissions[]" required>
                                    @foreach($permissions as $key => $value)
                                      <option value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Add Record -->
<!-- Edit Record -->
<div class="modal fade" id="editModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Role</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form id="edit">
                <div class="modal-body">
                    @csrf 
                    @method ('PUT')
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" name="name" id="editname" class="form-control" required />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="permissions">Assign Permissions:</label>
                                <select multiple="true" class="form-control select2" name="permissions[]" id="editpermissions" required>
                                    @foreach($permissions as $key => $value)
                                      <option value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Edit Record -->
@endsection 
@section('scripts')
<script>
    var datatable;
    var rowid;
    $(document).ready(function () {
        // Fetching Records
        datatable = $("#datatable").DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            ajax: {
                url: "{{url('roles')}}",
                method: "get",
            },
            columns: [
                { data: "name" },
                { data: "action" },
            ],
        });
        // End Fetching Records
        // Adding Record
        $("#add").submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{route('roles.store')}}",
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (success) {
                    if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        document.getElementById("add").reset();
                        $(".select2").val("").trigger("change");
                        $("#addModal").modal("hide");
                        toastr.success('Role has been Created Successfully!');
                    }
                },
                error: function (response) {
					toastr.error('Error! Role could not be created due to some reasons.');
				},
            });
        });
        // End Adding Record
        // Updating Record
        $("#edit").submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{url('roles')}}" + "/" + rowid,
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (success) {
                    if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        $("#editModal").modal("hide");
                        toastr.success('Role has been Updated Successfully!');
                    }
                },
                error: function (response) {
					toastr.error('Error! Role could not be updated due to some reasons.');
				},
            });
        });
        // End Updating Record
        // Initializing Select2
        $(".select2").select2({
            placeholder: "Select Permissions",
            closeOnSelect: true,
            allowClear: true,
        });
        // End Initializing Select2
    });
    // Fetching existing data to edit
    function edit(id) {
        rowid = id;
        $.ajax({
            url: "{{url('roles')}}" + "/" + id + "/edit",
            type: "get",
            success: function (success) {
                $("#editname").val(success.role.name);
                $("#editpermissions").val(success.permissions).select2();
                $("#editModal").modal("show");
            },
        });
    }
    // End Fetching existing data to edit
    // Deleting Record
    function deleterow(id) {
        if (confirm("Do you really want to delete this Role?")) {
            $.ajax({
                url: "{{url('roles')}}" + "/" + id,
                type: "delete",
                data: {
                    _token: "{{ csrf_token() }}",
                },
                datatype: "JSON",
                success: function (success) {
                    if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        toastr.success('Role has been Deleted Successfully!');
                    }
                },
                error: function (response) {
					toastr.error('Error! Role could not be deleted due to some reasons.');
				},
            });
        }
    }
    // End Deleting Record
</script>
@endsection
