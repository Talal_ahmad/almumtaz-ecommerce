@extends('layout.master')
@section('content')

    <!-- All Records -->
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header row">
                    <h3 class="col-10">All Stocks</h3>
                </div>
                <div class="card-body">
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
                        style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Item</th>
                                <th>Company</th>
                                <th>Quantity</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- End All Records -->

    <!-- Edit Record -->
    <div id="editmodal" class="modal fade" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered modal-lg" permission="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Stock</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form id="edit">
                    <div class="card-body" style="padding:0.9rem 1.25rem">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="item">Item:</label>
                                    <input type="text" name="edit_item" class="form-control" id="edit_item" required />
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="company">Company:</label>
                                    <input type="text" name="edit_company" class="form-control" id="edit_company" required />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="quantity">Quantity:</label>
                                    <input type="text" name="edit_quantity" class="form-control" id="edit_quantity" required />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer form_footer text-right">
                        <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Edit Record -->
@endsection
@section('scripts')
    <script>
        var datatable;
        var rowid;
        $(document).ready(function() {
            // Fetching Records
            datatable = $("#datatable").DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: false,
                ajax: {
                    url: "{{ url('stock') }}",
                    method: "get",
                },
                columns: [{
                        data: "id"
                    },
                    {
                        data: "item_id"
                    },
                    {
                        data: "company_id"
                    },
                    {
                        data: "quantity"
                    },
                    {
                        data: "action"
                    },
                ],

            });
            $('#edit').on('submit',function(e){
                e.preventDefault();
                var form = $('#edit').serialize();
                $.ajax({
                    type: "PUT",
                    url: "{{url('stock')}}" + "/" + rowid,
                    data: form,
                    success:function(success){
                        if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        $('#editmodal').modal('hide');
                        toastr.success('Stock has been Updated Successfully!');
                    }
                    },
                });
            });
        });
        $(document).on('click', '.dlt', function() {
                var id = $(this).data('id');
                if (confirm("Do you really want to delete this Item?")) {
                $.ajax({
                    type: "DELETE",
                    url: "stock/" + id,
                    data: {
                        id: id,
                        _token: $("input[name=_token]").val()
                    },
                    success: function(success) {
                        if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        toastr.success('Stock has been deleted Successfully!');
                    }

                    },
                });
            }
            });
            function edit(id){
                rowid = id;
                $.ajax({
                    type: "GET",
                    url: "{{url('stock')}}" +"/" + id + "/edit",
                    success:function(response){
                        $('#edit_item').val(response.item_id);
                        $('#edit_company').val(response.company_id);
                        $('#edit_quantity').val(response.quantity);
                        $('#editmodal').modal('show');
                    }
                });
            }

    </script>
@endsection
