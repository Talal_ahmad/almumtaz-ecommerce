@extends('layout.master') 
@section('content')
<!-- All Records -->
<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header row">
                <h3 class="col-10">Footer Images</h3>
                {{-- Checking permission --}}
                @can('Footer Image')
                <div class="col-2">
                    <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#add_new_modal">Add New</button>
                </div>
                @endcan
                {{-- End Checking permission --}}
            </div>
            <div class="card-body">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End All Records -->
<!-- Add Record -->
<div id="add_new_modal" class="modal fade" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" permission="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Footer Image</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="add">
                <div class="card-body">
                    @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <!--To give the control a modern look, I have applied a stylesheet in the parent span.-->
                                <span class="btn btn-success btnrelated fileinput-button">
                                    <span>Select Attachment</span>
                                    <input type="file" name="image_file[]" id="files" multiple><br />
                                </span>
                                <output id="Filelist"></output>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="banner">Banner Image:</label>
                                <input type="file" name="banner_file" class="form-control" accept="image/*" required/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer form_footer text-right">
                    <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Add Record -->
<!-- Edit Record -->
<div id="edit_modal" class="modal fade" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" permission="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update Image</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form class="editform">
                <div class="card-body">
                    @csrf 
                    @method('PUT')
                
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="edit_image">Display Image:</label>
                                <input type="file" name="image_file" class="form-control" accept="image/*" multiple required/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="edit_image">Banner Image:</label>
                                <input type="file" name="banner_file" class="form-control" accept="image/*" required/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer form_footer text-right">
                    <button type="submit" class="btn btn-success" id="updatebutton"><i class="far fa-save"></i> Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Edit Record -->
@endsection 
@section('scripts')
<script>
    var datatable;
    var rowid;
    $(document).ready(function () {
        // Fetching Records
        datatable = $("#datatable").DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            ajax: {
                url: "{{url('footerimage')}}",
                method: "get",
            },
            columns: [
                { data: "image", "render": function (data){
                    return '<img src="'+data+'" style="height:50px;width:50px;"/>'
                }},
                { data: "action"},
            ],
        });
        // End Fetching Records
        // Adding Record
        $("#add").submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{route('footerimage.store')}}",
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                responsive: true,
                success: function (success) {
                    if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        document.getElementById("add").reset();
                        $("#add_new_modal").modal("hide");
                        toastr.success('Images has been Added Successfully!');
                    }
                    
                },
                error: function (response) {
					toastr.error('Error! Images could not be created due to some reasons.');
				},
            });
        });
        // End Adding Record
        // Updating Record
        $(".editform").on("submit", function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{url('footerimage')}}" + "/" + rowid,
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (success) {
                    if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        $(".editform").off("submit", "**");
                        datatable.ajax.reload();
                        $("#edit_modal").modal("hide");
                        toastr.success('Images has been Updated Successfully!');
                    }
                    
                },
                error: function (response) {
					toastr.error('Error! Category could not be updated due to some reasons.');
				},
            });
        });
        // End Updating Record
        // Initializing Select2
        // $(".select2").select2({
        //     placeholder: "Select Installment Plans",
        //     closeOnSelect: true,
        //     allowClear: true,
        // });
        // End Initializing Select2
    });
    // Fetching existing data to edit
    function edit(id) {
        rowid = id;
        $.ajax({
            url: "{{url('footerimage')}}" + "/" + id + "/edit",
            type: "get",
            success: function (success) {
               
           
                $("#edit_modal").modal("show"); 
            },
        });
    }
    // End Fetching existing data to edit
    // Deleting Record
    function deleterow(id) {
        
        if (confirm("Do you really want to delete this Image?")) {
            $.ajax({
                url: "{{url('footerimage')}}" + "/" + id,
                type: "delete",
                data: {
                    _token: "{{ csrf_token() }}",
                },
                success: function (success) {
                    if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        toastr.success('Image has been Deleted Successfully!');
                    }
                },
                error: function (response) {
					toastr.error('Error! Image could not be deleted due to some reasons.');
				},
            });
        }
    }
    // End Deleting Record



               //I added event handler for the file upload control to access the files properties.
document.addEventListener("DOMContentLoaded", addinit, false);

//To save an array of attachments
var AttachmentArray = [];

//counter for attachment array
var arrCounter = 0;

//to make sure the error message for number of files will be shown only one time.
var filesCounterAlertStatus = false;

//un ordered list to keep attachments thumbnails
var ul = document.createElement("ul");
ul.className = "thumb-Images";
ul.id = "imgList";

function addinit() {
  //add javascript handlers for the file upload event
  document
    .querySelector("#files")
    .addEventListener("change", addhandleFileSelect, false);
}

//the handler for file upload event
function addhandleFileSelect(e) {
  //to make sure the user select file/files
  if (!e.target.files) return;

  //To obtaine a File reference
  var files = e.target.files;

  // Loop through the FileList and then to render image files as thumbnails.
  for (var i = 0, f; (f = files[i]); i++) {
    //instantiate a FileReader object to read its contents into memory
    var fileReader = new FileReader();

    // Closure to capture the file information and apply validation.
    fileReader.onload = (function(readerEvt) {
      return function(e) {
        //Apply the validation rules for attachments upload
        ApplyFileValidationRulesadd(readerEvt);

        //Render attachments thumbnails.
        RenderThumbnailadd(e, readerEvt);

        //Fill the array of attachment
        FillAttachmentArrayadd(e, readerEvt);
      };
    })(f);

    // Read in the image file as a data URL.
    // readAsDataURL: The result property will contain the file/blob's data encoded as a data URL.
    // More info about Data URI scheme https://en.wikipedia.org/wiki/Data_URI_scheme
    fileReader.readAsDataURL(f);
  }
  document
    .getElementById("files")
    .addEventListener("change", addhandleFileSelect, false);
}

//To remove attachment once user click on x button
jQuery(function($) {
  $("div").on("click", ".img-wraprelated .closerelated", function() {
    var id = $(this)
      .closest(".img-wraprelated")
      .find("img")
      .data("id");

    //to remove the deleted item from array
    var elementPos = AttachmentArray.map(function(x) {
      return x.FileName;
    }).indexOf(id);
    if (elementPos !== -1) {
      AttachmentArray.splice(elementPos, 1);
    }

    //to remove image tag
    $(this)
      .parent()
      .find("img")
      .not()
      .remove();

    //to remove div tag that contain the image
    $(this)
      .parent()
      .find("div")
      .not()
      .remove();

    //to remove div tag that contain caption name
    $(this)
      .parent()
      .parent()
      .find("div")
      .not()
      .remove();

    //to remove li tag
    var lis = document.querySelectorAll("#imgList li");
    for (var i = 0; (li = lis[i]); i++) {
      if (li.innerHTML == "") {
        li.parentNode.removeChild(li);
      }
    }
  });
});

//Apply the validation rules for attachments upload
function ApplyFileValidationRulesadd(readerEvt) {
  //To check file type according to upload conditions
  if (CheckFileTypeadd(readerEvt.type) == false) {
    alert(
      "The file (" +
        readerEvt.name +
        ") does not match the upload conditions, You can only upload jpg/png/gif files"
    );
    e.preventDefault();
    return;
  }

 
}

//To check file type according to upload conditions
function CheckFileTypeadd(fileType) {
  if (fileType == "image/jpeg") {
    return true;
  } else if (fileType == "image/png") {
    return true;
  } else if (fileType == "image/gif") {
    return true;
  } else {
    return false;
  }
  return true;
}

//To check file Size according to upload conditions


//Render attachments thumbnails.
function RenderThumbnailadd(e, readerEvt) {
  var li = document.createElement("li");
  ul.appendChild(li);
  li.innerHTML = [
    '<div class="img-wraprelated"> <span class="closerelated">&times;</span>' +
      '<img class="thumb" src="',
    e.target.result,
    '" title="',
    escape(readerEvt.name),
    '" data-id="',
    readerEvt.name,
    '"/>' + "</div>"
  ].join("");

  var div = document.createElement("div");
  div.className = "FileNameCaptionStyle";
  li.appendChild(div);
  div.innerHTML = [readerEvt.name].join("");
  document.getElementById("Filelist").insertBefore(ul, null);
}

//Fill the array of attachment
function FillAttachmentArrayadd(e, readerEvt) {
  AttachmentArray[arrCounter] = {
    AttachmentType: 1,
    ObjectType: 1,
    FileName: readerEvt.name,
    FileDescription: "Attachment",
    NoteText: "",
    MimeType: readerEvt.type,
    Content: e.target.result.split("base64,")[1],
    FileSizeInBytes: readerEvt.size
  };
  arrCounter = arrCounter + 1;
}


</script>
@endsection
