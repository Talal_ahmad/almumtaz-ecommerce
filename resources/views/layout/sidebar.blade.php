<!-- User Logo -->
<a href="{{ route('profile.show') }}" class="brand-link">
    <img src="{{ Auth::user()}}->profile_photo_path" alt="{{ Auth::user()->name }}" class="brand-image img-circle elevation-3" style="opacity: 0.8;" />
    <span class="brand-text font-weight-light"> {{ Auth::user()->name }}</span>
</a>

<!-- Sidebar -->
<div class="sidebar pt-3">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        {{-- Dashboard --}}
        @can('View Dashboard')
        <li class="nav-item">
            <a href="{{url('/dashboard')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='dashboard')?'active':''}}" style="margin-top: 5px;">
                <i class="nav-icon fas fa-tachometer-alt text_indigo"></i>
                <p>Dashboard</p>
            </a>
        </li>
        @endcan
        {{-- End Dashboard --}}
        {{-- Branches --}}
        @can('View Branches')
        <li class="nav-item">
            <a href="{{route('branch.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='branch')?'active':''}}" style="margin-top: 5px;">
                <i class="fa fa-code-branch nav-icon"></i>
                <p>List of Branches</p>
            </a>
        </li>
        @endcan
        {{-- End Branches --}}
        {{-- Disclaimer --}}
        @can('View Disclaimer')
        <li class="nav-item">
            <a href="{{route('disclaimer.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='disclaimer')?'active':''}}" style="margin-top: 5px;">
                <i class="fa fa-code-branch nav-icon"></i>
                <p>List of Disclaimer</p>
            </a>
        </li>
        @endcan
        {{-- End Disclaimer --}}
        {{-- Companies --}}
        @can('View Companies')
        <li class="nav-item">
            <a href="{{route('company.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='company')?'active':''}}" style="margin-top: 5px;">
                <i class="far fa-building nav-icon"></i>
                <p>List of Companies</p>
            </a>
        </li>
        @endcan
        {{-- End Companies --}}
        {{-- Categories --}}
        @can('View Categories')
        <li class="nav-item">
            <a href="{{route('category.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='category')?'active':''}}" style="margin-top: 5px;">
                <i class="fa fa-list-alt nav-icon" aria-hidden="true"></i>
                <p>List of Categories</p>
            </a>
        </li>
        @endcan
        {{-- End Categories --}}


        {{-- SubCategories --}}
        @can('View SubCategories')
        <li class="nav-item">
            <a href="{{route('subcategory.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='subcategory')?'active':''}}" style="margin-top: 5px;">
                <i class="fa fa-list-alt nav-icon" aria-hidden="true"></i>
                <p>List of Sub Categories</p>
            </a>
        </li>
        @endcan
        {{-- End  SubCategories --}}


        {{-- BannerImage --}}
        <!-- @can('View SubCategories') -->
        <li class="nav-item">
            <a href="{{route('bannerimage.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='bannerimage')?'active':''}}" style="margin-top: 5px;">
                <i class="fa fa-list-alt nav-icon" aria-hidden="true"></i>
                <p>List of Banner Images</p>
            </a>
        </li>
        <!-- @endcan -->
        {{-- End  BannerImage --}}

        {{-- BrandImage --}}
        <!-- @can('View BrandImages') -->
        <li class="nav-item">
            <a href="{{route('brandimage.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='brandimage')?'active':''}}" style="margin-top: 5px;">
                <i class="fa fa-list-alt nav-icon" aria-hidden="true"></i>
                <p>List of Brand</p>
            </a>
        </li>
        <!-- @endcan -->
        {{-- End  BrandImage --}}

        {{-- Blogs --}}
        @can('View Blogs')
        <li class="nav-item">
            <a href="{{route('blog.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='blogs')?'active':''}}" style="margin-top: 5px;">
                <i class="fa fa-list-alt nav-icon" aria-hidden="true"></i>
                <p>List of Blogs</p>
            </a>
        </li>
        @endcan
        {{-- End  Blogs --}}

         {{-- Stock --}}
         @can('View Stocks')
         <li class="nav-item">
             <a href="{{route('stock.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='stock')?'active':''}}" style="margin-top: 5px;">
                 <i class="fa fa-cubes nav-icon"></i>
                 <p>List of Stocks</p>
             </a>
         </li>
         @endcan
         {{-- End Stock --}}

        {{-- Items --}}
        @can('View Items')
        <li class="nav-item">
            <a href="{{route('item.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='item')?'active':''}}" style="margin-top: 5px;">
                <i class="fa fa-cubes nav-icon"></i>
                <p>List of Items</p>
            </a>
        </li>
        @endcan
        @can('View offers')
        <li class="nav-item">
            <a href="{{route('offer.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='offer')?'active':''}}" style="margin-top: 5px;">
                <i class="fa fa-cubes nav-icon"></i>
                <p>List of Offers</p>
            </a>
        </li>
        @endcan
        {{-- End items --}}
        {{-- add video --}}
        @can('View Videos')
        <li class="nav-item">
            <a href="{{route('video.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='video')?'active':''}}" style="margin-top: 5px;">
                <i class="fas fa-file-video nav-icon"></i>
                <p>Add video</p>
            </a>
        </li>
        @endcan
        {{-- add customer --}}
        @can('list customer')
        <li class="nav-item">
            <a href="{{route('customer.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='customer')?'active':''}}" style="margin-top: 5px;">
                <i class="fas fa-file-video nav-icon"></i>
                <p>List of Customers</p>
            </a>
        </li>
        @endcan
        {{-- End customer --}}
        {{-- add footer banner --}}
        @can('Add Footer banner')
        <li class="nav-item">
            <a href="{{route('footerimage.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='footerimage')?'active':''}}" style="margin-top: 5px;">
                <i class="fas fa-file-video nav-icon"></i>
                <p>Add Footer banner</p>
            </a>
        </li>
        @endcan
        {{-- End footer banners --}}
        {{-- Installment Plans --}}
        @can('View Installment Plans')
        <li class="nav-item">
            <a href="{{route('installmentplan.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='installmentplan')?'active':''}}" style="margin-top: 5px;">
                <i class="fa fa-tasks nav-icon" aria-hidden="true"></i>
                <p>List of Installment Plans</p>
            </a>
        </li>
        @endcan
        {{-- End Installment Plans --}}
        {{-- Installment Plans --}}
        @can('View Customer Application Form')
        <li class="nav-item">
            <a href="{{route('customerapplicationform.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='customerapplicationform')?'active':''}}" style="margin-top: 5px;">
                <i class="nav-icon fa fa-user" aria-hidden="true"></i>
                <p>Customer Application Form</p>
            </a>
        </li>
        @endcan
        @can('Contact Us Form')
        <li class="nav-item">
            <a href="{{route('contactus.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='contactusform')?'active':''}}" style="margin-top: 5px;">
                <i class="nav-icon fa fa-user" aria-hidden="true"></i>
                <p>Contact Us Form</p>
            </a>
        </li>
        @endcan
        @can('Feedback')
        <li class="nav-item">
            <a href="{{route('feedback.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='feedback')?'active':''}}" style="margin-top: 5px;">
                <i class="nav-icon fa fa-user" aria-hidden="true"></i>
                <p>Feedback</p>
            </a>
        </li>
        @endcan
        @can('Call Request')
        <li class="nav-item">
            <a href="{{route('callrequest.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='feedback')?'active':''}}" style="margin-top: 5px;">
                <i class="nav-icon fa fa-user" aria-hidden="true"></i>
                <p>Call Request</p>
            </a>
        </li>
        @endcan
        @can('Orderlist')
        <li class="nav-item has-treeview">
            <a href="javascript:void(0)" class="nav-link" style="margin-top: 5px;">
                <i class="nav-icon fas fa-cog floral_white"></i>
                <p>Orders<i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview">
            <li class="nav-item">
            <a href="{{route('order_detail.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='order_detail')?'active':''}}" style="margin-top: 5px;">
                <i class="nav-icon " aria-hidden="true"></i>
                <p>Order list</p>
            </a>
        </li>
            <li class="nav-item">
            <a href="{{route('addOrder')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='order_detail')?'active':''}}" style="margin-top: 5px;">
                <i class="nav-icon " aria-hidden="true"></i>
                <p>Add Order</p>
            </a>
        </li>
            </ul>
        </li>
        @endcan
    
        @can('Review')
        <li class="nav-item">
            <a href="{{route('review.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='customerapplicationform')?'active':''}}" style="margin-top: 5px;">
                <i class="nav-icon fa fa-user" aria-hidden="true"></i>
                <p>Review List</p>
            </a>
        </li>
        @endcan
        
        @can('Review')
        <li class="nav-item">
            <a href="{{route('remark.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='customerapplicationform')?'active':''}}" style="margin-top: 5px;">
                <i class="nav-icon fa fa-user" aria-hidden="true"></i>
                <p>Remark</p>
            </a>
        </li>
        @endcan
        {{-- End Customer Application Form --}}
        {{-- Settings --}}
        @can('View Settings')
        <li class="nav-item has-treeview">
            <a href="javascript:void(0)" class="nav-link" style="margin-top: 5px;">
                <i class="nav-icon fas fa-cog floral_white"></i>
                <p>Settings<i class="fas fa-angle-left right"></i></p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{route('users.index')}}" class="nav-link {{(\Route::getFacadeRoot()->current()->uri() =='users')?'active':''}}" style="margin-top: 5px;">
                        <i class="nav-icon fas fa-universal-access"></i>
                        <p>Roles & Permissions</p>
                    </a>
                </li>
            </ul>
        </li>
        @endcan
        {{-- End Settings --}}
    </ul>
</div>
<!-- Sidebar -->
