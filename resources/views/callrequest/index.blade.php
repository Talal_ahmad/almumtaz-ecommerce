
@extends('layout.master')
 @section('content')
<!-- All Records -->
<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header row">
                <h3 class="col-10">Call Request</h3>
            </div>
            <div class="card-body">
               <table id="datatable" class="table table-striped table-bordered  " style="width: 100%;">
                    <thead>
                        <tr>
                            <!-- <th>id</th> -->
                            <th>Number</th>
                            <th>Customer Name</th>
                            <th>Forword To</th>
                            <th>Forward Time</th>
                            <th>Forward By</th>
                            <th>Remark</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($call_request as $value)
                        <tr>
                            <td>{{$value->number}}</td>
                            <td>{{optional($value->customer)->name}}</td>
                            <td>{{optional($value->branch)->name}}</td>
                            <td>{{date('d-m-Y', strtotime($value->forward_time))}}</td>
                            <td>{{optional($value->forwardedBy)->name}}</td>
                            <td>{{optional($value->remark)->description}}</td>
                            <td class="d-flex">
                                @if($value->is_forward == 1)
                                <button class="btn bg-success p-1 m-1" >Forworded</button>
                                @elseif($value->cancel_request == 1)
                                <button class="btn bg-danger p-1 m-1" >closed</button>
                                @else
                                <a href="#" data-toggle="modal" data-id ="{{$value->id}}"  data-target="#exampleModal" class="btn bg-danger cancel p-1 m-1">close</a>
                                <a href="{{route('addOrder', ['id'=>$value->id, 'cust_id'=>$value->customer_id])}}" ><button type="button" class="btn bg-success p-1  m-1">processed</button></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Select Remark</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" id="add_remark">    
          @csrf  
        <div class="modal-body">

        <select name="remark" id="remark" class="custom-select">
            <option value="">Select Remark</option>
            @foreach($remarks as $remark)
            <option value="{{$remark->id}}">{{$remark->description}}</option>
            @endforeach
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>

    </div>
  </div>
</div>

@endsection
 @section('scripts')

<script>
    $(document).ready(function () {
        $('#datatable').DataTable({
            ordering: false,
        });
        
       
    });
       
       $("#add_remark").submit(function(e){
        e.preventDefault();
        var id = $('.cancel').data('id');
        console.log(id);
        $.ajax({
            url: "{{url ('addremark')}}",
            type: "post",
            data: {
                id: id,
                remark_id: $("#remark").val(),
                _token: "{{ csrf_token() }}",
            },
            success: function(success){
                $("#add_remark")[0].reset();
                $("#exampleModal").modal('hide');
              location.reload();
            }
        });
    })
 
</script>
@endsection
