@extends('layout.master')
@section('content')
    <div class="content">
        <div class="container-fluid pt-2">
            {{-- Checking User Have permission --}}
            @can('View Dashboard')
                <div class="row">
                    {{-- Users Info --}}
                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>{{count($users)}}</h3>
                                <p>Users</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-users"></i>
                            </div>
                            <a href="{{ route('users.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    {{-- End Users Info --}}
                    {{-- Branches Info --}}
                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>{{count($branches)}}</h3>
                                <p>Branches</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-code-branch"></i>
                            </div>
                            <a href="{{ route('branch.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    {{-- End Branches Info --}}
                    {{-- Companies Info --}}
                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>{{count($companies)}}</h3>
                                <p>Companies</p>
                            </div>
                            <div class="icon">
                                <i class="far fa-building"></i>
                            </div>
                            <a href="{{ route('company.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    {{-- End Companies Info --}}
                    {{-- Categories Info --}}
                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>{{count($categories)}}</h3>
                                <p>Categories</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-list-alt"></i>
                            </div>
                            <a href="{{ route('category.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    {{-- End Categories Info --}}
                </div>
            @endcan
            {{-- End Checking User Have permission --}}
        </div>
    </div>
@endsection
