@extends('layout.master') 
@section('content')
<!-- All Records -->
<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header row">
                <h3 class="col-10">All Disclaimer</h3>
                {{-- Checking permission --}}
                @can('Add Disclaimer')
                <div class="col-2">
                    <!-- <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#add_new_modal">Add New</button> -->
                </div>
                @endcan
                {{-- End Checking permission --}}
            </div>
            <div class="card-body">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End All Records -->
<!-- Add Record -->
<div id="add_new_modal" class="modal fade" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" permission="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Disclaimer</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="add">
                <div class="card-body">
                    @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="permission_name">Title:</label>
                                <input type="text" name="title" class="form-control" placeholder="Enter  Title" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer form_footer text-right">
                    <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Add Record -->
<!-- Edit Record -->
<div id="edit_modal" class="modal fade" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" permission="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Disclaimer</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="edit">
                <div class="card-body">
                    @csrf 
                    @method('PUT')
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="permission_name">Title:</label>
                                <!-- <input type="text" name="title" class="form-control" placeholder="Enter  Title" id="edit_title" /> -->
                                <textarea class="form-control" name="title" id="edit_title" cols="30" rows="4" placeholder="Enter Title here..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer form_footer text-right">
                    <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Edit Record -->
@endsection 
@section('scripts')
<script>
    var datatable;
    var rowid;
    $(document).ready(function () {
        // Fetching Records
        datatable = $("#datatable").DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            ajax: {
                url: "{{url('disclaimer')}}",
                method: "get",
            },
            columns: [
                { data: "title",
                    render: function (data) {
                        return data.substring(0,121);
                    }, },
                { data: "action" },
            ],
            drawCallback: function (settings) {
                $(".edit").click(function () {
                   rowid = $(this).attr("id");
                    $("#edit_title").val($(this).data("title"));
                    $("#edit_modal").modal("show");
                });
            },
        });
        // End Fetching Records
        // Adding Record
        $("#add").submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{route('disclaimer.store')}}",
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (success) {
                    if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        document.getElementById("add").reset();
                        $("#add_new_modal").modal("hide");
                        toastr.success('Disclaimer has been Added Successfully!');
                    }
                },
                error: function (response) {
					toastr.error('Error! Disclaimer could not be created due to some reasons.');
				},
            });
        });
        // End Adding Record
        // Updating Record
        $("#edit").submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{url('disclaimer')}}" + "/" + rowid,
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (success) {
                    if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        $("#edit_modal").modal("hide");
                        toastr.success('Disclaimer has been Updated Successfully!');
                    }
                },
                error: function (response) {
					toastr.error('Error! Disclaimer could not be updated due to some reasons.');
				},
            });
        });
        // End Updating Record
    });
    // Deleting Record
    function deleterow(id) {
        if (confirm("Do you really want to delete this Disclaimer?")) {
            $.ajax({
                url: "{{url('disclaimer')}}" + "/" + id,
                type: "delete",
                data: {
                    _token: "{{ csrf_token() }}",
                },
                success: function (success) {
                    if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        toastr.success('Disclaimer has been deleted Successfully!');
                    }
                },
                error: function (response) {
					toastr.error('Error! Disclaimer could not be deleted due to some reasons.');
				},
            });
        }
    }
    // End Deleting Record
</script>
@endsection
