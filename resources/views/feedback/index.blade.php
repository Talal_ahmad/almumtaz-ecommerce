@extends('layout.master')
@section('content')

    <!-- All Records -->
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header row">
                    <h3 class="col-10">All Feedbacks</h3>
                </div>
                <div class="card-body">
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
                        style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Message</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            // Fetching Records
            datatable = $("#datatable").DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: false,
                ajax: {
                    url: "{{ url('feedback') }}",
                    method: "get",
                },
                columns: [{
                        data: "id"
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "email"
                    },
                    {
                        data: "phone"
                    },
                    {
                        data: "message"
                    },
                    {
                        data: "image_path",
                        "render": function(data) {
                            return '<img src="./' + data +
                                '" style="height:100px;width:100px;"/>'
                        }
                    },
                    {
                        data: 'status'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],

                "drawCallback": function() {
                    $('.status').change(function(){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: "{{ url('change_status') }}",
                            data: {
                                'status': $(this).val(),
                                'id': $(this).attr('id'),
                                _token : "{{ csrf_token() }}"
                            },
                            success: function(data) {
                                datatable.ajax.reload();
                                toastr.success(data.success);
                            }
                        });
                    })
                },
            });
        });

    $(document).on('click', '.dlt', function() {
                 var id = $(this).data('id');
        if (confirm("Do you really want to delete this Feedback?")) {
            $.ajax({
                url: "{{url('feedback')}}" + "/" + id,
                type: "delete",
                data: {
                    _token: "{{ csrf_token() }}",
                },
                success: function (success) {
                    if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        toastr.success('Feedback has been deleted Successfully!');
                    }
                },
                error: function (response) {
					toastr.error('Error! Feedback could not be deleted due to some reasons.');
				},
            });
        }
    });

    </script>
@endsection
