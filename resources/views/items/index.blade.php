@extends('layout.master')
@section('content')
    <!-- All Records -->


    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header row">
                    <h3 class="col-10">All Items</h3>
                    {{-- Checking permission --}}
                    @can('Add Item')
                        <div class="col-2">
                            <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                                data-target="#add_new_modal">Add New</button>
                        </div>
                    @endcan
                    {{-- End Checking permission --}}
                </div>
                {{-- Filters --}}
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="filter_form">
                                @csrf
                                @method('GET')
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label for="category_filter">Select Category</label>
                                            <select name="category_filter" id="category_filter"
                                                class="select2 form-select w-100" data-placeholder="Search By Category...">
                                                <option value=""></option>
                                                @foreach ($categories as $category)
                                                    <option value="{{ $category->id }}"
                                                        {{ $category->name == request('category') ? 'selected' : '' }}>
                                                        {{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label for="company_filter">Select Company</label>
                                            <select name="company_filter" id="company_filter"
                                                class="select2 form-select w-100" data-placeholder="Search By Company...">
                                                <option value=""></option>
                                                @foreach ($companies as $company)
                                                    <option value="{{ $company->id }}"
                                                        {{ $company->name == request('company') ? 'selected' : '' }}>
                                                        {{ $company->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12" style="text-align: end">
                                        <a href="{{ url('/item') }}" type="button" class="btn btn-danger mt-1">Reset</a>
                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                    </div>
                                    <div class="col-8">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                {{-- Table Listing --}}
                <div class="card-body">
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
                        style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Sr No</th>
                                <th>Name</th>
                                <th>Company</th>
                                <th>Category</th>
                                {{-- <th>Sub Category</th> --}}
                                <th>Price</th>
                                <th>Discount Price</th>
                                <th>Net Amount</th>
                                <th>Onsale</th>
                                <th>New Arrival</th>
                                <th>Offer</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Records -->
    <!-- Add Record -->
    <div id="add_new_modal" class="modal fade" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered modal-lg" permission="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Item</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_modal">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form id="add">
                    <div class="modal-body" style="padding:0.9rem 1.25rem">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Name:</label>
                                    <input type="text" name="name" class="form-control" placeholder="Enter Name"
                                        autocomplete="off" required />
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Price:</label>
                                    <input type="text" name="price" class="form-control" placeholder="Enter Price"
                                        autocomplete="off" required />
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Discount:</label>
                                    <input type="text" name="discount_price" class="form-control"
                                        placeholder="Enter Discount Price" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Company:</label>
                                    <select name="company_id" class="custom-select" required>
                                        <option value="">--Select--</option>
                                        @foreach ($companies as $company)
                                            <option value="{{ $company->id }}">{{ $company->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Category:</label>
                                    <select name="category_id" id="category_id" class="custom-select" required>
                                        <option value="">--Select--</option>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Sub Category:</label>
                                    <select name="subcategory_id" id="subcategory_id" class="custom-select">
                                        <option value="">--Select--</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Status:</label>
                                    <select name="status" class="custom-select" required>
                                        <option value="Active">Active</option>
                                        <option value="Inactive">InActive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Featured Image:</label>
                                    <input type="file" name="image" class="form-control" accept="image/*"
                                        required />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="plans">Assign Plans:</label>
                                    <select multiple="true" class="form-control select2" name="plans[]"
                                        data-placeholder="Select Insatallments">
                                        @foreach ($plans as $key => $value)
                                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <span class="btn btn-success btnrelated fileinput-button">
                                        <span>Select Attachment</span>
                                        <input type="file" name="files[]" id="files" multiple><br />
                                    </span>
                                    <output id="Filelist"></output>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="image">Meta Tags:</label>
                                    <textarea name="meta_tags" id="meta_tags" cols="30" rows="1" class="form-control" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-1">
                                    <label class="form-label" for="description">Description</label>
                                    <textarea class="form-control" id="description" placeholder="Enter the Description" rows="5"
                                        name="description"></textarea>
                                </div>
                            </div>
                            {{-- <div class="col">
                                <label for="permission_name">Description:</label>
                                <textarea name="description" class="ckeditor form-control" required></textarea>
                            </div> --}}
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-1">
                                    <label class="form-label" for="specification">Specification</label>
                                    <textarea class="form-control" id="specification" placeholder="Enter the Description" rows="5"
                                        name="specification"></textarea>
                                </div>
                            </div>
                            {{-- <div class="col">
                                <label for="permission_name">Specification:</label>
                                <textarea name="specification" class="ckeditor form-control" required></textarea>
                            </div> --}}
                        </div>
                        <div class="row pt-2">
                            <div class="col">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="onsale"
                                            value="1">Onsale
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="new_arrival"
                                            value="2">New Arrival
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="offer"
                                            value="3">Offer
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer form_footer text-right">
                        <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Add Record -->
    <!-- Edit Record -->
    <div id="edit_modal" class="modal fade" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered modal-lg" permission="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Item</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close-editmodal">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form id="edit">
                    <div class="card-body" style="padding:0.9rem 1.25rem">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Name:</label>
                                    <input type="text" name="name" class="form-control" placeholder="Enter  Name"
                                        id="editname" required />
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Price:</label>
                                    <input type="text" name="price" class="form-control" placeholder="Enter  Price"
                                        id="editprice" required />
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Discount:</label>
                                    <input type="number" id="edit_discount_price" name="discount_price"
                                        class="form-control" placeholder="Enter discount">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Company:</label>
                                    <select name="company_id" class="custom-select" id="editcompany" required>
                                        <option value="">--Select--</option>
                                        @foreach ($companies as $company)
                                            <option value="{{ $company->id }}">{{ $company->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Category:</label>
                                    <select name="category_id" class="custom-select" id="editcategory" required>
                                        <option value="">--Select--</option>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Sub Category:</label>
                                    <select name="subcategory_id" id="editsubcategory_id" class="custom-select">
                                        <option value="">--Select--</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Status:</label>
                                    <select name="status" class="custom-select" id="editstatus" required>
                                        <option value="Active">Active</option>
                                        <option value="Inactive">InActive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Display Image:</label>
                                    <input type="file" name="image" class="form-control" accept="image/*" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="plans">Assign Plans:</label>
                                    <select multiple="true" class="form-control select2" name="plans[]" id="editplans">
                                        @foreach ($plans as $key => $value)
                                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">


                                    <!--To give the control a modern look, I have applied a stylesheet in the parent span.-->
                                    <span class="btn btn-success btnrelated fileinput-button">
                                        <span>Select Attachment</span>
                                        <input type="file" name="files[]" id="edit-files" multiple><br />
                                    </span>
                                    <output id="edit-Filelist"></output>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="image">Meta Tags:</label>
                                    <textarea name="meta_tags" id="edit_meta_tags" cols="30" rows="1" class="form-control" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label for="permission_name">Description:</label>
                                <textarea name="description" class="ckeditor form-control" id="description" required></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label for="permission_name">Specification:</label>
                                <textarea name="specification" class="ckeditor form-control" id="specification" required></textarea>
                            </div>
                        </div>
                        <div class="row pt-2">
                            <div class="col">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="onsale" id="edit_onsale"
                                            value="1">Onsale
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="new_arrival"
                                            id="edit_new_arrival" value="2">New Arrival
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="offer" id="edit_offer"
                                            value="3">Offer
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer form_footer text-right">
                        <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Edit Record -->
@endsection
@section('scripts')
    <script>
        ClassicEditor.create(document.querySelector('#specification'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor.create(document.querySelector('#description'))
            .catch(error => {
                console.error(error);
            });
        var datatable;
        var rowid;
        $(document).ready(function() {
            // Fetching Records
            var i = 1;
            datatable = $("#datatable").DataTable({
                lengthMenu: [
                    [10, 25, 50, -1],
                    [10, 25, 50, 'All'],
                ],
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: false,
                ajax: {
                    url: "{{ url('item') }}",
                    method: "get",
                    data: function(d) {
                        d.company_filter = $('#company_filter').val();
                        d.category_filter = $('#category_filter').val();
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false,
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "company.name"
                        // name: 'company.name'
                    },
                    {
                        data: "category.name"
                        // name: 'category.name'
                    },
                    // {
                    //     data: 'subcategory_id'
                    // },
                    {
                        data: "price"
                    },
                    {
                        data: "discount_price",
                        render: function(data) {
                            return data + "%";
                        },
                    },
                    {
                        data: "total_price"
                    },
                    {
                        data: "onsale",
                        render: function(data) {
                            return data != null ? 'Yes' : '';
                        },
                    },
                    {
                        data: "new_arrival",
                        render: function(data) {
                            return data != null ? 'Yes' : '';
                        },
                    },
                    {
                        data: "offer",
                        render: function(data) {
                            return data != null ? 'Yes' : '';
                        },
                    },
                    {
                        data: "status"
                    },
                    {
                        data: "action"
                    },
                ],
            });

            setInterval(function() {
                datatable.ajax.reload(null, false);
            }, 30000);
            // End Fetching Records
            // Adding Record
            $('#filter_form').submit(function(e) {
                e.preventDefault();
                datatable.draw();
            });

            $("#category_id").change(function() {
                var category_id = $(this).val();

                $.ajax({
                    type: "GET",
                    url: "{{ url('item') }}",
                    dataType: "json",
                    data: {
                        categoryId: category_id,
                    },
                    success: function(data) {
                        var $select = $("#subcategory_id");
                        $select.empty();
                        if (data) {
                            $select.append(
                                `<option value="">${data.length>0?'--Select--':'No Sub Category Available'}</option>`
                            );
                        }
                        if (data !== null) {
                            $.each(data, function(index, item) {
                                var text = (item.name ? item.name : '');
                                var $option = $("<option>", {
                                    value: item.id,
                                    text: text,
                                });
                                $select.append($option);
                            });
                        }
                    }
                });
            });

            $("#add").submit(function(e) {
                e.preventDefault();

                // var ckeditor_description = CKEDITOR.instances['description'];
                // var ckeditor_specification = CKEDITOR.instances['specification'];

                // if (ckeditor_description) {
                //     document.getElementById('description').value = ckeditor_description.getData();
                // }

                // if (ckeditor_specification) {
                //     document.getElementById('specification').value = ckeditor_specification.getData();
                // }

                $.ajax({
                    url: "{{ route('item.store') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(success) {
                        if (success.errors) {
                            $.each(success.errors, function(index, value) {
                                toastr.error(value);
                            });
                        } else if (success.error_message) {
                            toastr.error(
                                'An error has occurred! Please Contact Administrator.');
                        } else {
                            datatable.ajax.reload();
                            document.getElementById("add").reset();
                            $(".select2").val("").trigger("change");
                            $("#add_new_modal").modal("hide");
                            toastr.success('Item has been Added Successfully!');
                        }
                    },
                    error: function(response) {
                        toastr.error('Error! Item could not be created due to some reasons.');
                    },
                });
            });

            // End Adding Record
            // Updating Record
            $("#edit").submit(function(e) {
                e.preventDefault();
                var data = new FormData(this);
                data.append('description', CKEDITOR.instances['description'].getData());
                data.append('specification', CKEDITOR.instances['specification'].getData());
                $.ajax({
                    url: "{{ url('item') }}" + "/" + rowid,
                    type: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function(success) {
                        if (success.errors) {
                            $.each(success.errors, function(index, value) {
                                toastr.error(value);
                            });
                        } else if (success.error_message) {
                            toastr.error(
                                'An error has been occured! Please Contact Administrator.');
                        } else {
                            datatable.ajax.reload();

                            $("#edit_modal").modal("hide");
                            toastr.success('Item has been Updated Successfully!');
                        }
                    },
                    error: function(response) {
                        toastr.error('Error! Item could not be updated due to some reasons.');
                    },
                });
            });
            // End Updating Record
            // Initializing Select2
            $(".select2").select2({
                closeOnSelect: true,
                allowClear: true,
            });
            // End Initializing Select2
            // $('#close_modal').click(function() {
            //     location.reload();
            // })
            // $('#close-editmodal').click(function() {
            //     location.reload();
            // })

        });

        $("#editcategory").change(function() {
            var editcategory_id = $(this).val();

            $.ajax({
                type: "GET",
                url: "{{ url('item') }}",
                dataType: "json",
                data: {
                    editcategoryId: editcategory_id,
                },
                success: function(data) {
                    var $select = $("#editsubcategory_id");
                    $select.empty();

                    if (data !== null) {
                        $.each(data, function(index, item) {
                            var text = (item.name ? item.name : '');
                            var $option = $("<option>", {
                                value: item.id,
                                text: text
                            });
                            $select.append($option);
                        });
                    }
                }
            });
        });
        // Fetching existing data to edit
        function edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('item') }}" + "/" + id + "/edit",
                type: "get",
                success: function(success) {
                    console.log(success.meta_tags);
                    $("#edittext").empty();
                    $("#editname").val(success.name);
                    $("#editprice").val(success.price);
                    $("#edit_discount_price").val(success.discount_price);
                    CKEDITOR.instances['description'].setData(success.description);
                    CKEDITOR.instances['specification'].setData(success.specification);
                    // $("#edittext").append(success.description);
                    $("#editcompany").val(success.company_id);
                    $("#editcategory").val(success.category_id);
                    $("#editsubcategory_id").val(success.subcategory_id);
                    $("#editstatus").val(success.status);
                    $("#edit_new_arrival").prop('checked', success.new_arrival);
                    $("#edit_onsale").prop('checked', success.onsale);
                    $("#edit_meta_tags").val(success.meta_tags);
                    $("#editplans").val(success.plans_id).select2({
                        placeholder: "Select Installment Plans",
                        closeOnSelect: true,
                        allowClear: true,
                    });
                    $("#edit-Filelist").empty();
                    $("#edit_modal").modal("show");
                },
            });
        }
        // End Fetching existing data to edit
        // Deleting Record
        function deleterow(id) {
            if (confirm("Do you really want to delete this Item?")) {
                $.ajax({
                    url: "{{ url('item') }}" + "/" + id,
                    type: "delete",
                    data: {
                        _token: "{{ csrf_token() }}",
                    },
                    datatype: "JSON",
                    success: function(success) {
                        if (success.error_message) {
                            toastr.error('An error has been occured! Please Contact Administrator.');
                        } else {
                            datatable.ajax.reload();
                            toastr.success('Item has been deleted Successfully!');
                        }
                    },
                    error: function(response) {
                        toastr.error('Error! Item could not be deleted due to some reasons.');
                    },
                });
            }
        }
        // End Deleting Record

        //I added event handler for the file upload control to access the files properties.
        document.addEventListener("DOMContentLoaded", addinit, false);

        //To save an array of attachments
        var AttachmentArray = [];

        //counter for attachment array
        var arrCounter = 0;

        //to make sure the error message for number of files will be shown only one time.
        var filesCounterAlertStatus = false;

        //un ordered list to keep attachments thumbnails
        var ul = document.createElement("ul");
        ul.className = "thumb-Images";
        ul.id = "imgList";

        function addinit() {
            //add javascript handlers for the file upload event
            document
                .querySelector("#files")
                .addEventListener("change", addhandleFileSelect, false);
        }

        //the handler for file upload event
        function addhandleFileSelect(e) {

            ul.innerHTML = "";

            //to make sure the user select file/files
            if (!e.target.files) return;

            //To obtaine a File reference
            var files = e.target.files;

            // Loop through the FileList and then to render image files as thumbnails.
            for (var i = 0, f;
                (f = files[i]); i++) {
                //instantiate a FileReader object to read its contents into memory
                var fileReader = new FileReader();

                // Closure to capture the file information and apply validation.
                fileReader.onload = (function(readerEvt) {
                    return function(e) {
                        //Apply the validation rules for attachments upload
                        ApplyFileValidationRulesadd(readerEvt);

                        //Render attachments thumbnails.
                        RenderThumbnailadd(e, readerEvt);

                        //Fill the array of attachment
                        FillAttachmentArrayadd(e, readerEvt);
                    };
                })(f);

                // Read in the image file as a data URL.
                // readAsDataURL: The result property will contain the file/blob's data encoded as a data URL.
                // More info about Data URI scheme https://en.wikipedia.org/wiki/Data_URI_scheme
                fileReader.readAsDataURL(f);
            }
            document
                .getElementById("files")
                .addEventListener("change", addhandleFileSelect, false);
        }

        //To remove attachment once user click on x button
        jQuery(function($) {
            $("div").on("click", ".img-wraprelated .closerelated", function() {
                var id = $(this)
                    .closest(".img-wraprelated")
                    .find("img")
                    .data("id");

                //to remove the deleted item from array
                var elementPos = AttachmentArray.map(function(x) {
                    return x.FileName;
                }).indexOf(id);
                if (elementPos !== -1) {
                    AttachmentArray.splice(elementPos, 1);
                }

                //to remove image tag
                $(this)
                    .parent()
                    .find("img")
                    .not()
                    .remove();

                //to remove div tag that contain the image
                $(this)
                    .parent()
                    .find("div")
                    .not()
                    .remove();

                //to remove div tag that contain caption name
                $(this)
                    .parent()
                    .parent()
                    .find("div")
                    .not()
                    .remove();

                //to remove li tag
                var lis = document.querySelectorAll("#imgList li");
                for (var i = 0;
                    (li = lis[i]); i++) {
                    if (li.innerHTML == "") {
                        li.parentNode.removeChild(li);
                    }
                }
            });
        });

        //Apply the validation rules for attachments upload
        function ApplyFileValidationRulesadd(readerEvt) {
            //To check file type according to upload conditions
            if (CheckFileTypeadd(readerEvt.type) == false) {
                alert(
                    "The file (" +
                    readerEvt.name +
                    ") does not match the upload conditions, You can only upload jpg/png/gif files"
                );
                e.preventDefault();
                return;
            }


        }

        //To check file type according to upload conditions
        function CheckFileTypeadd(fileType) {
            if (fileType == "image/jpeg") {
                return true;
            } else if (fileType == "image/png") {
                return true;
            } else if (fileType == "image/gif") {
                return true;
            } else {
                return false;
            }
            return true;
        }

        //To check file Size according to upload conditions


        //Render attachments thumbnails.
        function RenderThumbnailadd(e, readerEvt) {
            var li = document.createElement("li");
            ul.appendChild(li);
            li.innerHTML = [
                '<div class="img-wraprelated"> <span class="closerelated">&times;</span>' +
                '<img class="thumb" src="',
                e.target.result,
                '" title="',
                escape(readerEvt.name),
                '" data-id="',
                readerEvt.name,
                '"/>' + "</div>"
            ].join("");

            var div = document.createElement("div");
            div.className = "FileNameCaptionStyle";
            li.appendChild(div);
            div.innerHTML = [readerEvt.name].join("");
            document.getElementById("Filelist").insertBefore(ul, null);
        }

        //Fill the array of attachment
        function FillAttachmentArrayadd(e, readerEvt) {
            AttachmentArray[arrCounter] = {
                AttachmentType: 1,
                ObjectType: 1,
                FileName: readerEvt.name,
                FileDescription: "Attachment",
                NoteText: "",
                MimeType: readerEvt.type,
                Content: e.target.result.split("base64,")[1],
                FileSizeInBytes: readerEvt.size
            };
            arrCounter = arrCounter + 1;
        }

        document.addEventListener("DOMContentLoaded", init, false);

        //To save an array of attachments
        var AttachmentArray = [];

        //counter for attachment array
        var arrCounter = 0;

        //to make sure the error message for number of files will be shown only one time.
        var filesCounterAlertStatus = false;

        //un ordered list to keep attachments thumbnails
        var ul = document.createElement("ul");
        ul.className = "thumb-Images";
        ul.id = "imgList";

        function init() {
            //add javascript handlers for the file upload event
            document
                .querySelector("#edit-files")
                .addEventListener("change", handleFileSelect, false);
        }

        //the handler for file upload event

        function handleFileSelect(e) {

            ul.innerHTML = "";

            //to make sure the user select file/files
            if (!e.target.files) return;

            //To obtaine a File reference
            var files = e.target.files;

            // Loop through the FileList and then to render image files as thumbnails.
            for (var i = 0, f;
                (f = files[i]); i++) {
                //instantiate a FileReader object to read its contents into memory
                var fileReader = new FileReader();

                // Closure to capture the file information and apply validation.
                fileReader.onload = (function(readerEvt) {
                    return function(e) {
                        //Apply the validation rules for attachments upload
                        ApplyFileValidationRules(readerEvt);

                        //Render attachments thumbnails.
                        RenderThumbnail(e, readerEvt);

                        //Fill the array of attachment
                        FillAttachmentArray(e, readerEvt);
                    };
                })(f);

                // Read in the image file as a data URL.
                // readAsDataURL: The result property will contain the file/blob's data encoded as a data URL.
                // More info about Data URI scheme https://en.wikipedia.org/wiki/Data_URI_scheme
                fileReader.readAsDataURL(f);
            }
            document
                .getElementById("edit-files")
                .addEventListener("change", handleFileSelect, false);
        }

        //To remove attachment once user click on x button
        jQuery(function($) {
            $("div").on("click", ".img-wraprelated .closerelated", function() {
                var id = $(this)
                    .closest(".img-wraprelated")
                    .find("img")
                    .data("id");

                //to remove the deleted item from array
                var elementPos = AttachmentArray.map(function(x) {
                    return x.FileName;
                }).indexOf(id);
                if (elementPos !== -1) {
                    AttachmentArray.splice(elementPos, 1);
                }

                //to remove image tag
                $(this)
                    .parent()
                    .find("img")
                    .not()
                    .remove();

                //to remove div tag that contain the image
                $(this)
                    .parent()
                    .find("div")
                    .not()
                    .remove();

                //to remove div tag that contain caption name
                $(this)
                    .parent()
                    .parent()
                    .find("div")
                    .not()
                    .remove();

                //to remove li tag
                var lis = document.querySelectorAll("#imgList li");
                for (var i = 0;
                    (li = lis[i]); i++) {
                    if (li.innerHTML == "") {
                        li.parentNode.removeChild(li);
                    }
                }
            });
        });

        //Apply the validation rules for attachments upload
        function ApplyFileValidationRules(readerEvt) {
            //To check file type according to upload conditions
            if (CheckFileType(readerEvt.type) == false) {
                alert(
                    "The file (" +
                    readerEvt.name +
                    ") does not match the upload conditions, You can only upload jpg/png/gif files"
                );
                e.preventDefault();
                return;
            }


        }

        //To check file type according to upload conditions
        function CheckFileType(fileType) {
            if (fileType == "image/jpeg") {
                return true;
            } else if (fileType == "image/png") {
                return true;
            } else if (fileType == "image/gif") {
                return true;
            } else {
                return false;
            }
            return true;
        }

        //To check file Size according to upload conditions


        //Render attachments thumbnails.
        function RenderThumbnail(e, readerEvt) {
            var li = document.createElement("li");
            ul.appendChild(li);
            li.innerHTML = [
                '<div class="img-wraprelated"> <span class="closerelated">&times;</span>' +
                '<img class="thumb" src="',
                e.target.result,
                '" title="',
                escape(readerEvt.name),
                '" data-id="',
                readerEvt.name,
                '"/>' + "</div>"
            ].join("");

            var div = document.createElement("div");
            div.className = "FileNameCaptionStyle";
            li.appendChild(div);
            div.innerHTML = [readerEvt.name].join("");
            document.getElementById("edit-Filelist").insertBefore(ul, null);
        }

        //Fill the array of attachment
        function FillAttachmentArray(e, readerEvt) {
            AttachmentArray[arrCounter] = {
                AttachmentType: 1,
                ObjectType: 1,
                FileName: readerEvt.name,
                FileDescription: "Attachment",
                NoteText: "",
                MimeType: readerEvt.type,
                Content: e.target.result.split("base64,")[1],
                FileSizeInBytes: readerEvt.size
            };
            arrCounter = arrCounter + 1;
        }
    </script>
    <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
@endsection
