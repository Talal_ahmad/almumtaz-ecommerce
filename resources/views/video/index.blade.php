@extends('layout.master')
@section('content')
<!-- All Records -->
<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header row">
                <h3 class="col-10">All Video</h3>
                {{-- Checking permission --}}
                @can('Add Video')
                @if($video->isEmpty())
                <div class="col-2">
                    <button type="button" id="addbtn" class="btn btn-primary float-right" data-toggle="modal" data-target="#add_new_modal">Add New</button>
                </div>
                @endif

                @endcan
                {{-- End Checking permission --}}
            </div>
            <div class="card-body">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Sr no</th>
                            <th>Video path</th>
                            <th>status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End All Records -->
<!-- Add Record -->
<div id="add_new_modal" class="modal fade" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" permission="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Video</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="add">
                <div class="card-body">
                    @csrf
                    {{-- <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="video">Display Video:</label>
                                <input type="file" name="video" class="form-control" accept="video/*" required />
                            </div>
                        </div>
                    </div> --}}
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="video_url">Add Url:</label>
                                <input type="link" name="video_path" id="video_path" class="form-control" required placeholder="Paste Video Link"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="permission_name">Status:</label>
                                <select name="status" class="form-control" id="status" required>
                                    <option value="Active">Active</option>
                                    <option value="Inactive">InActive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer form_footer text-right">
                        <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Add Record -->
<!-- Edit Record -->
<!-- Edit Record -->
<div class="modal fade" id="editModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Video</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form id="edit">
                <div class="card-body">
                    @csrf
                    @method ('PUT')
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="video_url">Add_url:</label>
                                <input type="link" name="video_path" class="form-control"id="editvideo" accept="video/*" required />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="permission_name">Status:</label>
                                <select name="status" class="form-control" id="editstatus" required>
                                    <option value="Active">Active</option>
                                    <option value="Inactive">InActive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Edit Record -->
@endsection
@section('scripts')
<script>
    var datatable;
    var rowid;
    $(document).ready(function () {
        // Fetching Records
        datatable = $("#datatable").DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            ajax: {
                url: "{{url('video')}}",
                method: "get",
            },
            columns: [
                { data: "id" },
                { data:  "video_path"},
                { data:  "status"},
                { data: "action"},
            ],
        });
        // End Fetching Records
        // Adding Record
        // $("#add").submit(function (e) {
        //     e.preventDefault();
        //     $.ajax({
        //         url: "{{route('video.store')}}",
        //         type: "post",
        //         data: new FormData(this),
        //         processData: false,
        //         contentType: false,
        //         responsive: true,
        //         success: function (success) {
        //             if(success.errors){
        //                 $.each( success.errors, function( index, value ){
        //                     toastr.error(value);
        //                 });
        //             }
        //             else if(success.error_message){
        //                 toastr.error('An error has been occured! Please Contact Administrator.');
        //             }
        //             else{
        //                 datatable.ajax.reload();
        //                 document.getElementById("add").reset();
        //                 $(".select2").val("").trigger("change");
        //                 $("#add_new_modal").modal("hide");
        //                 // location.reload();
        //                 toastr.success('Video has been Added Successfully!');
        //             }

        //         },
        //         error: function (response) {
		// 			toastr.error('Error! Video could not be created due to some reasons.');
		// 		},
        //     });
        // });
        $('#add').on('submit', function(e) {
            e.preventDefault();
            var regExp = /^https?\:\/\/(?:www\.youtube(?:\-nocookie)?\.com\/|m\.youtube\.com\/|youtube\.com\/)?(?:ytscreeningroom\?vi?=|youtu\.be\/|vi?\/|user\/.+\/u\/\w{1,2}\/|embed\/|watch\?(?:.*\&)?vi?=|\&vi?=|\?(?:.*\&)?vi?=)([^#\&\?\n\/<>"']*)/i;
            var match = $('#video_path').val().match(regExp);
            var video_path = (match && match[1].length==11)? match[1] : false;
            var status = $('#status').val();
            $.ajax({
                url: "{{ route('video.store') }}",
                type: "POST",
                data: {
                    video_path : video_path,
                    status : status,
                    _token : "{{ csrf_token() }}",
                },
                success: function (success) {
                    if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        document.getElementById("add").reset();
                        $(".select2").val("").trigger("change");
                        $("#add_new_modal").modal("hide");
                        location.reload();
                        toastr.success('Video has been Added Successfully!');
                    }

                },
            });
        });
        // End Adding Record
        // Updating Record
        $("#edit").submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{url('video')}}" + "/" + rowid,
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (success) {
                    if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        $(".edit").off("submit", "**");
                        datatable.ajax.reload();
                        $("#editModal").modal("hide");
                        toastr.success('video has been updated Successfully!');
                    }
                },
                error: function (response) {
					toastr.error('Error! User could not be updated due to some reasons.');
				},
            });
        });
        // End Updating Record
    });
    // Fetching existing data to edit
    function edit(id) {
        rowid = id;
        $.ajax({
            url: "{{url('video')}}" + "/" + id + "/edit",
            type: "get",
            success: function (success) {
                // $("#editvideo").val(success.video.video_path);
                $("#editstatus").val(success.status);
                $("#editvideo").val(success.video_path);
                $("#editModal").modal("show");
            },
        });
    }
    // End Fetching existing data to edit
    // Deleting Record
    function deleterow(id) {
        if (confirm("Do you really want to delete this Video?")) {
            $.ajax({
                url: "{{url('video')}}" + "/" + id,
                type: "delete",
                data: {
                    _token: "{{ csrf_token() }}",
                },
                success: function (success) {
                    if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        location.reload();
                        toastr.success('Video has been Deleted Successfully!');
                    }
                },
                error: function (response) {
					toastr.error('Error! Video could not be deleted due to some reasons.');
				},
            });
        }
    }
    // End Deleting Record
</script>
@endsection
