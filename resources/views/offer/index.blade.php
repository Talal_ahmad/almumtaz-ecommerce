@extends('layout.master')
@section('content')
<!-- All Records -->
<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header row">
                <h3 class="col-10">All Offers</h3>
                {{-- Checking permission --}}
                  @if($offer->isEmpty())
                <div class="col-2">
                    <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#add_new_modal">Add New</button>
                </div>
                @endif
                {{-- End Checking permission --}}
            </div>
            <div class="card-body">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>price</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End All Records -->
<!-- Add Record -->
<div id="add_new_modal" class="modal fade" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" permission="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Offers</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="add">
                <div class="card-body">
                    @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="name">Item Name:</label>
                                <select name="item_id" class="custom-select" required>
                                    <option value="">--Select--</option>
                                    @foreach($item as $items)
                                    <option value="{{$items->id}}">{{$items->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="name">Price:</label>
                                <input type="text" name="price" class="form-control" placeholder="Enter  Price"  />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="date">Start Date:</label>
                                <input type="date" name="start_date" class="form-control" required />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="date">End Date:</label>
                                <input type="date" name="end_date" class="form-control"  required />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-footer form_footer text-right">
                    <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Add Record -->
<!-- Edit Record -->
<div id="edit_modal" class="modal fade" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" permission="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update Offer</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form class="editform">
                <div class="card-body">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="edit_item_id">Item Name:</label>
                                <select name="item_id" id="edit_item_id" class="custom-select select2" required>
                                    @foreach($item as $items)
                                    <option value="{{$items->id}}">{{$items->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="name">Price:</label>
                                <input type="text" name="price" id="edit_price" class="form-control" placeholder="Enter  Price"  />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="date">Start Date:</label>
                                <input type="date" name="start_date" id="edit_strdate" class="form-control" required />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="date">End Date:</label>
                                <input type="date" name="end_date" id="edit_enddate" class="form-control"  required />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer form_footer text-right">
                    <button type="submit" class="btn btn-success" id="updatebutton"><i class="far fa-save"></i> Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Edit Record -->
@endsection
@section('scripts')
<script>
    var datatable;
    var rowid;
    $(document).ready(function () {
        // Fetching Records
        datatable = $("#datatable").DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: false,
                ajax: {
                    url: "{{ url('offer') }}",
                    method: "get",
                },
                columns: [{
                        data: "id"
                    },
                    {
                        data: "item_name"
                    },
                    {
                        data: "price"
                    },
                    {
                        data: "start_date"
                    },
                    {
                        data: "end_date"
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
            });
        // End Fetching Records
        // Adding Record
        $("#add").submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{route('offer.store')}}",
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                responsive: true,
                success: function (success) {
                    if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        $('#add')[0].reset();
                        location.reload();
                        $("#add_new_modal").modal("hide");

                        toastr.success('Offer has been Added Successfully!');
                    }

                },
                error: function (response) {
					toastr.error('Error! Please enter correct date.');
				},
            });
        });

        // End Adding Record
        // Updating Record
        $(".editform").on("submit", function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{url('offer')}}" + "/" + rowid,
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (success) {
                    if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        $(".editform").off("submit", "**");
                        datatable.ajax.reload();
                        $("#edit_modal").modal("hide");
                        toastr.success('Offer has been Updated Successfully!');
                    }

                },
                error: function (response) {
					toastr.error('Error! Offer could not be updated due to some reasons.');
				},
            });
        });
        // End Updating Record

    });
    $(".select2").select2({
            closeOnSelect: true,
            allowClear: true,
        });
    // Fetching existing data to edit
    function edit(id) {
        rowid = id;
        $.ajax({
            url: "{{url('offer')}}" + "/" + id + "/edit",
            type: "get",
            success: function (success) {
                $("#edit_item_id").val(success.item_id).select2({
                    placeholder: "Select name",
                    closeOnSelect: true,
                    allowClear: true,
                });
                $("#edit_price").val(success.price);
                $("#edit_strdate").val(success.start_date);
                $("#edit_enddate").val(success.end_date);
                $("#edit_modal").modal("show");
            },
        });
    }
    // End Fetching existing data to edit
    // Deleting Record
    $(document).on('click','.dlt',function(){
        var id = $(this).data('id');
        if (confirm("Do you really want to delete this Offer?")) {
            $.ajax({
                url: "{{url('offer')}}" + "/" + id,
                type: "delete",
                data: {
                    _token: "{{ csrf_token() }}",
                },
                success: function (success) {
                    if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        location.reload();
                        toastr.success('Offer has been Deleted Successfully!');
                    }
                },
                error: function (response) {
					toastr.error('Error! Offer could not be deleted due to some reasons.');
				},
            });
        }
    });

    // End Deleting Record
</script>
@endsection
