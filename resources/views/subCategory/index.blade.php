@extends('layout.master')
@section('content')
    <!-- All Records -->
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header row">
                    <h3 class="col-10">All Sub Categories</h3>
                    {{-- Checking permission --}}
                    @can('Add Category')
                        <div class="col-2">
                            <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                                data-target="#add_new_modal">Add New</button>
                        </div>
                    @endcan
                    {{-- End Checking permission --}}
                </div>
                <div class="card-body">
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap"
                        style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Sub Category</th>
                                {{-- <th>Code</th> --}}
                                <th>Category</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Records -->
    <!-- Add Record -->
    <div id="add_new_modal" class="modal fade" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" permission="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Sub Category</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form id="add">
                    <div class="card-body">
                        @csrf
                        {{-- <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Code:</label>
                                    <input type="text" name="code" class="form-control" placeholder="Enter  Code" />
                                </div>
                            </div>
                        </div> --}}
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="plans"> Categories:</label>
                                    <select name="category_id" id="" class="form-control">
                                        <option value="">--Select--</option>
                                        @foreach ($categories as $key => $value)
                                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Sub Category:</label>
                                    <input type="text" name="name" class="form-control"
                                        placeholder="Enter Sub-Category" />
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer form_footer text-right">
                        <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Add Record -->
    <!-- Edit Record -->
    <div id="edit_modal" class="modal fade" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" permission="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update SubCategory</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form class="editform">
                    <div class="card-body">
                        @csrf
                        @method('PUT')
                        {{-- <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Code:</label>
                                    <input type="text" name="code" class="form-control" placeholder="Enter  Code"
                                        id="editcode" />
                                </div>
                            </div>
                        </div> --}}
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="permission_name">Sub Category:</label>
                                    <input type="text" name="name" class="form-control" placeholder="Enter  Name"
                                        id="editname" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="plans">Category:</label>
                                    <select class="form-control" name="category_id" id="editcategories">
                                        <option value="">--Select--</option>
                                        @foreach ($categories as $key => $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="card-footer form_footer text-right">
                            <button type="submit" class="btn btn-success" id="updatebutton"><i class="far fa-save"></i>
                                Save Changes</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Edit Record -->
@endsection
@section('scripts')
    <script>
        var datatable;
        var rowid;
        $(document).ready(function() {
            // Fetching Records
            datatable = $("#datatable").DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: false,
                ajax: {
                    url: "{{ url('subcategory') }}",
                    method: "get",
                },
                columns: [{
                        data: "name"
                    },
                    // {
                    //     data: "code"
                    // },
                    {
                        data: "c_name"
                    },
                    {
                        data: "action"
                    },
                ],
            });
            // End Fetching Records
            // Adding Record
            $("#add").submit(function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ route('subcategory.store') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(success) {
                        if (success.errors) {
                            $.each(success.errors, function(index, value) {
                                toastr.error(value);
                            });
                        } else if (success.error_message) {
                            toastr.error(
                                'An error has been occured! Please Contact Administrator.');
                        } else {
                            datatable.ajax.reload();
                            document.getElementById("add").reset();
                            $("#add_new_modal").modal("hide");
                            toastr.success('Sub Category has been Added Successfully!');
                        }

                    },
                    error: function(response) {
                        toastr.error(
                            'Error! Sub Category could not be created due to some reasons.');
                    },
                });
            });
            // End Adding Record
            // Updating Record
            $(".editform").on("submit", function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ url('subcategory') }}" + "/" + rowid,
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(success) {
                        if (success.errors) {
                            $.each(success.errors, function(index, value) {
                                toastr.error(value);
                            });
                        } else if (success.error_message) {
                            toastr.error(
                                'An error has been occured! Please Contact Administrator.');
                        } else {
                            $(".editform").off("submit", "**");
                            datatable.ajax.reload();
                            $("#edit_modal").modal("hide");
                            toastr.success('SubCategory has been Updated Successfully!');
                        }

                    },
                    error: function(response) {
                        toastr.error(
                            'Error! Category could not be updated due to some reasons.');
                    },
                });
            });
            // End Updating Record
            // Initializing Select2
            // $(".select2").select2({
            //     placeholder: "Select Installment Plans",
            //     closeOnSelect: true,
            //     allowClear: true,
            // });
            // End Initializing Select2
        });
        // Fetching existing data to edit
        function edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('subcategory') }}" + "/" + id + "/edit",
                type: "get",
                success: function(success) {
                    // $("#editcode").val(success.code);
                    $("#editname").val(success.name);
                    $("#editcategories").val(success.category_id)
                    $("#edit_modal").modal("show");
                },
            });
        }
        // End Fetching existing data to edit
        // Deleting Record
        function deleterow(id) {
            if (confirm("Do you really want to delete this Category?")) {
                $.ajax({
                    url: "{{ url('subcategory') }}" + "/" + id,
                    type: "delete",
                    data: {
                        _token: "{{ csrf_token() }}",
                    },
                    success: function(success) {
                        if (success.error_message) {
                            toastr.error('An error has been occured! Please Contact Administrator.');
                        } else {
                            datatable.ajax.reload();
                            toastr.success('Category has been Deleted Successfully!');
                        }
                    },
                    error: function(response) {
                        toastr.error('Error! Category could not be deleted due to some reasons.');
                    },
                });
            }
        }
        // End Deleting Record
    </script>
@endsection
