@extends('layout.master')
@section('content')
<!-- All Records -->
<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header row">
                <h3 class="col-10">All Remark</h3>
             
                <div class="col-2">
                  <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">Add New</button>   
                </div>
              
            </div>
            <div class="card-body">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Add Record -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Remark</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add_remark">
                <div class="card-body">
                    @csrf
                    @method ('PUT')
                     <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="">Name:</label>
                                <input type="text" name="name" id="r_name" class="form-control"  required />
                            </div>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="">Description:</label>
                                <textarea  type="text" class="form-control"  id="description" name="description" rows="5" id="comment"></textarea>
                            </div>
                        </div>
                    </div>
             
                    <div class="card-footer  text-right">
                        <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save</button>
                    </div>
                </div>
            </form>
    </div>
  </div>
</div>
<!-- End Add Record -->


<div class="modal fade" id="editModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Remark</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form id="edit">
            <div class="card-body">
                    @csrf
                    @method ('PUT')
                     <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="">Name:</label>
                                <input type="text" name="name" id="edit_name" class="form-control"  required />
                            </div>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="">Description:</label>
                                <textarea  type="text" class="form-control"  id="edit_description" name="description" rows="5" id="comment"></textarea>
                            </div>
                        </div>
                    </div>
             
                    <div class="card-footer  text-right">
                        <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script>
    $(document).ready(function(){
        datatable = $("#datatable").DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            ajax: {
                url: "{{url('remark')}}",
                method: "get",
            },
            columns: [
                { data: "id" },
                { data:  "name"},
                { data:  "description"},
                { data: "action"},
            ],
        });
        
        $("#add_remark").on('submit',function(e){
            e.preventDefault();
            $.ajax({
                url: "{{route('remark.store')}}",
                type: "POST",
                data: {
                    name: $("#r_name").val(),
                    description: $("#description").val(),
                    _token: "{{ csrf_token() }}",
                },
                success: function(response){
                    if(response.errors)
                    {
                        $.each(response.errors, function (index, value){
                            toastr.error(value);
                        });
                    }
                    else if(response.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        // datatable.ajax.reload();
                        // document.getElementById("#add_remark").reset();
                        location.reload();
                        toastr.success('Remark has been Added Successfully!');
                    }
                     console.log(response);
                },

            });
        });
    });
      // Updating Record
      $("#edit").submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{url('remark')}}" + "/" + rowid,
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (success) {
                    if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        $(".edit").off("submit", "**");
                        datatable.ajax.reload();
                        $("#editModal").modal("hide");
                        toastr.success('video has been updated Successfully!');
                    }
                },
                error: function (response) {
					toastr.error('Error! User could not be updated due to some reasons.');
				},
            });
        });
        // End Updating Record
    function edit(id) {
        rowid = id;
        $.ajax({
            url: "{{url('remark')}}" + "/" + id + "/edit",
            type: "get",
            success: function (success) {
                // $("#editvideo").val(success.video.video_path);
                $("#edit_name").val(success.name);
                $("#edit_description").val(success.description);
                $("#editModal").modal("show");
            },
        });
    }
    function deleterow(id) {
        if (confirm("Do you really want to delete this Video?")) {
            $.ajax({
                url: "{{url('remark')}}" + "/" + id,
                type: "delete",
                data: {
                    _token: "{{ csrf_token() }}",
                },
                success: function (success) {
                    if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        toastr.success('Video has been Deleted Successfully!');
                    }
                },
                error: function (response) {
					toastr.error('Error! Video could not be deleted due to some reasons.');
				},
            });
        }
    }
</script>
@endsection