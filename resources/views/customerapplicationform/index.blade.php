@extends('layout.master')
@section('content')
    <!-- Customer Application Form -->
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header row">
                    <h3 class="col-9">Customer Application Form</h3>
                    {{-- Checking permission --}}
                    @can('Manage CAF')
                    <div class="col text-right">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modifyModal">Manage Form</button>
                    </div>
                    @endcan
                    {{-- End Checking permission --}}
                </div>
                {{-- All Form Fields --}}
                @php
                    $all_fields = ['Name', 'DOB', 'Mobile Number', 'CNIC', 'Address', 'Delievery Address', 'Monthly Income', 'Already Customer', 'Guarantee 1 Name', 'Guarantee 1 Mobile Number', 'Guarantee 1 CNIC', 'Guarantee 1 Address', 'Guarantee 2 Name', 'Guarantee 2 Mobile Number', 'Guarantee 2 CNIC', 'Guarantee 2 Address'];
                @endphp
                {{-- End All Form Fields --}}
                <div class="card-body">
                    <fieldset disabled="disabled">
                        <div class="row">
                            @if (in_array('1', $selected_fields))
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name:</label>
                                        <input class="form-control" placeholder="Enter Name" />
                                    </div>
                                </div>
                            @endif
                            @if (in_array('2', $selected_fields))
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>DOB:</label>
                                        <input class="form-control" placeholder="Select Date" />
                                    </div>
                                </div>
                            @endif
                            @if (in_array('3', $selected_fields))
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Mobile No:</label>
                                        <input class="form-control" placeholder="Enter Mobile Number" />
                                    </div>
                                </div>
                            @endif
                            @if (in_array('4', $selected_fields))
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>CNIC:</label>
                                        <input class="form-control" placeholder="Enter CNIC Number" />
                                    </div>
                                </div>
                            @endif
                            @if (in_array('5', $selected_fields))
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Address:</label>
                                        <input class="form-control" placeholder="Enter Current Address" />
                                    </div>
                                </div>
                            @endif
                            @if (in_array('6', $selected_fields))
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Delivery Address:</label>
                                        <input class="form-control" placeholder="Enter Delievery Address" />
                                    </div>
                                </div>
                            @endif
                            @if (in_array('7', $selected_fields))
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Monthly Income:</label>
                                        <input class="form-control" placeholder="Enter Monthly Income(PKR)" />
                                    </div>
                                </div>
                            @endif
                            @if (in_array('8', $selected_fields))
                                <div class="col-md-6">
                                    <div>
                                        <label>Already A Customer:</label>
                                    </div>
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input">Yes
                                        </label>
                                    </div>
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input">No
                                        </label>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            @if (in_array('9', $selected_fields))
                                <div class="col-sm-12">
                                    <h3>1st Guarantee:</h3>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name:</label>
                                        <input class="form-control" placeholder="Enter Name" />
                                    </div>
                                </div>
                            @endif
                            @if (in_array('10', $selected_fields))
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Mobile No:</label>
                                        <input class="form-control" placeholder="Enter Mobile Number"/>
                                    </div>
                                </div>
                            @endif
                            @if (in_array('11', $selected_fields))
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>CNIC:</label>
                                        <input class="form-control" placeholder="Enter CNIC Number"/>
                                    </div>
                                </div>
                            @endif
                            @if (in_array('12', $selected_fields))
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Address:</label>
                                        <input class="form-control" placeholder="Enter Current Address" />
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            @if (in_array('13', $selected_fields))
                                <div class="col-sm-12">
                                    <h3>2nd Guarantee:</h3>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name:</label>
                                        <input class="form-control" placeholder="Enter Name" />
                                    </div>
                                </div>
                            @endif
                            @if (in_array('14', $selected_fields))
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Mobile No:</label>
                                        <input class="form-control" placeholder="Enter Mobile Number" />
                                    </div>
                                </div>
                            @endif
                            @if (in_array('15', $selected_fields))
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>CNIC:</label>
                                        <input class="form-control" placeholder="Enter CNIC Number"/>
                                    </div>
                                </div>
                            @endif
                            @if (in_array('16', $selected_fields))
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Address:</label>
                                        <input class="form-control" placeholder="Enter Current Address"/>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
    <!-- End Customer Application Form -->
    <!-- Manage Application Form Fields -->
    <div class="modal fade" id="modifyModal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Manage Application Form</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form action="{{ route('customerapplicationform.store') }}" method="POST">
                    <div class="modal-body">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <h3>Choose Fields You Want To Display in Website Form:</h3>
                                <div class="form-check">
                                    <small>
                                        <label class="form-check-label font-weight-bold">
                                            <input type="checkbox" class="form-check-input" id="checkAll">Check All
                                        </label>
                                    </small>
                                </div>
                                <hr>
                            </div>
                            @php $count = 1; @endphp
                            @foreach ($all_fields as $field)
                                <div class="col-md-6">
                                    <div class="form-check">
                                        <label class="form-check-label font-weight-bold">
                                            <input type="checkbox" name="fields_to_display[]" class="form-check-input field_checkbox" value="{{ $count}}" @if (in_array($count, $selected_fields)) checked @endif>
                                            {{ $field }}
                                        </label>
                                    </div>
                                </div>
                                @php $count ++ ; @endphp
                            @endforeach
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Manage Application Form Fields -->
@endsection
@section('scripts')
    <script>
        // Display Success Message on form submission
        @if (Session::has('success'))
            toastr.success("{{ Session::get('success') }}");
        @endif
        // End Display Success Message on form submission
        // Display Validation Messages on form submission
        @if(Session::has('errors'))
            @foreach(Session::get('errors') as $error)
                toastr.error("{{$error}}");
            @endforeach
        @endif
        // Display Validation Messages on form submission
        // Display Error Message on form submission
        @if(Session::has('error_message'))
            toastr.error("Error! Please Contact Administrator.");
        @endif
        // Display Error Message on form submission
        $(document).ready(function() {
            // Check & Uncheck all Display Fields on click Check All field
            $("#checkAll").click(function() {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });
            // End Check & Uncheck all Display Fields on click Check All field
            // Change state of all checkbox field depending on other fields
            $(".field_checkbox").click(function() {
                if ($(".field_checkbox").length == $(".field_checkbox:checked").length) {
                    $("#checkAll").prop("checked", true);
                } else {
                    $("#checkAll").prop("checked", false);
                }
            });
            // End Change state all checkbox field depending on other fields
            // Change state of all checkbox field depending on other fields on load
            if ($(".field_checkbox").length == $(".field_checkbox:checked").length) {
                $("#checkAll").prop("checked", true);
            } else {
                $("#checkAll").prop("checked", false);
            }
            // End Change state of all checkbox field depending on other fields on load
        });
    </script>
@endsection
