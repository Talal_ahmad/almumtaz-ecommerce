@extends('layout.master') 
@section('content')
<!-- All Records -->
<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header row">
                <h3 class="col-10">All Blogs</h3>
                {{-- Checking permission --}}
                @can('Add Category')
                <div class="col-2">
                    <a href="{{route('blog.create')}}"><button type="button" class="btn btn-primary float-right">Add New</button></a>

                   
                </div>
                @endcan
                {{-- End Checking permission --}}
            </div>
            <div class="card-body">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Title</th>                          
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End All Records -->
@endsection 
@section('scripts')
<script>
    var datatable;
    var rowid;
    $(document).ready(function () {
        // Fetching Records
            datatable = $("#datatable").DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            ajax: {
                url: "{{url('blog')}}",
                method: "get",
            },
            columns: [
                { data: "image", "render": function (data){
                    return '<img src="'+data+'" style="height:50px;width:50px;"/>'
                }},
                { data: "title" },
                { data: "action"},
            ],
            drawCallback: function (settings) {
                $(".edit").click(function () {
                    id=$(this).attr('value');
                   window.location.replace(`{{url('blog')}}`+'/'+id+'/edit');
                });
            },
        });
    });



    function deleterow(id) {
        
        
        if (confirm("Do you really want to delete this Blog?")) {
            $.ajax({
                url: "{{url('blog')}}" + "/" + id,
                type: "delete",
                data: {
                    _token: "{{ csrf_token() }}",
                },
                success: function (success) {
                    if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        toastr.success('Blog has been Deleted Successfully!');
                    }
                },
                error: function (response) {
					toastr.error('Error! Blog could not be deleted due to some reasons.');
				},
            });
        }
    }
        </script>
@endsection 

