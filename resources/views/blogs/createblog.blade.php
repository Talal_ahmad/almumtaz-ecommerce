
@extends('layout.master') 
@section('content')
<!-- All Records -->
<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header row">
                <h3 class="col-10">Create Blogs</h3>
            <div class="card-body">
            <form method="post" action="{{route('blog.store')}}" enctype="multipart/form-data">
                     @csrf   

                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" name="title" id="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Image</label>
                            <input type="file" name="image_file" id="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Description</label>
                            <textarea class="ckeditor form-control" name="content"></textarea>
                        </div>

                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
            </div>
        </div>
    </div>
</div>
<!-- End All Records -->




@endsection

    
@section('scripts')

<script type="text/javascript">
        CKEDITOR.replace('content',{
            filebrowserUploadMethod : "form",
            filebrowserUploadUrl : "{{route('PhotoUpload',[ '_token' => csrf_token()])}}",

        });
</script>
@endsection

