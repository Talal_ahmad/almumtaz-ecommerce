@extends('layout.master')
@section('content')
<!-- All Records -->
<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header row">
                <h3 class="col-10">All Reviews</h3>
                {{-- Checking permission --}}
                {{-- @can('Add Company')
                <div class="col-2">
                    <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#add_new_modal">Add New</button>
                </div>
                @endcan --}}
                {{-- End Checking permission --}}
            </div>
            <div class="card-body">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Message</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scripts')
<script>
    var datatable;
    var rowid;
    $(document).ready(function () {
        // Fetching Records
        datatable = $("#datatable").DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            ajax: {
                url: "{{url('review')}}",
                method: "get",
            },
            columns: [
                { data: "user" },
                { data: "message" },
                { data: "status" },
                { data: "action" },

            ],
            "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": 0
            } ],
            // "order": [[ 0, 'asc' ]],
            // "drawCallback": function() {
            // $('.toggle-class').bootstrapToggle();
            // status();
            // },

        });
        // End Fetching Records
    });

    $(document).on('click', '.accept', function(e){
        e.preventDefault();
        var accept_id = $(this).data('id');
        $.ajax({
            type: "POST",
            url : "{{url('under_review')}}",
            data :{
                accept_id : accept_id,
                _token: $("input[name=_token]").val()
            },
            success:function(success){
                if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        toastr.success('User Review has been Accepted Successfully!');
                    }
            }
        });
    })

    $(document).on('click', '.reject', function(e){
        e.preventDefault();
        var reject_id = $(this).data('id');
        $.ajax({
            type: "POST",
            url : "{{url('under_review')}}",
            data :{
                reject_id : reject_id,
                _token: $("input[name=_token]").val()
            },
            success:function(success){
                if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        toastr.success('User Review has been Rejected Successfully!');
                    }
            }
        });
    })

    // function status(){
    //     $(".toggle-class").change(function (event) {
    //         event.preventDefault();
    //             var status = $(this).prop('checked') == true ? 0 : 1;
    //             var user_id = $(this).data('id');
    //             $.ajax({
    //                 type:"GET",
    //                 datatype:"json",
    //                 url: "{{url('userstatus')}}",
    //                 data: {
    //                     status : status,
    //                     user_id : user_id
    //                 },
    //                 success:function (data) {
    //                     console.log(data.success);
    //                 }
    //             });

    //     });
    // }
    // Deleting Record
    // function deleterow(id) {
    //     if (confirm("Do you really want to delete this Company?")) {
    //         $.ajax({
    //             url: "{{url('company')}}" + "/" + id,
    //             type: "delete",
    //             data: {
    //                 _token: "{{ csrf_token() }}",
    //             },
    //             success: function (success) {
    //                 if(success.error_message){
    //                     toastr.error('An error has been occured! Please Contact Administrator.');
    //                 }
    //                 else{
    //                     datatable.ajax.reload();
    //                     toastr.success('Company has been deleted Successfully!');
    //                 }
    //             },
    //             error: function (response) {
	// 				toastr.error('Error! Company could not be deleted due to some reasons.');
	// 			},
    //         });
    //     }
    // }
    // End Deleting Record
</script>
@endsection
