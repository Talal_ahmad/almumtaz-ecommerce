@extends('layout.master')
@section('content')
<!-- All Records -->
<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header row">
                <h3 class="col-10">All Banner Images</h3>
                {{-- Checking permission --}}
                @can('Banner Image')
                <div class="col-2">
                    <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#add_new_modal">Add New</button>
                </div>
                @endcan
                {{-- End Checking permission --}}
            </div>
            <div class="card-body">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Company</th>
                            <th>Category</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End All Records -->
<!-- Add Record -->
<div id="add_new_modal" class="modal fade" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" permission="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Banner Image</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="add">
                <div class="card-body">
                    @csrf

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" name="name" id="" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="permission_name">Company Name:</label>
                                <select name="company_id" class="form-control" required id="company_id">
                                    <option value="">--Select--</option>
                                    @foreach($companies as $company)
                                    <option value="{{$company->id}}">{{$company->name}}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="form-group">
                                <label for="permission_name">Category Name:</label>
                                <select name="category_id" class="form-control" required id="category_id">
                                    <option value="">--Select--</option>
                                    @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="form-group">
                                <label for="image">Display Image:</label>
                                <input type="file" name="image_file" class="form-control" accept="image/*" required />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer form_footer text-right">
                    <button type="submit" class="btn btn-success"><i class="far fa-save"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Add Record -->
<!-- Edit Record -->
<div id="edit_modal" class="modal fade" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" permission="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update Image</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form class="editform">
                <div class="card-body">
                    @csrf
                    @method('PUT')

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" name="name" id="edit_name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="permission_name">Company Name:</label>
                                <select name="company_id" class="form-control" required id="edit_company_id">
                                    <option value="">--Select--</option>
                                    @foreach($companies as $company)
                                    <option value="{{$company->id}}">{{$company->name}}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="form-group">
                                <label for="permission_name">Category Name:</label>
                                <select name="category_id" class="form-control" required id="edit_category_id">
                                    <option value="">--Select--</option>
                                    @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="form-group">
                                <label for="edit_image">Display Image:</label>
                                <input type="file" name="image_file" class="form-control" accept="image/*" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer form_footer text-right">
                    <button type="submit" class="btn btn-success" id="updatebutton"><i class="far fa-save"></i> Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Edit Record -->
@endsection
@section('scripts')
<script>
    var datatable;
    var rowid;
    $(document).ready(function () {
        // Fetching Records
        datatable = $("#datatable").DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            ajax: {
                url: "{{url('bannerimage')}}",
                method: "get",
            },
            columns: [
                { data: 'name'},
                { data: 'companys'},
                { data: 'category'},
                { data: "image", "render": function (data){
                    return '<img src="'+data+'" style="height:50px;width:50px;"/>'
                }},
                { data: "action"},
            ],
        });
        // End Fetching Records
        // Adding Record
        $("#add").submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{route('bannerimage.store')}}",
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                responsive: true,
                success: function (success) {
                    if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        document.getElementById("add").reset();
                        $("#add_new_modal").modal("hide");
                        toastr.success('Images has been Added Successfully!');
                    }

                },
                error: function (response) {
					toastr.error('Error! Images could not be created due to some reasons.');
				},
            });
        });
        // End Adding Record
        // Updating Record
        $(".editform").on("submit", function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{url('bannerimage')}}" + "/" + rowid,
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (success) {
                    if(success.errors){
                        $.each( success.errors, function( index, value ){
                            toastr.error(value);
                        });
                    }
                    else if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        $(".editform").off("submit", "**");
                        datatable.ajax.reload();
                        $("#edit_modal").modal("hide");
                        toastr.success('Images has been Updated Successfully!');
                    }

                },
                error: function (response) {
					toastr.error('Error! Category could not be updated due to some reasons.');
				},
            });
        });
        // End Updating Record
        // Initializing Select2
        // $(".select2").select2({
        //     placeholder: "Select Installment Plans",
        //     closeOnSelect: true,
        //     allowClear: true,
        // });
        // End Initializing Select2
    });
    // Fetching existing data to edit
    function edit(id) {
        rowid = id;
        $.ajax({
            url: "{{url('bannerimage')}}" + "/" + id + "/edit",
            type: "get",
            success: function (success) {
                $("#edit_name").val(success.name);
                $("#edit_company_id").val(success.company_id);
                $("#edit_category_id").val(success.category_id);
                $("#edit_modal").modal("show");
            },
        });
    }
    // End Fetching existing data to edit
    // Deleting Record
    function deleterow(id) {

        if (confirm("Do you really want to delete this Image?")) {
            $.ajax({
                url: "{{url('bannerimage')}}" + "/" + id,
                type: "delete",
                data: {
                    _token: "{{ csrf_token() }}",
                },
                success: function (success) {
                    if(success.error_message){
                        toastr.error('An error has been occured! Please Contact Administrator.');
                    }
                    else{
                        datatable.ajax.reload();
                        toastr.success('Image has been Deleted Successfully!');
                    }
                },
                error: function (response) {
					toastr.error('Error! Image could not be deleted due to some reasons.');
				},
            });
        }
    }
    // End Deleting Record
</script>
@endsection