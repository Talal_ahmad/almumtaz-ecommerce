/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue").default;


import VueRouter from "vue-router";
Vue.use(VueRouter);
// Vue.use(VeeValidate);
Vue.use(VeeValidate, { fieldsBagName: "veeFields" });
import VeeValidate from "vee-validate";

// sweetalert
// import VueSweetalert2 from 'vue-sweetalert2';
// app.use(VueSweetalert2);
// Bootstrap
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.js";
// String Length Filter
Vue.filter('stringLength', function(value) {
    if (!value) return ''
    return value.substring(0, 16);
});

// Slick Slider
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.min.js";

// Custom Css
require("../../public/css/style.css");
require("../../public/css/store.css");
require("../../public/css/detailproduct.css");

// Vue bootstrap
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
    // Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

import AOS from 'aos'
import 'aos/dist/aos.css'

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
import { Form, HasError } from "vform";
import Vue from "vue";
window.Form = Form;

// Vue Image Zoom Pro
import vuePhotoZoomPro from 'vue-photo-zoom-pro';
import "vue-photo-zoom-pro/dist/style/vue-photo-zoom-pro.css";

Vue.component(
    "index-component",
    require("./components/website/IndexComponent.vue").default
);
Vue.component(
    "login-component",
    require("./components/website/LoginComponent.vue").default
);
Vue.component(
    "Registration-component",
    require("./components/website/RegistrationComponent.vue").default
);
Vue.component(
    "TermsCondition-component",
    require("./components/website/TermsConditionComponent.vue").default
);
Vue.component(
    "Privacy-component",
    require("./components/website/PrivacyComponent.vue").default
);
Vue.component(
    "ReturnPolicy-component",
    require("./components/website/ReturnPolicyComponent.vue").default
);

Vue.component("pagination", require("laravel-vue-pagination"));
Vue.component(
    "blog-component",
    require("./components/website/BlogComponent.vue").default
);
Vue.component(
    "shop-component",
    require("./components/website/ShopComponent.vue").default
);
// Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component(
    "itemdetail-component",
    require("./components/website/ItemDetailComponent.vue").default
);
Vue.component(
    "error-component",
    require("./components/website/Error404Component.vue").default
);
Vue.component(
    "offer-component",
    require("./components/website/OfferComponent.vue").default
);
Vue.component(
    "CheckOut-component",
    require("./components/website/CheckOutComponent.vue").default
);
Vue.component('vue-photo-zoom-pro', vuePhotoZoomPro);

const routes = [{
        path: "/",
        component: require("./components/website/IndexComponent.vue").default
    },
    {
        path: "/customerlogin",
        component: require("./components/website/LoginComponent.vue").default
    },

    {
        path: "/register",
        component: require("./components/website/RegistrationComponent.vue")
            .default
    },
    {
        path: "/termscondition",
        component: require("./components/website/TermsConditionComponent.vue")
            .default
    },
    {
        path: "/privacy",
        component: require("./components/website/PrivacyComponent.vue").default
    },
    {
        path: "/delivery",
        component: require("./components/website/DeliveryComponent.vue").default
    },
    {
        path: "/returnpolicy",
        component: require("./components/website/ReturnPolicyComponent.vue")
            .default
    },
    {
        path: "/blog",
        component: require("./components/website/BlogComponent.vue").default
    },
    {
        path: "/readblog/:id?",
        component: require("./components/website/ReadblogComponent.vue")
            .default,
        name: "readblog",
        props: true
    },
    // Customer profile
    {
        path: "/customerprofile",
        component: require("./components/website/CustomerProfileComponent.vue")
            .default
    },
    {
        path: "/aboutus",
        component: require("./components/website/AboutusComponent.vue").default
    },
    {
        path: "/cart",
        component: require("./components/website/CartComponent.vue").default
    },
    {
        path: "/CheckOut",
        component: require("./components/website/CheckOutComponent.vue").default
    },
    {
        path: "/wishlist",
        component: require("./components/website/WishlistComponent.vue").default
    },
    {
        path: "/feedback",
        component: require("./components/website/FeedbackComponent.vue").default
    },
    {
        path: "/banner",
        component: require("./components/website/ShopComponent.vue").default,
        name: "banner",
        props: true
    },
    {
        path: "/store",
        component: require("./components/website/ShopComponent.vue").default,
        name: "store",
        props: true
    },
    {
        path: "/store/category/:category?",
        component: require("./components/website/ShopComponent.vue").default,
        name: "store",
        props: true
    },
    {
        path: "/store/company/:company?",
        component: require("./components/website/ShopComponent.vue").default,
        name: "store",
        props: true
    },
    {
        path: '/store/search/:search_keyword?',
        component: require("./components/website/ShopComponent.vue").default,
        props: true
    },
    //deals
    {
        path: "/deals",
        component: require("./components/website/DealsComponent.vue").default
    },
    // onsale
    {
        path: "/onsale",
        component: require("./components/website/OnsaleComponent.vue")
            .default
    },
    // new arrival
    {
        path: "/newarrival",
        component: require("./components/website/NewarrivalComponent.vue")
            .default
    },
    {
        path: "/contactus",
        component: require("./components/website/ContactusComponent.vue")
            .default
    },
    {
        path: "/offers",
        component: require("./components/website/OfferComponent.vue")
            .default
    },
    {
        path: "/detail-items/:id?",
        component: require("./components/website/ItemDetailComponent.vue")
            .default,
        name: "detail-item",
        props: true
    },
    {
        path: "/:catchAll(.*)",
        component: require("./components/website/Error404Component.vue").default
    }

];

const router = new VueRouter({
    // mode: 'history',
    routes // short for `routes: routes`
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Thousand Seperator Filter
Vue.filter('formatNumber', function(value) {
    if (!value) return ''
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
});

const app = new Vue({
    router,
    methods: {
        getCartCount() {
            axios
                .get("getcartcount")
                .then(function(response) {
                    if (response.data.code == 200) {
                        $('#user_cart_count').html(response.data.data);
                    }
                })
                .catch(function(error) {
                    // console.log(error);
                });
        },
        addToCart(id) {
            let self = this;
            axios
                .post("cart", {
                    item_id: id
                })
                .then(function(response) {
                    if (response.data.code == 200) {
                        alert('Added');
                        self.getCartCount();
                    } else if (response.data.code == 419) {
                        alert('Already Exists');
                    } else {
                        alert('Error');
                    }
                })
                .catch(function(error) {
                    if (error.response) {
                        if (error.response.status == 401) {
                            router.push('/customerlogin');
                        }
                    }
                });
        },
        addTowishList(id) {
            axios
                .post("wishlist", {
                    item_id: id,
                })
                .then(function(response) {
                    if (response.data.code == 200) {
                        alert('Added');
                    } else if (response.data.code == 419) {
                        alert('Already Exists');
                    } else {
                        alert('Error');
                    }
                })
                .catch(function(error) {
                    if (error.response) {
                        if (error.response.status == 401) {
                            router.push('/customerlogin');
                        }
                    }
                });
        },
        checkUserLogin() {
            axios
                .get("check_user_login")
                .then(function(response) {
                    if (response.data) {
                        $("#addReviewButton").fadeIn();
                        $("#without_login_review_message").fadeOut();
                    } else {
                        $("#addReviewButton").fadeOut();
                        $("#without_login_review_message").fadeIn();
                    }
                })
                .catch(function(error) {
                    if (error.response) {
                        console.log(error);
                    }
                });
        },
    },
    created() {
        AOS.init()
    },
}).$mount("#app");