<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;
    protected $guarded=[];
    protected $appends = ['company','category','plans_id'];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function plans()
    {
        return $this->hasMany(ItemHasPlans::class);
    }

    public function cart()
    {
        return $this->hasOne(Cart::class);
    }

    public function wishlist()
    {
        return $this->hasOne(Wishlist::class);
    }

    public function getCompanyAttribute(){
		if($this->company()->exists()){
			return $this->company()->first()->name;
		}
        else{
			return '';
		}
	}

    public function getCategoryAttribute(){
		if($this->category()->exists()){
			return $this->category()->first()->name;
		}
        else{
			return '';
		}
	}

    public function getPlansIdAttribute(){
		if($this->plans()->exists()){
			$months =  $this->plans()->get()->pluck('plan_id');
            return $months;
		}
        else{
			return '';
		}
	}

    public function syncplans($plans)
    {

        $this->deleteChild();

        foreach($plans as $key => $value){

            ItemHasPlans::create(['item_id' => $this->id , 'plan_id' => $value]);

        }
    }

    public function revokeplans()
    {
        $this->deleteChild();
    }


    public static function InsertImage($image){
        $fileOrignalName = $image->getClientOriginalName();
        $image_path = '/items';
        $path = public_path() . $image_path;
        $filename = time().'_'.rand(000 ,999).'.'.$image->getClientOriginalExtension();
        $image->move($path, $filename);
        $paths = $image_path.'/'.$filename;
        return $paths;
    }
    public static function multiImage($images)
    {
        foreach($images as $key => $image){
            $fileOrignalName = $image->getClientOriginalName();
            $image_path = '/multiimage';
            $path = public_path() . $image_path;
            $filename = time().'_'.rand(000 ,999).'.'.$image->getClientOriginalExtension();
            $image->move($path, $filename);
            $paths[] = $image_path.'/'.$filename;
        }
        return implode(',',$paths);
    }

    public function DeletePrevious(){
        $images = explode(',',$this->image_path);

        foreach($images as $key => $image){
            if(file_exists(public_path().$image)){
                unlink(public_path().$image);
            }
        }
    }

    public function deletMultiimg(){
        $multi_images = explode(',', $this->related_image);
        foreach($multi_images as $key => $image){
            if(file_exists(public_path().$image)){
                unlink(public_path().$image);
            }

        }

    }

    public function deleteChild()
    {
        if($this->plans()->exists()){
            // delete all associated plans
            $this->plans()->delete();
        }
        else{
            return '';
        }

    }

}
