<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    
    protected $guarded= [];
    protected $appends = ['plans_id'];

    public function plans()
    {
        return $this->hasMany(CategoryHasPlans::class);
    }

    public function getPlansIdAttribute(){
		if($this->plans()->exists()){
			$months =  $this->plans()->get()->pluck('plan_id');
            return $months;
		}
        else{
			return '';
		}
	}

    public static function InsertImage($image){

        $fileOrignalName = $image->getClientOriginalName();
        $image_path = '/categories';
        $path = public_path() . $image_path;
        $filename = time().'_'.rand(000 ,999).'.'.$image->getClientOriginalExtension();
        $image->move($path, $filename);
        return $image_path.'/'.$filename;
        
    }

    public function syncplans($plans)
    {

        $this->deleteChild();
        
        foreach($plans as $key => $value){

            CategoryHasPlans::create(['category_id' => $this->id , 'plan_id' => $value]);

        }
    }

    public function revokeplans()
    {
        $this->deleteChild();
    }

    public function deleteChild()
    {
        if($this->plans()->exists()){
            // delete all associated plans
            $this->plans()->delete();
        }
        else{
            return '';
        }    
        
    }
 
}
