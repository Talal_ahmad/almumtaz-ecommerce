<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;
    protected $guarded=[];
    
    public static function brandImage($brand){
        $fileoriginalname = $brand->getClientOriginalName();
        $image_path = '/brands';
        $path = public_path() . $image_path;
        $filename = time() . '_' . rand(000, 999) . '.' .$brand->getClientOriginalExtension();
        $brand->move($path, $filename);
        return $image_path. '/' . $filename;
    }
    public function DeletePrevious(){
        // dd($this->image);
        $images = explode(',',$this->image_path);
        // dd($images);
        foreach($images as $key => $image){
            if($image)
            {
                unlink(public_path().$image);
            }   
        }
    }
}
