<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "orders";
    protected $guarded = [];
    use HasFactory;
    
    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    public static function sendsingleMessage($user_id,$get_heading,$get_content) {
        $content = array(
            "en" => $get_content
            );
            $headings=array(
                'en'=>$get_heading
            );
        $fields = array(
            'app_id' => '461d50b9-2b91-404d-87ac-362c9e5071a5',
            'include_player_ids' => array($user_id),
            'data' => array("type" => "0"),
            'contents' => $content,
            'headings'=>$headings
        );
        
        $fields = json_encode($fields);

        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
        
        // echo $response;
    }
}
