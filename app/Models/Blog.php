<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;
    protected $fillable = ['id','title','image','content'];

    public function BlogSave($arrData)
    {
        Blog::create($arrData);
        return true;
    }


    public static function InsertImage($image){

        $fileOrignalName = $image->getClientOriginalName();
        $image_path = '/blogs_images';

        
      
        $path = public_path() .$image_path;
        
        $filename = time().'_'.rand(000 ,999).'.'.$image->getClientOriginalExtension();
        
        $image->move($path, $filename);
        return $filename;
        
    }

    public function DeletePrevious(){
      
        $images = explode(',',$this->image);
        
    
        foreach($images as $key => $image){

            if($image)
            {
                
               return unlink(public_path().'/blogs_images/'.$image);
            }
            else
            {
                echo "file not found";
            }
             
               
           
        }
        


    }

}
