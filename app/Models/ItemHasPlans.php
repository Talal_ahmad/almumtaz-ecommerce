<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemHasPlans extends Model
{
    use HasFactory;
    protected $table = 'item_has_plans';
    protected $guarded = [];
    public $timestamps = false;

    public function item()
    {
        return $this->belongsTo(Item::class, 'foreign_key', 'item_id');
    }

    public function plan()
    {
        return $this->belongsTo(InstallmentPlan::class, 'foreign_key', 'plan_id');
    }
}
