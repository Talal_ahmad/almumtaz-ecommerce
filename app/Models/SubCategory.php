<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    use HasFactory;
    protected $fillable = ['id','name','category_id'];


    public function categories()
    {
        return $this->belongsTo(Category::class,'category_id');
    }
    public function SUbCateSave($arrData)
    {
        SubCategory::create($arrData);
        return true;
    }
}
