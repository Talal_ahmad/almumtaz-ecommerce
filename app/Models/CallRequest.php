<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CallRequest extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = "request_calls";



    public function remark()
    {
        return $this->belongsTo(Remark::class,'remark_id');
    }

    public function customer()
    {
        return $this->belongsTo(User::class,'customer_id');
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class,'forword_to');
    }

    public function forwardedBy()
    {
        return $this->belongsTo(User::class,'forward_by');
    }
  
}


