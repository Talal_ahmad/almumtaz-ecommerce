<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerApplicationForm extends Model
{
    use HasFactory;
    protected $table = 'customer_application_form';
    protected $guarded = [];

    // Return Fields To Display
    public static function SelectedFields(){
        $fields = CustomerApplicationForm::first();
        if($fields){
            $data = explode(',',$fields->fields_to_display);
            return $data;
        }
        else{
            $data = [];
            return $data;
        }
    }

    // Return Converted String from array
    public static function ConvertToString($fields){
        $fieldstring = implode(',',$fields);
        return $fieldstring;
    }

    // Update existing record
    public static function UpdateRecord($data){
        $record = CustomerApplicationForm::first();
        CustomerApplicationForm::find($record->id)->fill($data)->save();
    }
}
