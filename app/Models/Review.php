<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    
    use HasFactory;
    protected $guarded = [];
    protected $appends = ['user'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getUserAttribute(){
		if($this->user()->exists()){
			return $this->user()->first()->name;
		}
        else{
			return '';
		}
	}
}
