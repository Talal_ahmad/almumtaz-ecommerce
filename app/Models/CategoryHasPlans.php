<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryHasPlans extends Model
{
    use HasFactory;
    protected $table = 'category_has_plans';
    protected $guarded = [];
    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo(Category::class, 'foreign_key', 'category_id');
    }

    public function plan()
    {
        return $this->belongsTo(InstallmentPlan::class, 'foreign_key', 'plan_id');
    }
}
