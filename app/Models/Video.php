<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    use HasFactory;
    protected $fillable = ['id','video_path','status'];

    public static function InsertImage($video){
        $fileOrignalName = $video->getClientOriginalName();
        $video_path = '/videos';
        $path = public_path() . $video_path;
        $filename = time().'_'.rand(000 ,999).'.'.$video->getClientOriginalExtension();
        // dd($filename);
        $video->move($path, $filename);
        return $video_path.'/'.$filename;
    }
}