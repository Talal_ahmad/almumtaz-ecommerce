<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Footer extends Model
{
    use HasFactory;
  protected $guarded=[];


  public static function footerImage($images){
    foreach($images as $key => $image){
      $fileOrignalName = $image->getClientOriginalName();
      $image_path = '/footer_iamge';
      $path = public_path() . $image_path;
      $filename = time().'_'.rand(000 ,999).'.'.$image->getClientOriginalExtension();
      $image->move($path, $filename);
      $paths[] = $image_path.'/'.$filename;
  }
  return implode(',',$paths);
}
public static function bannerfooter($banner){
  $fileOrignalName = $banner->getClientOriginalName();
  $image_path = '/banner_footer';
  $path = public_path() . $image_path;
  $filename = time().'_'.rand(000 ,999).'.'.$banner->getClientOriginalExtension();
  $banner->move($path, $filename);
  return $image_path.'/'.$filename;
}
public function DeletePrevious(){
    // dd($this->image);
    $images = explode(',',$this->image);
    // dd($images);
    foreach($images as $key => $image){

      if($image)
      {
        unlink(public_path().$image);
      }   
    }
  }
}

