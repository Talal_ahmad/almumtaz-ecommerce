<?php
namespace App\Helpers;

class JsonResponse {

    public static function send($code = 200, $msg = '',$data = []){
        $response['code'] = $code;
        $response['msg'] = $msg;
        $response['data'] = $data;
        response()->json($response, 200)
        ->header('Access-Control-Allow-Origin','*')
        ->send();
        die();
    }
}
