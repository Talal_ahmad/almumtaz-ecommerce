<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class CheckUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    public function handle(Request $request, Closure $next)
    {
        // Preventing Disabled users from logging in

        if(Auth::check()){

            $status = auth()->user()->status;

            if($status == 0){

                auth('web')->logout();

                $message = 'Your Account Is No Longer Active. Please Contact Administrator.';

                return redirect()->route('login')->withMessage($message);

            }

        }

        return $next($request);
    }
}
