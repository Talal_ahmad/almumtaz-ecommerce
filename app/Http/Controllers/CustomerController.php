<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Str;
use App\Models\User;
use DB;
use DataTables;
use Auth;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $customers = User::select('id','name','email','status', 'is_customer','phone_number')->where('is_customer', '=', 1);
                return  DataTables::eloquent($customers)
                ->addColumn('action', function($data)
                {
                    $button = '';
                    if(Auth::user()->can('Edit Customer'))
                    $button .='<i class="fas fa-edit edit" onclick=edit('.$data->id.') style="cursor:pointer" title="Edit"></i>';
                    if(Auth::user()->can('Delete Customer'))
                    $button .= '<i class="fas fa-trash delete text-danger" onclick=deleterow("'.$data->id.'") style="cursor:pointer;margin-left:3px" title="Delete"></i>';
                    return $button;
                })
                ->addColumn('status',function($data){
                    $status = ($data->status == 1) ? 'Active' : 'Inactive';
                    return $status;
                })->make(true);
            }
        return view ('customers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function login(Request $request)
    {
        // try
        // {
            // Validating Request Data
            $this->validate($request, [
                'email' => 'required|email',
            ]);
            $data = $request->all();
            if(Auth::attempt($data))
            {
                // return response()->json(Auth::attempt($data));
                return ['code'=>'200','message'=>'success', 'data'=>Auth::attempt($data)];
            }
            else{
                return ['code'=>'400','message'=>'failed', 'data'=>Auth::attempt($data)];
            }
        // }
        // catch(\Exception | ValidationException $e){
        //     if($e instanceof ValidationException){
        //         return ['code'=>'200','errors' => $e->errors()];
        //     }
        //     else{
        //         return ['code'=>'200','error_message'=>$e->getMessage()];
        //     }
        // }
    }
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try
        {
            // Validating Request Data
            $this->validate($request, [
                // 'first_name' => 'required',
                // 'last_name' =>'required',
                'phone_number' => 'required|unique:users',
            ]);
            if ($request->ajax()) {
                $data = $request->all();
                $data['name'] = $data['first_name']." ".$data['last_name'];
                $data['is_customer'] = 1;
                $data['password'] = Hash::make($request->password);
                $data['api_token'] = Str::random(45);
                User::create($data);
                return ['code'=>'200','message'=>'success'];
            }
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'400','errors' => $e->errors()];
            }
            else{
                return ['code'=>'300','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return $user->toArray();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        try
        {
            if ($request->ajax()) {
                $data = $request->all();
                $data['password'] = Hash::make($request->password);
                $user->fill($data)->save();
                return ['code'=>'200','message'=>'success'];
            }
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        try{
            // Model Binding
            $user->delete();

            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'200','error_message'=>$e->getMessage()];
        }
    }


    public function searchCustomer(Request $request)
    {

        if($request->searchkeyowrd)
        {
            $user = User::where('is_customer',1)
            ->where(function ($query) use ($request){
                $query->where('name', "like", "%" . $request->searchkeyowrd . "%");
                $query->orWhere('email', "like", "%" . $request->searchkeyowrd . "%");
                $query->orWhere('phone_number', "like", "%" . $request->searchkeyowrd . "%");
            })->get();

        }
        return response()->json($user);
    }
}
