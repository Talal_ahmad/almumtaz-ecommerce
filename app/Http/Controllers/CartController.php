<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;
use App\Models\Cart;
use App\Models\Customer;
use Auth;
use App\Helpers\JsonResponse;

class CartController extends Controller
{
    // Checking User is login or not
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        JsonResponse::send(200,'success',Cart::with('item')->where('user_id',Auth::user()->id)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Cart::where('user_id',Auth::user()->id)->where('item_id',$request->item_id)->count() > 0){
            JsonResponse::send(419,'Error');
        }
        $obj = new Cart;
        $obj->user_id = Auth::user()->id;
        $obj->item_id = $request->item_id;
        $obj->qty = 1;
        $obj->save();
        return ['code'=>'200','message'=>'success'];
    }

    public function updateCartItems(Request $request){
        // dd($request);
        // $items = $request['items'];
        // dd($items);
        foreach($request['items'] as $key => $item)
        {
            $cartitem = Cart::find($item['id']);
            $cartitem->qty = $item['qty'];
            $cartitem->update();
        }
        JsonResponse::send(200,'success');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::find($id)->delete();
        JsonResponse::send(200,'success');
    }

    public function getCartCount(){
        JsonResponse::send(200,'success',Cart::where('user_id',Auth::user()->id)->count());
    }
}
