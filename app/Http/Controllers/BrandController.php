<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\Company;
use Illuminate\Validation\ValidationException;
use DataTables;
use Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Helpers\JsonResponse;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd($request);
        if($request->ajax())
        {
            $brands = Brand::select('id','image_path','title','company_id');
            if (request()->website == true) {
                JsonResponse::send(200,'success',$brands->get());
            }
            else {
                return  DataTables::eloquent($brands)
                ->addColumn('image', function($data)
                {
                    $url = url('/').$data->image_path;
                    return $url;
                })
                ->addColumn('action', function($data)
                {
                    $button = '';
                    if(Auth::user()->can('Edit Brand'))
                    $button .='<i class="fas fa-edit edit" onclick=edit('.$data->id.') style="cursor:pointer" title="Edit"></i>';
                    if(Auth::user()->can('Delete Brand'))
                    $button .= '<i class="fas fa-trash delete text-danger" onclick=deleterow("'.$data->id.'") style="cursor:pointer;margin-left:3px" title="Delete"></i>';
                    return $button;
                })->make(true);
            }
        }
        $companies =Company::get();
        return view('brandimages.index',compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $this->validate($request, [
                'image_file' => 'required',
                'title' => 'required',
                'image_file' => 'required',
            ]);

            $data = $request->all();
            // dd($data);
            if ($request->hasfile('image_file'))
            {
                $data['image_path'] = Brand::brandImage($data['image_file']);
                unset($data['image_file']);
            }
            $data['created_by'] = Auth::id();
            // Creating new record
            Brand::create($data);
            return ['code'=>'200','message'=>'success'];

        }
        catch (\Exception | ValidationException $e)
        {
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brandimage)
    {

        return $brandimage->toArray();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brandimage)
    {
        //
        try {
            $this->validate($request,[
                // 'image_file' => 'required',
                'title' => 'required',
            ]);

            $data = $request->all();
            // dd($data);
            if (!empty($data['image_file'])) {
                // Remove Previous Image
                $brandimage->DeletePrevious();

                // Add New image
                $data['image_path'] = Brand::brandImage($data['image_file']);

                unset($data['image_file']);
            }

            $data['updated_by'] = Auth::id();

            // Updating existing record
            $brandimage->fill($data)->save();

            return ['code'=>'200','message'=>'success'];

        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brandimage)
    {
        try{
            // Remove Image
            $brandimage->DeletePrevious();
            // Model Binding
            $brandimage->delete();

            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'200','error_message'=>$e->getMessage()];
        }
    }
}
