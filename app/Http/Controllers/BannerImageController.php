<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Category;
use App\Models\Company;
use Auth;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class BannerImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $banners = Banner::select('banners.id', 'image', 'banners.name',"companies.name as companys", 'categories.name as category')
            ->leftjoin('companies','companies.id','=','banners.company_id')
            ->leftjoin('categories', 'categories.id', '=', 'banners.category_id');

            // dd($banners->get());
            return DataTables::eloquent($banners)
                ->addColumn('image', function ($data) {
                    $url = url('/') . $data->image;
                    return $url;
                })
                ->addColumn('action', function ($data) {
                    $button = '';
                    if (Auth::user()->can('Edit Banner')) {
                        $button .= '<i class="fas fa-edit edit" onclick=edit(' . $data->id . ') style="cursor:pointer" title="Edit"></i>';
                    }

                    if (Auth::user()->can('Delete Banner')) {
                        $button .= '<i class="fas fa-trash delete text-danger"  onclick=deleterow("' . $data->id . '") style="cursor:pointer;margin-left:3px" title="Delete"></i>';
                    }

                    return $button;
                })->make(true);
        }
        $companies =Company::get();
        $categories = Category::get();
        // dd($companies);
        return view('bannerimages.index', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            // Validating Request Data
            $this->validate($request, [
                'image_file' => 'required',
            ]);

            $data = $request->all();

            // Storing Image of Item
            if ($request->hasFile('image_file')) {

                $data['image'] = Banner::bannerImage($data['image_file']);

                unset($data['image_file']);
            }

            $data['created_by'] = Auth::id();

            // Creating new record
            Banner::create($data);

            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '200', 'errors' => $e->errors()];
            } else {
                return ['code' => '200', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $bannerimage)
    {

        return $bannerimage->toArray();
        // Banner::where('id',$id)->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $bannerimage)
    {

        try {

            // Validating Request Data
            $this->validate($request, [

                'image_file' => 'nullable | image ',
            ]);

            $data = $request->all();

            // Storing Image of Category
            if (!empty($data['image_file'])) {

                $bannerimage->DeletePrevious();
                // dd('hello');

                $data['image'] = Banner::bannerImage($data['image_file']);
                // dd($data);

                unset($data['image_file']);

            }

            $data['updated_by'] = Auth::id();

            // Updating existing record
            $bannerimage->fill($data)->save();

            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '200', 'errors' => $e->errors()];
            } else {
                return ['code' => '200', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $bannerimage)
    {
        try {
            // Model Binding
            $bannerimage->delete();

            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '200', 'error_message' => $e->getMessage()];
        }
    }

}