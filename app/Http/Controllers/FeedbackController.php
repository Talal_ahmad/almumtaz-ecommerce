<?php

namespace App\Http\Controllers;
use App\Models\Feedback;
use DataTables;
use Auth;
use Storage;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $data = Feedback::get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function($row){
                    $t = $row->status == 0 ? 'selected' : '';
                    $t2 = $row->status == 1 ? 'selected' : '';
                    $status = "<select class='custom-select status' id='$row->id'>
                        <option value= '1' $t2>Active</option>
                        <option value= '0' $t>Inactive</option>
                    </select>";
                    // dd($status);
                    return $status;
                })
                ->addColumn('action', function($row){
                    $btn ='&nbsp;<a href="javascript:void(0)" data-id="'.$row->id.'" class=" btn btn-danger btn-sm  dlt">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['status','action'])
                ->make(true);
        }
        return view('feedback.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $feedback = new Feedback;
        $feedback->name = $request->name;
        $feedback->email = $request->email;
        $feedback->phone = $request->phone;
        if($request->hasFile('picture')){
            $feedback_file = $request->file('picture');
            // dd($feedback_file);
            $feedback_file_name = rand().'.'. $feedback_file->getClientOriginalExtension();
            $feedback_file_path = $feedback_file->move(public_path(),$feedback_file_name);
        }
        $feedback->message = $request->message;
        $feedback->image_path= !empty($feedback_file_name) ? $feedback_file_name : NULL;
        $feedback->save();
        return response()->json("saved");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $feedback = Feedback::find($id);
        $feedback->delete();
        return response()->json('deleted');
    }

    public function status(Request $request)
    {
        $feedback = Feedback::find($request->id);
        $feedback->status = $request->status;
        $feedback->update();
        return response()->json(['success'=>'Status Updated successfully.']);
    }
}
