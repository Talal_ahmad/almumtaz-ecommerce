<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Disclaimer;
use App\Models\User;
use Illuminate\Validation\ValidationException;
use DataTables;
use Auth;
use DB;

class DisclaimerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Fetching all related data
        if ($request->ajax()) {
            // $Disclaimer = Disclaimer::get();
            $Disclaimer=Disclaimer::select('id','title');


            return  DataTables::eloquent($Disclaimer)->addColumn('action',function($data){
                $button = '';
                if(Auth::user()->can('Edit Company'))
                $button .='<i class="fas fa-edit edit" id="'.$data->id.'" data-title="'.$data->title.'"  style="cursor:pointer" title="Edit">';
                // if(Auth::user()->can('Delete Company'))
                // $button .= '</i><i class="fas fa-trash delete text-danger"  onclick=deleterow("'.$data->id.'") style="cursor:pointer;margin-left:3px" title="Delete"></i>';
            return $button;
            })->make(true);
        }
        return view('disclaimer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            // Validating Request Data
            $this->validate($request, [
                'title' => 'required',
            ]);

            $data = $request->all();

            // $data['created_by'] = Auth::id();

            // Creating new record
            Disclaimer::create($data);

            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Disclaimer $Disclaimer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Disclaimer $Disclaimer)
    {
        try{
            // Validating Request Data
            $this->validate($request, [
                'title' => 'required ']);

            $data = $request->all();

            // $data['updated_by'] = Auth::id();

            // Updating existing record
            $Disclaimer->fill($data)->save();

            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Disclaimer $Disclaimer)
    {
        try{
            // Model Binding
            $Disclaimer->delete();

            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'200','error_message'=>$e->getMessage()];
        }
    }
}
