<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Footer;
use Illuminate\Validation\ValidationException;
use DataTables;
use Auth;
use App\Helpers\JsonResponse;

class FooterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $footers=Footer::select('image', 'id');
        return  DataTables::eloquent($footers)
        ->addColumn('image',function($data){
            $url = url('/').$data->image;
            return $url;
        })
        ->addColumn('action',function($data){
            $button = '';
            if(Auth::user()->can('Edit Footer'))
            $button .='<i class="fas fa-edit edit" onclick=edit('.$data->id.') style="cursor:pointer" title="Edit"></i>';
            if(Auth::user()->can('Delete Footer'))
            $button .= '<i class="fas fa-trash delete text-danger"  onclick=deleterow("'.$data->id.'") style="cursor:pointer;margin-left:3px" title="Delete"></i>';
        return $button;
        })->make(true);
        }
        
            return view('footerimages.index');
        
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            // Validating Request Data
            $this->validate($request, [
                'image_file' => 'required',
                'banner_file' => 'required',
            ]);

            $data=$request->all();

            // Storing Image of Item
            if($request->hasFile('image_file')){

                $data['image']=Footer::footerImage($data['image_file']);
               
                unset($data['image_file']);
            }  
            if($request->hasFile('banner_file')){

                $data['banner']=Footer::bannerfooter($data['banner_file']);
               
                unset($data['banner_file']);
            } 


            $data['created_by'] = Auth::id();
           
            // Creating new record
                 Footer::create($data);
               
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Footer $footerimage)
    { 
        // dd($footerimage);
        return $footerimage->toArray();
        // Footer::where('id',$id)->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Footer $footerimage)
    {
        
        try{
           
            // Validating Request Data
            $this->validate($request, [
               
                'image_file' => 'nullable | image ',
            ]);

            $data = $request->all();
          
           
            // Storing Image of Category
            if(!empty($data['image_file'])){

                $footerimage->DeletePrevious();
                // dd('hello');

                $data['image']=Footer::footerImage($data['image_file']);
                // dd($data);

                unset($data['image_file']);

            }

            $data['updated_by'] = Auth::id();

            // Updating existing record
            $footerimage->fill($data)->save();

            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Footer $footerimage)
    {
        $footerimage->delete();
    }
}
