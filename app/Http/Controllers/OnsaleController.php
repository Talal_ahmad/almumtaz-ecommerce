<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Onsale;
use App\Models\Company;
use App\Models\Category;
use Illuminate\Validation\ValidationException;
use DataTables;
use Auth;
class OnsaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::get();
        $company = Company::get();
        $onsales = Onsale::all();
        return view('onsale.index', compact('onsales','company','category'));
    }

    public function list(Request $request){


            $items=Onsale::select('id','name','image_path','price','description','item_type','season','discount_price','net_amount');

            return  DataTables::eloquent($items)
            ->addColumn('action',function($data){
                $button = '';
                if(Auth::user()->can('Edit Item'))
                $button .='<i class="fas fa-edit edit" onclick=edit('.$data->id.') style="cursor:pointer" title="Edit"></i>';
                if(Auth::user()->can('Delete Item'))
                $button .= '<i class="fas fa-trash delete text-danger"  onclick=deleterow("'.$data->id.'") style="cursor:pointer;margin-left:3px" title="Delete"></i>';
                return $button;
            })->make(true);



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            // Validating Request Data
            $this->validate($request, [
                'name' => 'required',
                'price' => 'required | numeric',
                'category_id' => 'required | numeric',
                // 'company_id' => 'required | integer',
                'image' => 'required',
                'image.*' => 'image',
                'description' => 'required',
            ]);

            $data=$request->all();

            // Storing Image of Item
            if(!empty($data['image'])){

               $data['image_path']=Onsale::InsertImage($data['image']);

               unset($data['image']);
            }
            $data['discount_price'] = $data['discount_price'] != null ? $data['discount_price'] : 0;

            $data['net_amount']= $data['price'] - ($data['price'] * $data['discount_price'] / 100);

            // unset($data['plans']);

            // $data['created_by'] = Auth::id();

            // Creating new record
            $item = Onsale::create($data);



            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Onsale $onsale)
    {
      return $onsale->toArray();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Onsale $onsale)
    {

        try{
            // Validating Request Data
            $this->validate($request, [
                'name' => 'required',
                'price' => 'required | numeric',
                // 'company_id' => 'required | integer',
                // 'image' => 'required',
                'image.*' => 'image',
                'description' => 'required',
            ]);

            $data=$request->all();
            // Storing Image of onsale
            if(!empty($data['image'])){

                $onsale->DeletePrevious();


                $data['image_path']=Onsale::InsertImage($data['image']);

                unset($data['image']);

            }

                $data['net_amount']= $data['price'] - ($data['price'] * $data['discount_price'] / 100);

            // unset($data['plans']);

            // $data['updated_by'] = Auth::id();

            // Updating existing record
            $onsale->fill($data)->save();

            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Onsale $onsale)
    {

        try{

            // Model Binding
            $onsale->delete();

            return ['code'=>'200','message'=>'success'];

        }
        catch(\Exception $e){
            return ['code'=>'200','error_message'=>$e->getMessage()];
        }
    }
}
