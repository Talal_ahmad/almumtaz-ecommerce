<?php

namespace App\Http\Controllers;
use App\Models\User;
use Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Helpers\JsonResponse;

class ApploginController extends Controller
{
    public function register(Request $request)
    {
            // Validating Request Data
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'password' => 'required',
            ]);
                $data = $request->all();
                $data['is_customer'] = 1;
                $data['api_token'] = Str::random(45);
                $data['password'] = Hash::make($request->password);
                User::create($data);
                return ['code'=>'200','message'=>'success'];
    }

    public function login(Request $request)
{
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);
        if(Auth::attempt($request->all()))
        {
        $user = Auth::user();
        $user->api_token = Str::random(45);
        $user->save();
            return response()->json(['code'=>'200','message'=>'success', 'user' => $user]);
        }
        else {

            return response()->json(['code'=>'300','message'=>'failed',]);
        }

}

}
