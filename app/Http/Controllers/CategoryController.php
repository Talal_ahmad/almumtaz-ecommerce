<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\InstallmentPlan;
use Illuminate\Validation\ValidationException;
use DataTables;
use Auth;
use App\Helpers\JsonResponse;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // Checking User is login or not
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if($request->ajax())
        {

            $users=Category::select('id','name','image_path','popular');



                return  DataTables::eloquent($users)->addColumn('action',function($data){
                    $button = '';
                    if(Auth::user()->can('Edit Category'))
                    $button .='<i class="fas fa-edit edit" onclick=edit('.$data->id.') style="cursor:pointer" title="Edit"></i>';
                    if(Auth::user()->can('Delete Category'))
                    $button .= '<i class="fas fa-trash delete text-danger"  onclick=deleterow("'.$data->id.'") style="cursor:pointer;margin-left:3px" title="Delete"></i>';
                return $button;
                })->make(true);

        }
        $plans=InstallmentPlan::select('id','name')->get();
        return view('category.index',compact('plans'));
    }

    // Returning data via server side datatable


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            // Validating Request Data
            $this->validate($request, [
                'name' => 'required |unique:categories,name',
                'image' => 'required | image ',
            ]);

            $data = $request->all();

            // Storing Image of Category
            if($request->hasFile('image')){

                $data['image_path']=Category::InsertImage($data['image']);

                unset($data['image']);
            }

            unset($data['plans']);

            $data['created_by'] = Auth::id();

            // Creating new record
            $category = Category::create($data);

            // Assigning Installment Plans to category
            if($request->filled('plans')){

                $category->syncplans($request->plans);
            }

            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return $category->toArray();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        try{
            // Validating Request Data
            $this->validate($request, [
                'name' => 'required | unique:categories,name,'.$category->id,
                'image' => 'nullable | image ',
            ]);

            $data = $request->all();
            if($request->popular)
            {
                $data['popular'] = 1;
            }
            else{
                $data['popular'] = 0;
            } 

            // Storing Image of Category
            if($request->hasFile('image')){

                // Deleting previous image
                if($category->image_path){
                    unlink(public_path().$category->image_path);
                }

                $data['image_path'] = Category::InsertImage($data['image']);

                unset($data['image']);
            }

            unset($data['plans']);
            // unset($data['popular']);

            $data['updated_by'] = Auth::id();

            // Updating existing record
            $category->fill($data)->save();

            // Assigning Installment Plans to category
            if($request->filled('plans')){

                $category->syncplans($request->plans);

            }
            else
            {
                // Revoking Installment Plans from category
                $category->revokeplans();
            }

            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {

        // return $category;
        try{
            // Model Binding
            $category->delete();

            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'200','error_message'=>$e->getMessage()];
        }
    }
}
