<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Review;
use DataTables;
use App\Helpers\JsonResponse;


class ReviewController extends Controller
{
    // Checking User is login or not
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $data = Review::get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function($row){
                if($row->under_review == 1){
                    $status = '<span class="badge rounded-pill bg-success">Accepted</span>';
                }
                elseif($row->under_review == 0){
                    $status = '<span class="badge rounded-pill bg-danger">Rejected</span>';
                }
                else {
                    $status = '<span class="badge rounded-pill bg-info">Pending</span>';
                }
                    return $status;
                })
                ->addColumn('action', function($row){
                    if($row->under_review == 1){
                        $btn ='&nbsp;<button class="btn btn-danger reject" data-id="'.$row->id.'">Reject</button>';
                    }
                    elseif($row->under_review == 0)
                    {
                        $btn = '<button class="btn btn-primary accept" data-id="'.$row->id.'">Accept</button>';
                    }
                    else{
                         $btn = '<button class="btn btn-primary accept" data-id="'.$row->id.'">Accept</button>';
                         $btn .='&nbsp;<button class="btn btn-danger reject" data-id="'.$row->id.'">Reject</button>';
                    }

                    return $btn;
                })
                ->rawColumns(['status', 'action'])
                ->make(true);
        }

        return view('review.index');
    }



    // public function changestatus(Request $request)
    // {


    //     $user = Review::find($request->user_id);
    //     $user->status = $request->status;
    //     $user->save();
    // }

    public function under_review(Request $request)
    {
        if($request->accept_id)
        {
            $proceed = Review::find($request->accept_id);
            $proceed->under_review = 1;
            $proceed->save();
        }
        else
        {
            $proceed = Review::find($request->reject_id);
            $proceed->under_review = 0;
            $proceed->save();
        }
        return response()->json($proceed);
    }

    public function reject(Request $request)
    {
        $proceed = Review::find($request->user_id);
         $proceed->under_review = 0;
        $proceed->save();
        return response()->json("Rejected");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'rating' => 'required',
            'message' => 'required',
        ]);

        Review::create([
            'user_id' => \Auth::user()->id,
            'item_id' => $request->item_id,
            'message' => $request->message,
            'rating' => $request->rating,
        ]);
        JsonResponse::send(200, 'Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
