<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\CallRequest;
use App\Models\Remark;
use DataTables;
use App\Helpers\JsonResponse;
use Auth;
use DB;
class CallRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $call_request = CallRequest::get();
        // dd($call_request);
        $remarks = Remark::get();
        return view('callrequest.index',compact('call_request','remarks'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


     public function callRequest(Request $request)
     {
        if (!empty($request->header('token'))) {

            $user = DB::table('users')
                ->where('api_token', $request->header('token'))
                ->value('id');
            $data = $request->all();
            $data = CallRequest::create($data);
            JsonResponse::send(200,'Success');

        }else {

            $data = $request->all();
            $data = CallRequest::create($data);
            JsonResponse::send(200,'Success');
        }
        //
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addRemark(Request $request)
    {
    
        $remarks = CallRequest::find($request->id);
        $remarks->remark_id = $request->remark_id;
        $remarks->cancel_request = 1;
        $remarks->save();
       return response()->json('success');

    }
}
