<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Video;
use Illuminate\Validation\ValidationException;
use DataTables;
use Auth;


class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $video = Video::get();
        if($request->ajax())
        {
        $users=Video::select('id','video_path','status');
        return  DataTables::eloquent($users)
        ->addColumn('action',function($data){
            $button = '';    
            if(Auth::user()->can('Edit Setting'))
            $button .='<i class="fas fa-edit edit" title="Edit User" onclick=edit('.$data->id.') style="cursor:pointer" title="Edit"></i>';
            if(Auth::user()->can('Delete Setting'))
            $button .= '<i class="fa fa-trash delete text-danger" title="Delete User"  onclick=deleterow("'.$data->id.'") style="cursor:pointer;margin-left:3px" title="Delete"></i>';
            return $button;
        })->make(true);
    }
        return view('video.index',compact('video'));
  }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Video $video)
    {
        try{
            // dd($request);
            // Validating Request Data
            $this->validate($request, [
                'video_path' => 'required',
            ]);
            $data=$request->all();
            // Creating new record
            $video = Video::create($data);
            // Assigning Installment Plans to item
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        } 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        return $video->toArray();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        try{
            // Validating Request Data
            $this->validate($request, [
                'video_path' => 'required',
                // 'video.*' => 'video',
               
            ]);
            $data=$request->all();
            // if($request->hasFile('video')){
                
            //      // Deleting previous image
            //      if($video->video_path){
            //         unlink(public_path().$video->video_path);    
            //     }
            //    $data['video_path'] = Video::InsertImage($data['video']);
            //    unset($data['video']);
            // }

            // Creating new record
            $video->fill($data)->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }   
      }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
       
        try{
            
            // Model Binding
            $video->delete();
            
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'200','error_message'=>$e->getMessage()];
        }
    }
}