<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Validation\ValidationException;
use DataTables;
use DB;
use Auth;

class RoleController extends Controller
{
    // Checking User is login or not
    public function __construct(){
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Fetching related data

        if($request->ajax())
        {
            
        $plans= Role::select();

        return  DataTables::eloquent($plans)
            ->addColumn('action',function($data){
            $button = '';    
            if(Auth::user()->can('Edit Setting'))
            $button .='<i class="fas fa-edit edit" onclick=edit('.$data->id.') style="cursor:pointer" title="Edit"></i>';
            if(Auth::user()->can('Delete Setting'))
            $button .= '<i class="fas fa-trash delete text-danger"  onclick=deleterow("'.$data->id.'") style="cursor:pointer;margin-left:3px" title="Delete"></i>';
            return $button;
        })->make(true);
        }
        $permissions = Permission::get();
        return view('roles.index',compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{

            // Validating Request Data
            $this->validate($request, [
                'name' => 'required | unique:roles,name',
                'permissions' => 'required',
            ]);

            $data = $request->all();

            unset($data['permissions']);

            // Creating new record
            $obj = Role::create($data);

            // Assigning Permissions to role , Minimum 1 permission required
            $obj->syncPermissions($request->permissions);

            return ['code'=>'200','message'=>'success'];

        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        } 
    }

    // Returning data via server side datatable
    public function list(){

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $data = [];
        // Fetching related permissions
        $data['permissions'] = DB::table('role_has_permissions')->where('role_id',$role->id)->get()->pluck('permission_id');
        // Model Binding
        $data['role'] = $role; 

        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        try{

            // Validating Request Data
            $this->validate($request, [
                'name' => 'required|unique:roles,name,'.$role->id,
                'permissions' => 'required',
            ]);

            $data = $request->all();

            unset($data['permissions']);

            // Updating existing record
            $role->fill($data)->save();

            // Assigning Permissions to role , Minimum 1 permission required
            $role->syncPermissions($request->permissions);

            return ['code'=>'200','message'=>'success'];

        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        try{
            // Model Binding
            $role->delete();
            
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'200','error_message'=>$e->getMessage()];
        }
    }
}
