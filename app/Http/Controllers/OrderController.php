<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Item;
use App\Models\Cart;
use App\Models\OrderItem;
use App\Models\User;
use App\Models\Branch;
use App\Models\CallRequest;
use DataTables;
use DB;
use Auth;
use App\Helpers\JsonResponse;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // JsonResponse::send(200,'success',Cart::with('item')->where('user_id',Auth::user()->id)->get());
        if($request->ajax()){
            $data = DB::table('users')
            ->join('orders', 'orders.user_id', '=', 'users.id')
            ->select('users.name','orders.*')->get();
            return DataTables::of($data)
            ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<button type="button" class="btn btn-primary" onclick="item_details('.$row->id.')">Item Details</button>';
                    if($row->is_processed == 0 AND $row->is_canceled == 0 ){
                    $btn .='&nbsp;<button type="button" data-id="'.$row->id.'" class="btn btn-info process">Order Processed</button>';
                    $btn .='&nbsp;<button type="button" data-id="'.$row->id.'" class="btn btn-danger cancel">Cancel</button>';
                    }
                    return $btn;
                })
                ->addColumn('status', function($row){
                    if($row->is_processed == 1){
                        $status = '<span class="badge rounded-pill bg-success">Processed</span>';
                    }
                    elseif($row->is_canceled == 1){
                        $status = '<span class="badge rounded-pill bg-danger">Cancle</span>';
                    }else{
                        $status = '<span class="badge rounded-pill bg-info">Pending</span>';
                    }
                    return $status;
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('orderlist.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request['items']);
        $order_details = new Order;
        $order_details->user_id = $request->id;
        $order_details->phone_number=$request->phone_number;
        $order_details->country = $request->country;
        $order_details->city = $request->city;
        // $order_details->branch_id = $request->branch;
        // $order_details->timeframe = $request->timeframe;
        $order_details->postal_code = $request->zip;
        $order_details->delivery_address = $request->address;
        $order_details->total_amount = $request->total;
        $order_details->total_items = $request->total_items;
        $order_details->payment_method = $request->paymentMethod;
        $order_details->card_number = !empty($request->cardNumber) ? $request->cardNumber : NULL;
        $order_details->cvv_number = !empty($request->cvv) ? $request->cvv : NULL;
        $order_details->month = !empty($request->month) ? $request->month : NULL;
        $order_details->year = !empty($request->year) ? $request->year : NULL;
        $order_details->save();

        foreach($request['items'] as $key=>$value)
        {
            $order_item = json_decode($value);
            $obj = new OrderItem;
            $obj->order_id = $order_details->id;
            $obj->item_id = $order_item->item->id;
            $obj->quantity = $order_item->qty;
            $obj->price = $order_item->item->price;
            $obj->discount = $order_item->item->discount_price;
            $obj->onsale = $order_item->item->onsale;
            $obj->net_amount = $order_item->item->total_price * $order_item->qty;
            $obj->save();

            $cart = Cart::where('item_id', $order_item->item->id)->first();
            $cart->delete();
        }
        $user = User::role('Admin')->pluck('onesignal_token');
        if(!empty($user))
        {
            Order::sendsingleMessage($user[0], 'New Order', 'You have new order!!');
        }
        return response()->json("saved");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order= Item::join('order_items', 'order_items.item_id', '=', 'items.id')
        ->select('items.name','order_items.*')->where('order_items.order_id',$id)->get();
         return response()->json($order);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $token)
    {
        $user = User::find(Auth::user()->id);
        $user->onesignal_token  = $token;
        $user->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function addOrder(Request $request)
    {
        $users = User::where('is_customer',1)->get();
        $items = Item::get();
        $branches = Branch::get();
        $customer_details = User::where('id', $request->query('cust_id'))->first();
        return view('orderlist.addorder', compact('users','items','branches', 'customer_details'));
    }

    public function getUserDetail(Request $request)
    {
        $user = User::select('name','phone_number')->where('id',$request->customer_id)->first();
       return response()->json($user);
    }

    public function getProduct(Request $request)
    {
        $items = Item::select('id as item_id','onsale','name','price','discount_price as discount','total_price')->where('id',$request->product_id)->first();
        $items->setAppends([]);
        return response()->json($items);
    }

    public function manualOrder(Request $request)
    {
        $data = $request->except('items');
        $data['manual_order'] = 1;
        $order = Order::create($data);
        $call_request = CallRequest::find($request->id);
        $call_request->customer_id = $order['user_id'];
        $call_request->order_id = $order['id'];
        $call_request->forword_to = $request->branch_id;
        $call_request->forward_by = Auth::user()->id;
        $call_request->is_forward = 1;
        $call_request->forward_time = $order['created_at'];
        $call_request->save();
        foreach ($request->items as $value) {
            $value['order_id'] = $order['id'];
            unset($value['name']);
            unset($value['total_price']);
            OrderItem::create($value);
        }

        return response()->json(['success' => 'Order has been Placed Sucessfully!']);
    }

    public function orderstatus(Request $request)
    {
        if($request->processed)
        {
            $proceed = Order::find($request->processed);
            $proceed->is_processed = 1;
            $proceed->save();
        }
        else
        {
            $proceed = Order::find($request->cancel);
            $proceed->is_canceled = 1;
            $proceed->save();
        }
        return response()->json($proceed);
    }
}
