<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\SubCategory;
use Auth;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $users = SubCategory::select('id', 'name', 'category_id')->with('categories');

            return DataTables::eloquent($users)
                ->addColumn('action', function ($data) {

                    $button = '';
                    if (Auth::user()->can('Edit SubCategory')) {
                        $button .= '<i class="fas fa-edit edit" onclick=edit(' . $data->id . ') style="cursor:pointer" title="Edit"></i>';
                    }

                    if (Auth::user()->can('Delete SubCategory')) {
                        $button .= '<i class="fas fa-trash delete text-danger"  onclick=deleterow("' . $data->id . '") style="cursor:pointer;margin-left:3px" title="Delete"></i>';
                    }

                    return $button;

                })
                ->addColumn('c_name', function ($data) {
                    if (isset($data->categories->name)) {
                        $cate = $data->categories->name;
                    } else {
                        $cate = '';
                    }
                    return $cate;

                })->rawColumns(array("c_name", "action"))->make(true);

        }
        $categories = Category::select('id', 'name')->get();

        return view('subCategory.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            // Validating Request Data
            $this->validate($request, [
                'name' => 'required',
                // 'code' => 'required',
                'category_id' => 'required',
            ]);

            $data = $request->all();

            $data['created_by'] = Auth::id();

            // Creating new record
            $category = new SubCategory;
            $dds = $category->SUbCateSave($data);

            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception  | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '200', 'errors' => $e->errors()];
            } else {
                return ['code' => '200', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SubCategory $subcategory)
    {
        return $subcategory->toArray();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubCategory $subcategory)
    {
        try {
            // Validating Request Data
            $this->validate($request, [
                'name' => 'required',
                // 'code' => 'required',
                'category_id' => 'required',
            ]);

            $data = $request->all();

            $data['updated_by'] = Auth::id();

            // Updating existing record
            $subcategory->fill($data)->save();

            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception  | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '200', 'errors' => $e->errors()];
            } else {
                return ['code' => '200', 'error_message' => $e->getMessage()];
            }
        }}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCategory $subcategory)
    {
        try {
            // Model Binding
            $subcategory->delete();

            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '200', 'error_message' => $e->getMessage()];
        }
    }
}
