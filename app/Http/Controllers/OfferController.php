<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Offer;
use App\Models\Item;
use DataTables;
use DB;


class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $data = DB::table('offers')
            ->join('items','offers.item_id','=','items.id')->select('offers.*','items.name as item_name')->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="javascript:void(0)" data-toggle="editmodal"  data-id="'.$row->id.'" onclick="edit('.$row->id.')" class="btn btn-primary btn-sm editbtn">Edit</a>';
                    $btn .='&nbsp;<a href="javascript:void(0)" data-id="'.$row->id.'" class=" btn btn-danger btn-sm  dlt">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        $offer = Offer::get();
        $item = Item::select('id','name')->get();
        return view('offer.index',compact('offer','item'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            // Validating Request Data
            $this->validate($request, [
                'item_id' => 'required',
                'price' => 'required',
                'start_date' => 'required|date|after:yesterday',
                'end_date' =>   'required|date|after:start_date',
            ]);
        $data = $request->all();
        $offer = Offer::create($data);
        return response()->json('success');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $offer = Offer::find($id);
        return response()->json($offer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {

        $this->validate($request, [
            'item_id' => 'required',
            'price' => 'required',
            'start_date' => 'required|date|after:yesterday',
            'end_date' =>   'required|date|after:start_date',
        ]);
            $data = $request->all();
            $offer->fill($data)->save();
            return response()->json('success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $offer = Offer::find($id)->delete();
        return response()->json('Delete Sucessfully');
    }
}
