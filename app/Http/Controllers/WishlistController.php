<?php

namespace App\Http\Controllers;
use App\Models\Wishlist;
use DataTables;
use Auth;
use Illuminate\Http\Request;
use App\Helpers\JsonResponse;

class WishlistController extends Controller
{
    // Checking User is login or not
    public function __construct(){
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        JsonResponse::send(200,'success',Wishlist::with('item')->where('user_id',Auth::user()->id)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Wishlist::where('user_id',Auth::user()->id)->where('item_id',$request->item_id)->count() > 0){
            JsonResponse::send(419,'Error');
        }
        $obj = new Wishlist;
        $obj->user_id = Auth::user()->id;
        $obj->item_id = $request->item_id;
        $obj->save();
        JsonResponse::send(200,'Success');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wishlist = Wishlist::find($id)->delete();
        jsonResponse::send(200,'success');
    }
}
