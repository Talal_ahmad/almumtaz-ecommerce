<?php

namespace App\Http\Controllers;

use App\Helpers\JsonResponse;
use App\Models\Banner;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Company;
use App\Models\Feedback;
use App\Models\Footer;
use App\Models\Item;
use App\Models\Offer;
use App\Models\Review;
use App\Models\User;
use App\Models\Video;
use Auth;
use DB;
use Illuminate\Http\Request;

class WebsiteController extends Controller
{
    public function getCategory(Request $request)
    {

        $categories = Category::select('id', 'name', 'image_path', 'description');
        $data["categories"] = $categories->get();
        $data["popularCategories"] = $categories->where('popular', 1)->get();
        JsonResponse::send(200, 'success', $data);
    }

    public function getCompany(Request $request)
    {
        $company = Company::select('id', 'name');
        JsonResponse::send(200, 'success', $company->get());
    }
    public function getOffer(Request $request)
    {

        $offer = Offer::join('items', 'offers.item_id', '=', 'items.id')
            ->select('items.name', 'items.image_path', 'offers.price', 'offers.start_date', 'offers.end_date')->first();
        if (!empty($offer->end_date)) {
            $offer->end_date = date("F j, Y", strtotime($offer->end_date));
        }
        JsonResponse::send(200, 'success', $offer);
    }

    public function getFooter(Request $request)
    {
        $footers = Footer::select('id', 'banner', 'image')->first();
        $banner = explode(",", $footers->image);
        $footers['image'] = $banner;

        JsonResponse::send(200, 'success', $footers);
    }

    public function getItem()
    {
        // dd(request()->all());
        // if (request()->categofilterByCategoriesry && request()->filterByCompanies) {
        //     dd('test');
        //     JsonResponse::send(
        //         200,
        //         'success',
        //         Item::when(request()->has('filterByCompanies'), function ($query) {
        //             return $query->whereIn('company_id', request()->filterByCompanies);
        //         })
        //             ->when(request()->has('filterByCategories'), function ($query) {
        //                 return $query->whereIn('category_id', request()->filterByCategories);
        //             })
        //             ->where('status', '=', 'Active')
        //             ->paginate(20)
        //             ->appends(request()->query())
        //     );

        // } elseif (request()->onsale == 1) {
        //     JsonResponse::send(
        //         200,
        //         'success',
        //         Item::where('onsale', 1)
        //             ->where('status', '=', 'Active')
        //             ->paginate(20)
        //             ->appends(request()->query())
        //     );
        // } elseif (request()->newArival == 2) {
        //     JsonResponse::send(
        //         200,
        //         'success',
        //         Item::where('new_arrival', 2)
        //             ->where('status', '=', 'Active')
        //             ->paginate(20)
        //             ->appends(request()->query())
        //     );
        // } elseif (request()->category) {
        //     // dd('test');
        //     JsonResponse::send(
        //         200,
        //         'success',
        //         Item::join('categories', 'items.category_id', '=', 'categories.id')->where('status', '=', 'Active')->where('categories.name', 'like', '%' . request()->category . '%')->select('items.*')
        //             ->paginate(20)
        //             ->appends(request()->query())
        //     );
        // } elseif (request()->search_keyword) {
        //     JsonResponse::send(
        //         200,
        //         'success',
        //         Item::join('categories', 'items.category_id', '=', 'categories.id')
        //             ->join('companies', 'items.company_id', '=', 'companies.id')
        //             ->where('status', '=', 'Active')
        //             ->where('items.name', 'like', '%' . request()->search_keyword . '%')
        //             ->orWhere('companies.name', 'like', '%' . request()->search_keyword . '%')
        //             ->orWhere('categories.name', 'like', '%' . request()->search_keyword . '%')->select('items.*')
        //             ->paginate(20)
        //             ->appends(request()->query())
        //     );
        // } elseif (request()->filterByCompanies) {
        //     JsonResponse::send(
        //         200,
        //         'success',
        //         Item::whereIn('company_id', request()->filterByCompanies)
        //             ->where('status', '=', 'Active')
        //             ->paginate(20)
        //             ->appends(request()->query())
        //     );
        // } elseif (request()->filterByCategories) {
        //     JsonResponse::send(
        //         200,
        //         'success',
        //         Item::whereIn('category_id', request()->filterByCategories)
        //             ->where('status', '=', 'Active')
        //             ->paginate(20)
        //             ->appends(request()->query())
        //     );
        // } elseif (request()->company) {
        //     JsonResponse::send(
        //         200,
        //         'success',
        //         Item::where('company_id', request()->company)
        //             ->where('status', '=', 'Active')
        //             ->paginate(20)
        //             ->appends(request()->query())
        //     );
        // } elseif (request()->filterByRange) {
        //     $min = request()->filterByRange;
        //     $max = request()->filterByRange1;
        //     JsonResponse::send(
        //         200,
        //         'success',
        //         Item::where([
        //             ['total_price', '>=', $min],
        //             ['total_price', '<=', $max],
        //         ])
        //             ->where('status', '=', 'Active')
        //             ->paginate(20)
        //             ->appends(request()->query())
        //     );
        // } else {
        //     // dd('test2');
        //     JsonResponse::send(200, 'success', Item::where('status', '=', 'Active')->paginate(20)->appends(request()->query()));
        // }
        // Default query with 'Active' status
        $query = Item::where('status', '=', 'Active');

        JsonResponse::send(200, 'success', $query

                ->when(request()->onsale == 1, function ($q) {
                    return $q->where('onsale', 1)->select('items.*');
                })
                ->when(request()->newArival == 2, function ($q) {
                    return $q->where('new_arrival', 2)->select('items.*');
                })
                ->when(request()->category, function ($q) {
                    return $q->join('categories', 'items.category_id', '=', 'categories.id')
                        ->where('categories.name', 'like', '%' . request()->category . '%')
                        ->select('items.*');

                })
                ->when(request()->search_keyword, function ($q) {
                    return $q->join('categories', 'items.category_id', '=', 'categories.id')
                        ->join('companies', 'items.company_id', '=', 'companies.id')
                        ->where(function ($query) {
                            $query->where('items.name', 'like', '%' . request()->search_keyword . '%')
                                ->orWhere('companies.name', 'like', '%' . request()->search_keyword . '%')
                                ->orWhere('categories.name', 'like', '%' . request()->search_keyword . '%');
                        })
                        ->select('items.*');
                })
                ->when(request()->filterByCompanies, function ($q) {
                    return $q->whereIn('company_id', request()->filterByCompanies)->select('items.*');
                })
                ->when(request()->filterByCategories, function ($q) {
                    return $q->whereIn('category_id', request()->filterByCategories)->select('items.*');
                })
                ->when(request()->company, function ($q) {
                    return $q->where('company_id', request()->company)->select('items.*');
                })
                ->when(request()->category2, function ($q) {
                    return $q->where('category_id', request()->category2)->select('items.*');
                })
                ->when(request()->filterByRange, function ($q) {
                    $min = request()->filterByRange;
                    $max = request()->filterByRange1;
                    return $q->where([
                        ['total_price', '>=', $min],
                        ['total_price', '<=', $max],
                    ])->select('items.*');
                })
                ->paginate(20)
                ->appends(request()->query())
        );

    }

    public function getBlog(Request $request)
    {
        $blogs = Blog::select('id', 'image', 'title', 'content', DB::Raw('Date(created_at) as date'));
        JsonResponse::send(200, 'success', $blogs->get());
    }

    public function getVideo(Request $request)
    {
        $vedios = Video::select('id', 'video_path', 'status');
        JsonResponse::send(200, 'success', $vedios->get());
    }

    public function getfeatureditems(Request $request)
    {
        // DB::connection()->enableQueryLog();
        $featured = Item::select('id', 'name', 'image_path', 'price', 'total_price', 'description', 'onsale', 'new_arrival')->where('status', '=', 'Active')
            ->where(function ($query) {
                $query->where('onsale', 1)->orWhere('new_arrival', 2);
            })
            ->limit(10)
            ->get();

        // $queries = DB::getQueryLog();
        // dd(end($queries));

        JsonResponse::send(200, 'success', $featured);
    }

    public function getdeals(Request $request)
    {
        $request->id ? $deals = Item::where('company_id', $request->id)->where('status', '=', 'Active')->where('onsale', 1)->where('new_arrival', 2) : $deals = Item::where('status', '=', 'Active')->where('onsale', 1)->orWhere('new_arrival', 2);
        JsonResponse::send(200, 'success', $deals->get());
    }

    public function getnewarrivals(Request $request)
    {
        $request->id ? $new_arrivals = Item::where('company_id', $request->id)->where('status', '=', 'Active')->where('new_arrival', 2) : $new_arrivals = Item::where('status', '=', 'Active')->where('new_arrival', 2);
        JsonResponse::send(200, 'success', $new_arrivals->get());
    }

    public function getonsales(Request $request)
    {
        $request->id ? $on_sales = Item::where('company_id', $request->id)->where('status', '=', 'Active')->where('onsale', 1) : $on_sales = Item::where('status', '=', 'Active')->where('onsale', 1);
        JsonResponse::send(200, 'success', $on_sales->get());
    }

    public function ItemDetail()
    {
        $itemdetail = Item::where('id', request()->id)->first();

        $itemdetail->related_image = $itemdetail->related_image ? explode(",", $itemdetail->related_image) : [];

        $data['itemDetails'] = $itemdetail;

        $data['relatedItems'] = Item::where('category_id', $itemdetail->category_id)->limit(10)->get();
        JsonResponse::send(200, 'success', $data);
    }

    public function itemReviews()
    {

        $data['itemReviews'] = Review::where('item_id', request()->id)->where('under_review', '1')->paginate(5)->appends(request()->query());

        $data['itemReviewsSummary'] = Review::select('rating', DB::raw('count(*) as total_reviews'))->where('item_id', request()->id)->where('under_review', '1')->groupBy('rating')->orderBy('rating', 'desc')->get();

        JsonResponse::send(200, 'success', $data);
    }

    public function getpopularCategoryItems(Request $request)
    {
        $popularItem = Item::select('items.id', 'items.name', 'items.image_path', 'items.description', 'items.price', 'items.meta_tags')
            ->join('categories', 'categories.id', '=', 'items.category_id')
            ->where('categories.popular', 1);

        if ($request->id) {
            JsonResponse::send(
                200,
                'success',
                $popularItem
                    ->where('category_id', $request->id)
                    ->limit(3)
                    ->get()
            );
        } else {
            JsonResponse::send(200, 'success', $popularItem->limit(3)->get());
        }
    }

    public function blogDetail(Request $request)
    {
        if ($request->id) {
            $blogs = Blog::select('title', 'image', 'content')->where('id', $request->id);
            JsonResponse::send(200, 'success', $blogs->get());
        }
    }

    public function feedbacks(Request $request)
    {
        $feedback = Feedback::select('name', 'message', 'image_path')->where('status', 1);
        JsonResponse::send(200, 'success', $feedback->get());
    }

    public function getBanners()
    {
        $banners = Banner::select('id', 'image', 'name', 'company_id')->get();
        // dd($banners);
        JsonResponse::send(200, 'success', $banners);
    }
    public function getUsers()
    {
        // $users = User::select('id')->get();
        $users = auth()->user();
        // dd($users);
        JsonResponse::send(200, 'success', $users);
    }
    public function checkLogin()
    {
        // dd('test');
        if (!empty(Auth::user())) {
            // dd('test');
            JsonResponse::send(200, 'success');
        } else {
            // dd('test');
            JsonResponse::send(419, 'Error');
        }
    }
    public function filterByRange()
    {
        $data['max'] = item::max('total_price');
        $data['min'] = item::min('total_price');
        // dd($data['min']);
        JsonResponse::send(200, 'success', $data);

    }
    public function getnewoffer()
    {
        $data = item::where('offer', 3)->get();
        JsonResponse::send(200, 'success', $data);

    }

}
