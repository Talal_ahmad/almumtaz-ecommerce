<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Branch;
use App\Models\User;
use Illuminate\Validation\ValidationException;
use DataTables;
use Auth;
use DB;

class BranchController extends Controller
{
    // Checking User is login or not
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        // Fetching all related data
        if($request->ajax())
        {
            $users=Branch::select('branches.id','branches.name as branch','address',"users.name as manager")->join('users','users.id','=','branches.manager_id');

            return  DataTables::eloquent($users)->addColumn('action',function($data){
                $button = '';
                if(Auth::user()->can('Edit Branch'))
                $button .='<i class="fas fa-edit edit" onclick=edit('.$data->id.') style="cursor:pointer" title="Edit"></i>';
                if(Auth::user()->can('Delete Branch'))
                $button .= '<i class="fas fa-trash delete text-danger"  onclick=deleterow("'.$data->id.'") style="cursor:pointer;margin-left:3px" title="Delete"></i>';
    
                return $button;
                
            })->make(true);
    
        }
        $users=User::select('id','name')->get();    

        return view('branches.index',compact('users'));
    }

    // Returning data via server side datatable
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        try{
            // Validating Request Data
            $this->validate($request, [
                'name' => 'required | unique:branches,name',
                'manager_id' => 'required',
                'address' => 'required',
            ]);

            $data = $request->all();

            $data['created_by'] = Auth::id();

            // Creating new record
            Branch::create($data);

            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function show(Branch $branch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function edit(Branch $branch)
    {
        // Model Binding
        return $branch;
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Branch $branch)
    {
        try{
            // Validating Request Data
            $this->validate($request, [
                'name' => 'required | unique:branches,name,'.$branch->id,
                'manager_id' => 'required',
                'address' => 'required',
            ]);

            $data = $request->all();

            $data['updated_by'] = Auth::id();

            // Updating existing record
            $branch->fill($data)->save();

            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Branch $branch)
    {
        try{
            // Model Binding
            $branch->delete();

            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'200','error_message'=>$e->getMessage()];
        }
    }
}
