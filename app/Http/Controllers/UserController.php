<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use DataTables;
use DB;
use Auth;

class UserController extends Controller
{
    // Checking User is login or not
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $users= User::with('roles');
            return  DataTables::eloquent($users)
                ->addColumn('action',function($data){
                $button = '';
                if(Auth::user()->can('Edit Setting'))
                $button .='<i class="fas fa-edit edit" title="Edit User" onclick=edit('.$data->id.') style="cursor:pointer" title="Edit"></i>';
                if(Auth::user()->can('Delete Setting'))
                $button .= '<i class="fa fa-trash delete text-danger" title="Delete User"  onclick=deleterow("'.$data->id.'") style="cursor:pointer;margin-left:3px" title="Delete"></i>';
                return $button;
            })
            ->addColumn('role',function($data){
                $role = $data->roles;
                if(count($role) > 0){
                    $role = current(current($role))->name;
                    return $role;
                }
            })
            ->addColumn('status',function($data){
                $status = ($data->status == 1) ? 'Active' : 'Deactivated';
                return $status;
            })

            ->make(true);
        }
        $roles = Role::get();
        return view('users.index',compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

            // Validating Request Data
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required | same:confirm_password',
            ]);

            $data = $request->all();

            $data['password'] = Hash::make($request->password);
            $data['api_token'] =  Str::random(45);

            unset($data['role']);

            // Creating new record
            $user = User::create($data);

            // Assigning Role to user
            if(!empty($request->role)){

                $user->assignRole($request->role);
            }

            return ['code'=>'200','message'=>'success'];

        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    // Returning data via server side datatable
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {


        $data = [];
        // Fetching related role
        $data['role'] = DB::table('model_has_roles')->where('model_id',$user->id)->first();

        // Model Binding
        $data['user'] = $user;

        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        try{

            // Validating Request Data
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users,email,'.$user->id,
                'password' => 'same:confirm_password',
            ]);

            $data = $request->all();

            if(!empty($request->password)){
                $data['password'] = Hash::make($request->password);
            }
            else{
                unset($data['password']);
            }

            unset($data['role']);

            // Updating existing record
            $user->fill($data)->save();

            // Assigning Role to user
            if(!empty($request->role)){

                DB::table('model_has_roles')->where('model_id',$user->id)->delete();
                $user->assignRole($request->role);
            }

            return ['code'=>'200','message'=>'success'];

        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        try{
            // Model Binding
            $user->delete();

            return ['code'=>'200','message'=>'success'];
        }catch(\Exception $e){
            return ['code'=>'200','error_message'=>$e->getMessage()];
        }
    }
}
