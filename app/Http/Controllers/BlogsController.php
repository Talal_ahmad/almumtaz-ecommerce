<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;
use Illuminate\Validation\ValidationException;
use DataTables;
use Auth;
 
class BlogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        if($request->ajax()){
            $blogs=Blog::select('id','image','title');
            return  DataTables::eloquent($blogs)
            ->addColumn('image',function($data){
                $url = url('/').'/blogs_images/'.$data->image;
                return $url;
            })        
            ->addColumn('action',function($data){
                $button = '';
                $button .='<a class="edit" id="edit" value="'.$data->id.'"><i class="fas fa-edit "  style="cursor:pointer" title="Edit"></i></a>';
                
                $button .= '<i class="fas fa-trash delete text-danger" onclick=deleterow("'.$data->id.'")  style="cursor:pointer;margin-left:3px" title="Delete"></i>';
            return $button;
            })->addColumn('content',function($data){
                if(isset($data->content)){
                    $cate= $data->content;
                }
                else{
                    $cate='';
                }
                    return $cate ;
            }) ->rawColumns(array("content", "action"))->make(true);        
    }
      return view('blogs.index');
  }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blogs.createblog');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
        try{
            // Validating Request Data
            $this->validate($request, [
                'title' => 'required',
                'image_file' => 'required',
                'content' => 'required',
            ]);

            $data = $request->all();
        
            // Storing Image of Category
            if($request->hasFile('image_file')){

                $data['image']=Blog::InsertImage($data['image_file']);
 
                unset($data['image_file']);
            }
            

            $data['created_by'] = Auth::id();

            // // Creating new record
            $blogs = new Blog;
            $dds=$blogs->BlogSave($data);

            return redirect()->route('blog.index');
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    public function uploadPhoto(Request $request)
    {

        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
        
            $request->file('upload')->move(public_path('images'), $fileName);
   
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('images/'.$fileName); 
            $msg = 'Image uploaded successfully'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
               
            @header('Content-type: text/html; charset=utf-8'); 
            echo $response;
        }
  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
 
        $blogs = Blog::find($id);
  
       return view('blogs.editblog', compact('blogs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Blog $blog ) 
    {
       
       
       
 
        try{
            // Validating Request Data
            $this->validate($request, [
                'title' => 'required',
                'image_file' => 'required',
                'content' => 'required',
            ]);
        //    $blogs=Blog::find($id);
          
            $data = $request->all();
           
          

            // Storing Image of Blog
            if(!empty($data['image_file'])){
              
                
                $blog->DeletePrevious();
              
              
                $data['image']= Blog::InsertImage($data['image_file']);
                

                unset($data['image_file']);

            }

         
           
            // $data['updated_by'] = Auth::id();
            // Updating existing record
            $blog->fill($data)->save();

            return redirect()->route('blog.index')->with('message','Update successful');
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {

 
     
        try{
           
            $blog->delete();
            // Blog::where('id',$id)->delete();
            
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'200','error_message'=>$e->getMessage()];
        }
    }
}
