<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Company;
use App\Models\InstallmentPlan;
use App\Models\Item;
use App\Models\SubCategory;
use Auth;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ItemController extends Controller
{
    // Checking User is login or not
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        // dd($request->all());
        // Fetching all related data
        if ($request->ajax()) {
            $category_id = $request->input('categoryId');
            $editcategoryId = $request->input('editcategoryId');

            if ($category_id) {
                // dd($category_id);
                $subcategories = SubCategory::select('id', 'name')->where('sub_categories.category_id', $category_id)->get();
                // dd($subcategories->all());
                return response()->json($subcategories);
            }
            if ($editcategoryId) {
                // dd($category_id);
                $editsubcategories = SubCategory::select('id', 'name')->where('sub_categories.category_id', $editcategoryId)->get();
                // dd($subcategories->all());
                return response()->json($editsubcategories);
            }
            // dd($request->all());
            $items = Item::with(['company:id,name', 'category'])->select('items.*');
            if ($request->category_filter) {
                $items = $items->where('items.category_id', $request->category_filter);
            }
            if ($request->company_filter) {
                $items = $items->where('items.company_id', $request->company_filter);
            }
            // dd($items->first());
            // dd($request->all());
            return DataTables::eloquent($items)
            ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    $button = '';
                    if (Auth::user()->can('Edit Item')) {
                        $button .= '<i class="fas fa-edit edit" onclick=edit(' . $data->id . ') style="cursor:pointer" title="Edit"></i>';
                    }

                    if (Auth::user()->can('Delete Item')) {
                        $button .= '<i class="fas fa-trash delete text-danger"  onclick=deleterow("' . $data->id . '") style="cursor:pointer;margin-left:3px" title="Delete"></i>';
                    }

                    return $button;
                })
                ->make(true);
        }
        $companies = Company::select('id', 'name')->get();
        $categories = Category::select('id', 'name')->get();
        $items = Item::all();
        $plans = InstallmentPlan::select('id', 'name')->get();

        return view('items.index', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
    }

    public function addtocart()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        try {
            // Validating Request Data
            $this->validate($request, [
                'name' => 'required | unique:items,name',
                'price' => 'required | numeric',
                // 'discount_price' => 'required | numeric',
                'company_id' => 'required | integer',
                'category_id' => 'required | integer',
                'status' => 'required',
                'image' => 'required',
                'image.*' => 'image',

                'description' => 'required',
                'specification' => 'required',
            ]);
            // dd($request->all());
            $data = $request->all();
            // Storing Image of Item
            if (!empty($data['image'])) {

                $data['image_path'] = Item::InsertImage($data['image']);
                unset($data['image']);
            }
            if (!empty($data['files'])) {
                $data['related_image'] = Item::multiImage($data['files']);
                unset($data['files']);
            }

            unset($data['plans']);

            $data['created_by'] = Auth::id();
            $data['discount_price'] = $data['discount_price'] != null ? $data['discount_price'] : 0;
            $data['total_price'] = $data['price'] - ($data['price'] * $data['discount_price'] / 100);

            // Creating new record
            $item = Item::create($data);
            // Assigning Installment Plans to item
            if ($request->filled('plans')) {

                $item->syncplans($request->plans);
            }

            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception  | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '200', 'errors' => $e->errors()];
            } else {
                return ['code' => '200', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(Item $item)
    {
        // dd($item->toArray());
        return $item->toArray();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Item $item)
    {
        try {
            // Validating Request Data
            $this->validate($request, [
                'name' => 'required | unique:items,name,' . $item->id,
                'price' => 'required | numeric',
                'company_id' => 'required | integer',
                'category_id' => 'required | integer',
                'status' => 'required',
                'image' => 'nullable',
                'image.*' => 'image',
                'description' => 'required',
                'specification' => 'required',
            ]);

            $data = $request->all();
            // dd($data);
            // dd($data['image']);

            // Storing Image of Item
            if (!empty($data['image'])) {

                $item->DeletePrevious();

                $data['image_path'] = Item::InsertImage($data['image']);

                unset($data['image']);

            }

            if (!empty($data['files'])) {

                $item->deletMultiimg();
                $data['related_image'] = item::multiImage($data['files']);
                unset($data['files']);
            }

            $data['total_price'] = $data['price'] - ($data['price'] * $data['discount_price'] / 100);

            // Creating new record
            unset($data['plans']);

            $data['updated_by'] = Auth::id();
            $data['onsale'] = !empty($data['onsale']) ? $data['onsale'] : null;
            $data['new_arrival'] = !empty($data['new_arrival']) ? $data['new_arrival'] : null;

            // Updating existing record
            $item->fill($data)->save();

            // Assigning Installment Plans to item
            if ($request->filled('plans')) {

                $item->syncplans($request->plans);

            } else {
                // Revoking Installment Plans from item
                $item->revokeplans();
            }

            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception  | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '200', 'errors' => $e->errors()];
            } else {
                return ['code' => '200', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Item $item)
    {
        try {

            // Model Binding
            $item->delete();

            return ['code' => '200', 'message' => 'success'];

        } catch (\Exception $e) {
            return ['code' => '200', 'error_message' => $e->getMessage()];
        }
    }
}
