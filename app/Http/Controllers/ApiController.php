<?php

namespace App\Http\Controllers;
use App\Models\Wishlist;
use App\Models\User;
use App\Models\Cart;
use App\Models\Item;
use App\Models\Order;
use App\Models\OrderItem;
use Auth;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Helpers\JsonResponse;


class ApiController extends Controller
{
    // public function __construct(){
    //     $this->middleware('auth:sanctum');

    // }

    public function Getwishlist(Request $request)
    {
        if (!empty($request->header('token'))) {
            $user = DB::table('users')
                ->where('api_token', $request->header('token'))
                ->pluck('id');
            // dd($user);
                JsonResponse::send(200,'success',Wishlist::with('item')->where('user_id',$user[0])->get());
        }else {
            return [
                'code' => '500',
                'message' => 'Token Missing',
                'status' => 'falied'
            ];
        }

    }

    public function storewishlist(Request $request)
    {
        // dd($request);
        if (!empty($request->header('token'))) {
            $user = DB::table('users')
                ->where('api_token', $request->header('token'))
                ->pluck('id');
            // dd($user);
            $obj = new Wishlist;
            $obj->user_id = $user[0];
            $obj->item_id = $request->item_id;
            $obj->save();
            JsonResponse::send(200,'Success');
        }else {
            return [
                'code' => '500',
                'message' => 'Token Missing',
                'status' => 'falied'
            ];
        }

    }

    public function delwishlist(Request $request)
    {
            $wishlist = Wishlist::find($request->id)->delete();
            jsonResponse::send(200,'success');
    }

    public function Getcart(Request $request)
    {
        // dd('test');
        if (!empty($request->header('token'))) {
            $user = DB::table('users')
                ->where('api_token', $request->header('token'))
                ->pluck('id');
            // dd($user);
                JsonResponse::send(200,'success',Cart::with('item')->where('user_id',$user[0])->get());
        }else {
            return [
                'code' => '500',
                'message' => 'Token Missing',
                'status' => 'falied'
            ];
        }

    }


    public function storecart(Request $request)
    {
            if(!empty($request->header('token'))){
                $user = DB::table('users')
                ->where('api_token', $request->header('token'))
                ->pluck('id');
                $obj = new Cart;
                $obj->user_id = $user[0];
                $obj->item_id = $request->item_id;
                $obj->qty = 1;
                $obj->save();
                return ['code'=>'200','message'=>'success'];
        }else {
            return [
                'code' => '500',
                'message' => 'Token Missing',
                'status' => 'falied'
            ];
        }
    }


    public function deleteCart(Request $request)
    {
            $cart = Cart::find($request->id)->delete();
            jsonResponse::send(200,'success');
    }

    public function updateCartItems(Request $request){
        // dd($request);
        foreach($request['items'] as $key => $item)
        {
            $cartitem = Cart::find($item['id']);
            $cartitem->qty = $item['qty'];
            $cartitem->update();
        }
        JsonResponse::send(200,'success');
    }

    public function orderplace(Request $request){
            // dd($request);
            $this->validate($request, [
                'payment_method' => 'required',
            ]);
        $order_details = new Order;
        $order_details->user_id = $request->user_id;
        $order_details->country = $request->country;
        $order_details->phone_number = $request->phone_number;
        $order_details->payment_method = $request->payment_method;
        $order_details->city = $request->city;
        $order_details->postal_code = $request->zip;
        $order_details->delivery_address = $request->address;
        $order_details->total_amount = $request->total;
        $order_details->total_items = $request->total_items;
        $order_details->save();


        foreach ($request['items'] as $key => $value) {
            $order_item = json_decode($value);
            $obj = new OrderItem;
            $obj->order_id = $order_details->id;
            $obj->item_id = $order_item->item_id;
            $obj->quantity = $order_item->qty;
            $obj->price = $order_item->item->price;
            $obj->onsale = $order_item->item->onsale;
            $obj->discount = $order_item->item->discount_price;
            $obj->net_amount = $order_item->item->total_price * $order_item->qty;
            $obj->save();

              $cart = Cart::where('user_id', $request->user_id)->delete();
        }
        return response()->json("Order Placed");
    }

    public function GetOrder(Request $request)
    {
        if (!empty($request->header('token'))) {
            $user = DB::table('users')
                ->where('api_token', $request->header('token'))
                ->pluck('id');
            $data = Order::where('user_id',$user[0]);
            if($request->tab === 'requested'){
                $data = $data->where('is_processed',0)->where('is_canceled',0)->get();
            }
            else{
                $data = $data->where($request->tab,1)->get();
            }
            JsonResponse::send(200,'Success', $data);
        }else {
            return [
                'code' => '500',
                'message' => 'Token Missing',
                'status' => 'falied'
            ];
        }
    }

    public function GetOrderSummary(Request $request)
    {
        // dd($request);
        if (!empty($request->header('token'))) {
            $user = DB::table('users')
                ->where('api_token', $request->header('token'))
                ->pluck('id');
                JsonResponse::send(200,'success',Order::where('user_id',$user[0])->where('id',$request->order_id)->get());
        }else {
            return [
                'code' => '500',
                'message' => 'Token Missing',
                'status' => 'falied'
            ];
        }

    }
}
