<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CustomerApplicationForm;
use Illuminate\Validation\ValidationException;
use Auth;

class CustomerApplicationFormController extends Controller
{
    // Checking User is login or not
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $selected_fields = CustomerApplicationForm::SelectedFields(); 
        return view('customerapplicationform.index',compact('selected_fields'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

            // Validating Request Data
            $this->validate($request, [
                'fields_to_display' => 'required',
            ]);

            $data = $request->all();

            $data['fields_to_display'] = CustomerApplicationForm::ConvertToString($request->fields_to_display);

            $data['created_by'] = Auth::id();

            if(CustomerApplicationForm::exists()){
                // update existing record
                $record = CustomerApplicationForm::UpdateRecord($data);
            }
            else{
                // Creating new record
                $record = CustomerApplicationForm::create($data);
            }

            return redirect()->route('customerapplicationform.index')->with('success','Application Form Updated Successfully!');

        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return redirect()->route('customerapplicationform.index')->with('errors', current($e->errors()));
            }
            else{
                return redirect()->route('customerapplicationform.index')->with('error_message', $e->getMessage());
            }
        }     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerApplicationForm $customerapplicationform)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerApplicationForm $customerapplicationform)
    {
       //   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
