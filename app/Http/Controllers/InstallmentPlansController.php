<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InstallmentPlan;
use Illuminate\Validation\ValidationException;
use DataTables;
use Auth;

class InstallmentPlansController extends Controller
{
    // Checking User is login or not
    public function __construct(){
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
                
            $plans= InstallmentPlan::select();

            return  DataTables::eloquent($plans)
                ->addColumn('action',function($data){
                $button = '';    
                if(Auth::user()->can('Edit Installment Plan'))
                $button .='<i class="fas fa-edit edit" onclick=edit('.$data->id.') style="cursor:pointer" title="Edit"></i>';
                if(Auth::user()->can('Delete Installment Plan'))
                $button .= '<i class="fas fa-trash delete text-danger"  onclick=deleterow("'.$data->id.'") style="cursor:pointer;margin-left:3px" title="Delete"></i>';
                return $button;
            })->addColumn('status',function($data){
                $status = ($data->status == 1) ? 'Active' : 'Deactivated';
                return $status;
            })->make(true);
        }
        return view('installmentplans.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            // Validating Request Data
            $this->validate($request, [
                'name' => 'required | unique:installment_plans,name',
                'months' => 'required | integer',
                'interest_rate' => 'required | numeric',
                'min_advance_rate' => 'required | numeric',
            ]);

            $data = $request->all();

            $data['created_by'] =  Auth::id();

            // Creating new record
            $obj = InstallmentPlan::create($data);

            return ['code'=>'200','message'=>'success'];

        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }       
    }

    // Returning data via server side datatable
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(InstallmentPlan $installmentplan)
    {
        // Model Binding
        return $installmentplan;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,InstallmentPlan $installmentplan)
    {
        try{
            // Validating Request Data
            $this->validate($request, [
                'name' => 'required | unique:installment_plans,name,'.$installmentplan->id,
                'months' => 'required | integer',
                'interest_rate' => 'required | numeric',
                'min_advance_rate' => 'required | numeric',
                'status' => 'required | integer',
            ]);

            $data = $request->all();

            if($request->filled('advance_deduction')){
                $data['advance_deduction'] = $request->advance_deduction;
            }
            else{
                $data['advance_deduction'] = 0;
            }

            $data['updated_by'] = Auth::id();
            
            // Updating existing record
            $installmentplan->fill($data)->save();

            return ['code'=>'200','message'=>'success'];

        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(InstallmentPlan $installmentplan)
    {
        try{
            // Model Binding
            $installmentplan->delete();

            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'200','error_message'=>$e->getMessage()];
        }
    }
}
