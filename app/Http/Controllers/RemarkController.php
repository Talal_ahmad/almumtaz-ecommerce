<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Remark;
use DataTables;
use Auth;


class RemarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $video = Remark::get();
        if($request->ajax())
        {
        $users=Remark::select('id','name','description');
        return  DataTables::eloquent($users)
        ->addColumn('action',function($data){
            $button = '';    
            if(Auth::user()->can('Edit Setting'))
            $button .='<i class="fas fa-edit edit" title="Edit User" onclick=edit('.$data->id.') style="cursor:pointer" title="Edit"></i>';
            if(Auth::user()->can('Delete Setting'))
            $button .= '<i class="fa fa-trash delete text-danger" title="Delete User"  onclick=deleterow("'.$data->id.'") style="cursor:pointer;margin-left:3px" title="Delete"></i>';
            return $button;
        })->make(true);
    }
        return view('remark.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Remark $remark)
    {
        try
        {
            $this->validate($request,[
                'name' => 'required',
                'description' => 'required',
            ]);
            $data = $request->all();
            $remark = Remark::create($data);
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Remark $remark)
    {
        return $remark->toArray();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Remark $remark)
    {
        try{
            // Validating Request Data
            $this->validate($request,[
                'name' => 'required',
                'description' => 'required',
            ]);
            $data=$request->all();
     

            // Creating new record
            $remark->fill($data)->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'200','errors' => $e->errors()];
            }
            else{
                return ['code'=>'200','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Remark $remark)
    {
        try{
            
            // Model Binding
            $remark->delete();
            
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'200','error_message'=>$e->getMessage()];
        }
    }
}
