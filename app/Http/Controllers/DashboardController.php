<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Branch;
use App\Models\Company;
use App\Models\Category;

class DashboardController extends Controller
{
    // Checking User is login or not
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        // Fetching all related data
        $users = User::select('id')->get();
        $branches = Branch::select('id')->get();
        $companies = Company::select('id')->get();
        $categories = Category::select('id')->get();
        return view('dashboard.dashboard',compact('users','branches','companies','categories'));
    }
}
