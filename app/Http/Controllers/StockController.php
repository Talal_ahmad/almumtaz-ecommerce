<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Stock;
use DataTables;
use Auth;
class StockController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $stock = Stock::get();
            return  DataTables::of($stock)
            ->addColumn('action',function($row){
                $button = '';
                if(Auth::user()->can('Edit Item'))
                $button .='<i class="fas fa-edit edit" data-toggle="editmodal" data-id="'.$row->id.'" onclick=edit('.$row->id.') style="cursor:pointer" title="Edit"></i>';
                if(Auth::user()->can('Delete Item'))
                $button .= '<i class="fas fa-trash delete text-danger dlt"  data-id="'.$row->id.'" style="cursor:pointer;margin-left:3px" title="Delete"></i>';
                return $button;
            })->make(true);

         }
        return view('stock.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $stock = StocK::find($id);
        return response()->json($stock);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $stock = Stock::find($id);
        $stock->item_id = $request->edit_item;
        $stock->company_id = $request->edit_company;
        $stock->quantity = $request->edit_quantity;
        $stock->save();
        return response()->json("success");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stock = Stock::find($id);
        $stock->delete();
        return response()->json(['success'=>'Stock has been deleted']);
    }
}
