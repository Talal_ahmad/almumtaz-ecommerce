<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();

});

Route::get('/',function(){
    return 'working';
});

Route::get('getcategory','WebsiteController@getCategory');
Route::get('getitem', 'WebsiteController@getItem');
Route::get('itemdetail', 'WebsiteController@ItemDetail');
Route::post('submit','ContactusController@store');
Route::get('getwishlist', 'ApiController@Getwishlist');
Route::post('savewishlist', 'ApiController@storewishlist');
Route::post('savecart', 'ApiController@storecart');
Route::post('deletewishlist', 'ApiController@delwishlist');
Route::post('customer','ApploginController@register');
Route::post('customer/login','ApploginController@login');
Route::post('customer/logout','ApploginController@logout');
Route::get('getcartitem','ApiController@Getcart');
Route::post('dellcart', 'ApiController@deleteCart');
Route::post('updatecart','ApiController@updateCartItems');
Route::post('callrequest','CallRequestController@callRequest');
Route::post('order_detail', 'ApiController@orderplace');
Route::get('getorder', 'ApiController@GetOrder');
Route::get('getordersummary', 'ApiController@GetOrderSummary');



