<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('website.master');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard','DashboardController@index')->name('dashboard');

// Users
Route::resource('users','UserController');


// Roles
Route::resource('roles','RoleController');

// Permissions
Route::resource('permissions','PermissionsController');

// Branches
Route::resource('branch','BranchController');

// Branches
Route::resource('disclaimer','DisclaimerController');

// Companies
Route::resource('company','CompanyController');

// Categories
Route::resource('category','CategoryController');

//Sub Categories
Route::resource('subcategory','SubCategoryController');

//Blogs
Route::resource('blog','BlogsController');
Route::post('photoupload','BlogsController@uploadPhoto')->name('PhotoUpload');

// cart controller
Route::resource('cart', 'CartController');
Route::get('getcartcount','CartController@getCartCount');
Route::post('updatecart','CartController@updateCartItems');
//Bannerimage
Route::resource('bannerimage','BannerImageController'); 

//  Brand Image
Route::resource('brandimage','BrandController');

// Items
Route::resource('item','ItemController');


// Offers
Route::resource('offer','OfferController');

// Videos
Route::resource('video','VideoController');

// Customers
Route::resource('customer','CustomerController');
Route::post('search_customer','CustomerController@searchCustomer');

// Stock
Route::resource('stock','StockController');

// Installment Plans
Route::resource('installmentplan','InstallmentPlansController');

// Customer Application Form
Route::resource('customerapplicationform','CustomerApplicationFormController');

// Contact Us Form
Route::resource('contactus','ContactusController');
Route::post('submit','ContactusController@store');

// Review
Route::resource('review','ReviewController');

Route::resource('userstatus','ReviewController@changestatus');
Route::post('reviewform','ReviewController@store');

// website Related Routes
Route::get('check_user_login', function () {
    if(Auth::check()){
        return true;
    }
    else{
        return false;
    }
});
Route::post('customer/login','CustomerController@login');
Route::get('logoutcustomer','CustomerController@logout')->name('logoutcustomer');
Route::get('getcategory','WebsiteController@getCategory');
Route::get('getitem', 'WebsiteController@getItem');
Route::get('getblog', 'WebsiteController@getBlog');
Route::get('getfeatureditems', 'WebsiteController@getfeatureditems');
Route::get('getdeal', 'WebsiteController@getdeals');
Route::get('getnewarrival', 'WebsiteController@getnewarrivals');
Route::get('getonsale', 'WebsiteController@getonsales');
Route::get('getoffers', 'WebsiteController@getnewoffer');
Route::get('getfeedbacks', 'WebsiteController@feedbacks');
Route::get('getfooter', 'WebsiteController@getFooter');
Route::get('getcompany', 'WebsiteController@getCompany');
Route::get('getoffer', 'WebsiteController@getOffer');
Route::get('getpopularCategoryItems', 'WebsiteController@getpopularCategoryItems');
Route::get('getvedio',  'WebsiteController@getVideo');
Route::get('itemdetail', 'WebsiteController@ItemDetail');
Route::get('itemReviews', 'WebsiteController@itemReviews');
Route::get('blogdetail/{id}', 'WebsiteController@blogDetail');
Route::get('userstatus','ReviewController@changestatus');
Route::post('under_review','ReviewController@under_review');
Route::post('orderstatuss','OrderController@orderstatus');
Route::resource('footerimage','FooterController');
Route::resource('wishlist', 'WishlistController');
Route::resource('feedback', 'FeedbackController');
Route::post('change_status', 'FeedbackController@status');
Route::get('getbanners','WebsiteController@getBanners');
Route::get('getuser' ,'WebsiteController@getUsers');
Route::post('check' ,'WebsiteController@checkLogin');
Route::resource('order_detail', 'OrderController');
Route::post('manual_detail', 'OrderController@manualOrder');
Route::get('addorder','OrderController@addOrder')->name("addOrder");
Route::post('getuserdetail','OrderController@getUserDetail');
Route::post('getproduct','OrderController@getProduct');
Route::post('getprice','WebsiteController@filterByRange');
Route::get('/path/{offers?}', 'AppController@index')->where('offers', '[\/\w\.-]*');
Route::resource('remark', 'RemarkController');
Route::resource('callrequest', 'CallRequestController');
Route::post('addremark','CallRequestController@addRemark');
